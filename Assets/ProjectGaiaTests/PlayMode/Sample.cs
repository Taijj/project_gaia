﻿using System.Collections;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;
using NUnit.Framework;

namespace Taijj.ProjectGaia.Tests
{
    public class MiscTests
    {
       [UnityTest]
        public IEnumerator MainSceneOpensCorrectly()
        {
            yield return TestSceneUtils.OpenScenePlaymode(TestSceneUtils.MAIN_SCENE_NAME);

            Assert.True(SceneManager.sceneCount == 1);
            Assert.True(SceneManager.GetActiveScene().name == TestSceneUtils.MAIN_SCENE_NAME);
        }
    }
}
