﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;

namespace Taijj.ProjectGaia.Tests
{
    public class BattleTests
    {
        /*[UnityTest]
        public IEnumerator DefendersWillSkipTurns()
        {
            yield return RunTest<DefenseOrderTest>();
        }
        */



        private IEnumerator RunTest<T>() where T:BattleTest, new()
        {
            UnitTestChecker.IsTest = true;
            BattleTest test = new T();
            yield return test.Run();
        }
    }
}