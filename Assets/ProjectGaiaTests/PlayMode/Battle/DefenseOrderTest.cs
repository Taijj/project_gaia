﻿

//Out of Order for now. Is too tightly coupled to a specific scene setup!
/*
using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using Taijj.ProjectGaia.Battle;
using Taijj.ProjectGaia.Battle.Data;
using Taijj.ProjectGaia.Battle.Order;
using Taijj.ProjectGaia.Battle.Phases.Execution.SkillImpacts;
using Taijj.ProjectGaia.StaticData.Skills;
using Taijj.ProjectGaia.StaticData.Skills.Impacts;
using Taijj.ProjectGaia.StaticData;
using Taijj.ProjectGaia.Battle.World;
using UnityEngine;
using Taijj.ProjectGaia.Battle.Phases.Execution.SkillGames;

namespace Taijj.ProjectGaia.Tests
{
    public class DefenseOrderTest : BattleTest
    {
        public override IEnumerator Run()
        {
            yield return TestSceneUtils.OpenScenePlaymode(TestSceneUtils.DEFENSE_ORDER_TEST_SCENE_NAME);

            InitializeTests();
            TestForSingleDefender();
            TestForMultipleDefenders();
        }

        private void InitializeTests()
        {
            Database.Load();

            BattleExecutor battleExecutor = GameObject.FindObjectOfType<BattleExecutor>();
            battleExecutor.Wake();

            BattleStage.Scenery.Wake();
            BattleExecutor.BattleData.OnBattleStart();

            FighterOrder = BattleExecutor.OrderManager;
            FighterOrder.OnBattleStart();

            Defenders = new List<Fighter>();
        }


        #region Single Defender
        private void TestForSingleDefender()
        {
            FighterOrder.OnTurnStart();
            ExecuteSkill(SkillImpactKind.Stat);

            FighterOrder.OnTurnStart();
            Defenders.Add(FighterOrder.CurrentFighter);
            ExecuteSkill(SkillImpactKind.Defend);

            FighterOrder.OnTurnStart();
            Assert.True(FighterOrder.CurrentFighter != Defenders[0]);
            ExecuteSkill(SkillImpactKind.Stat);

            FighterOrder.OnTurnStart();
            Assert.True(FighterOrder.CurrentFighter == Defenders[0]);
            ExecuteSkill(SkillImpactKind.Stat);

            Defenders.RemoveAt(0);
        }
        #endregion



        #region Multiple Defenders
        private void TestForMultipleDefenders()
        {
            DefendA();
            DefendB();
            DefendC();
            AttackD();
            AttackA();
            DefendAgainB();
            AttackE();
            AttackCThenB();
        }

        private void DefendA()
        {
            FighterOrder.OnTurnStart();
            Defenders.Add(FighterOrder.CurrentFighter);
            ExecuteSkill(SkillImpactKind.Defend);
        }

        private void DefendB()
        {
            FighterOrder.OnTurnStart();
            Defenders.Add(FighterOrder.CurrentFighter);
            Assert.True(FighterOrder.CurrentFighter != Defenders[0]);
            ExecuteSkill(SkillImpactKind.Defend);
        }

        private void DefendC()
        {
            FighterOrder.OnTurnStart();
            Assert.True(FighterOrder.CurrentFighter != Defenders[0]);
            Assert.True(FighterOrder.CurrentFighter != Defenders[1]);
            Defenders.Add(FighterOrder.CurrentFighter);
            ExecuteSkill(SkillImpactKind.Defend);
        }

        private void AttackD()
        {
            FighterOrder.OnTurnStart();
            Assert.True(FighterOrder.CurrentFighter != Defenders[0]);
            Assert.True(FighterOrder.CurrentFighter != Defenders[1]);
            Assert.True(FighterOrder.CurrentFighter != Defenders[2]);

            List<Fighter> currentOrder = CurrentOrder;
            Assert.True(currentOrder[0] == FighterOrder.CurrentFighter);
            Assert.True(currentOrder[1] == Defenders[0]);
            Assert.True(currentOrder[2] == Defenders[1]);
            Assert.True(currentOrder[3] == Defenders[2]);

            ExecuteSkill(SkillImpactKind.Stat);
        }

        private void AttackA()
        {
            FighterOrder.OnTurnStart();
            Assert.True(FighterOrder.CurrentFighter == Defenders[0]);
            Defenders.RemoveAt(0);

            List<Fighter> currentOrder = CurrentOrder;
            Assert.True(currentOrder[0] == FighterOrder.CurrentFighter);
            Assert.True(currentOrder[1] == Defenders[0]);
            Assert.True(currentOrder[2] == Defenders[1]);

            ExecuteSkill(SkillImpactKind.Stat);
        }

        private void DefendAgainB()
        {
            FighterOrder.OnTurnStart();
            Assert.True(FighterOrder.CurrentFighter == Defenders[0]);
            Defenders.RemoveAt(0);

            List<Fighter> currentOrder = CurrentOrder;
            Assert.True(currentOrder[0] == FighterOrder.CurrentFighter);
            Assert.True(currentOrder[1] == Defenders[0]);

            ExecuteSkill(SkillImpactKind.Defend);
            Defenders.Add(FighterOrder.CurrentFighter);

        }

        private void AttackE()
        {
            FighterOrder.OnTurnStart();
            Assert.True(FighterOrder.CurrentFighter != Defenders[0]);

            List<Fighter> currentOrder = CurrentOrder;
            Assert.True(currentOrder[0] == FighterOrder.CurrentFighter);
            Assert.True(currentOrder[1] == Defenders[0]);
            Assert.True(currentOrder[2] == Defenders[1]);

            ExecuteSkill(SkillImpactKind.Stat);
        }

        private void AttackCThenB()
        {
            FighterOrder.OnTurnStart();
            Defenders.RemoveAt(0);
            ExecuteSkill(SkillImpactKind.Stat);

            FighterOrder.OnTurnStart();
            Defenders.RemoveAt(0);
            ExecuteSkill(SkillImpactKind.Stat);

            Assert.True(Defenders.Count == 0);
        }
        #endregion



        #region Misc
        private void ExecuteSkill(SkillImpactKind kind)
        {
            SkillConfig selectedSkill = FighterOrder.CurrentFighter.GetSkillWithImpact(kind);
            Scheme scheme = GetScheme(selectedSkill);

            BattleExecutor.BattleData.CurrentScheme = scheme;
            BattleExecutor.BattleData.CurrentFighter = FighterOrder.CurrentFighter;

            Executor skillExecutor = EnumReliantFactory<Executor>.Create(kind);
            ImpactConfig impact = scheme.selectedSkill.Impacts[0];
            skillExecutor.Execute( GetExecutorData(impact) );
        }

        private Scheme GetScheme(SkillConfig skill)
        {
            Scheme scheme = new Scheme();
            scheme.selectedSkill = skill;
            scheme.targetPackages = Scheme.CreatePackagesForSoleTarget(skill, FighterOrder.CurrentFighter);
            return scheme;
        }

        private ExecutorData GetExecutorData(ImpactConfig impact)
        {
            switch(impact.ImpactKind)
			{
				case SkillImpactKind.Stat:	return new StatData(impact, new ImpactPackage(0, 1f, false));
				case SkillImpactKind.Defend: return new DefendData(impact);
				default: throw new ApplicationException("There's no ExecutorData type for Impactkind " + impact.ImpactKind);
			}
        }


        private List<Fighter> CurrentOrder { get { return FighterOrder.Oracle.FightersInOrder; } }
        private OrderManager FighterOrder { get; set; }
        private List<Fighter> Defenders { get; set; }
        #endregion
    }
}
*/