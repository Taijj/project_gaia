﻿using System.Collections;

namespace Taijj.ProjectGaia.Tests
{
    public abstract class BattleTest
    {
        public abstract IEnumerator Run();
    }
}