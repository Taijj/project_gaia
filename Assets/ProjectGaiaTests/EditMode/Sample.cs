﻿using NUnit.Framework;
using UnityEditor.SceneManagement;

namespace Taijj.ProjectGaia.Tests
{
    public class MiscTests
    {
        [Test]
        public void MainSceneOpensCorrectly()
        {
            TestSceneUtils.OpenSceneEditor(TestSceneUtils.MAIN_SCENE_PATH);
            string currentSceneName = EditorSceneManager.GetActiveScene().name;

            Assert.True(EditorSceneManager.loadedSceneCount == 1);
            Assert.True(currentSceneName == "Main");
        }
    }
}
