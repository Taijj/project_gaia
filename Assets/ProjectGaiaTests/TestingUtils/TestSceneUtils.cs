﻿#if UNITY_EDITOR
    using System.Collections;
    using UnityEditor.SceneManagement;
    using UnityEngine;
    using UnityEngine.SceneManagement;

    namespace Taijj.ProjectGaia.Tests
    {
        public class TestSceneUtils : MonoBehaviour
        {
            public const string MAIN_SCENE_NAME = "Main";
            public const string MAIN_SCENE_PATH = "Assets/ProjectGaia/Scenes/Main.unity";
            public const string DEFENSE_ORDER_TEST_SCENE_NAME = "DefenseOrderTest";


            public static IEnumerator OpenScenePlaymode(string sceneName)
            {
                AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);
                while(!operation.isDone)
                {
                    yield return null;
                }
            }

            public static void OpenSceneEditor(string scenePath)
            {
                EditorSceneManager.OpenScene(scenePath);
            }
        }
    }
#endif