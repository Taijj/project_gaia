﻿using UnityEngine;

namespace Taijj.ProjectGaia.Attributes
{
	public class DisabledAttribute : PropertyAttribute
	{
		public DisabledAttribute() {}
	}
}