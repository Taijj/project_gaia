﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using System;

namespace Taijj.ProjectGaia.Editor.Events
{
    [InitializeOnLoad]
    public static class EditorEventDispatcher
    {
        #region Init
        static EditorEventDispatcher()
        {
            EditorSceneManager.sceneOpened += OnSceneOpened;
            EditorSceneManager.sceneClosed += OnSceneClosed;
            EditorApplication.update += OnEditorUpdate;
        }

        [UnityEditor.Callbacks.DidReloadScripts]
        private static void OnScriptsReloaded()
        {
            OnScriptsRecompiled?.Invoke();
            DispatchForOpenScenes();
        }

        private static void OnEditorUpdate()
        {
            EditorApplication.update -= OnEditorUpdate;
            SceneView.duringSceneGui += DuringSceneGui;
            OnUnityBootupCompleted?.Invoke();
            DispatchForOpenScenes();
        }
        #endregion



        #region Events
        public static event Action OnUnityBootupCompleted;
        public static event Action OnScriptsRecompiled;
        public static event Action<string> SceneWasOpened;
        public static event Action<string> SceneWasClosed;
        public static event Action OnSceneGui;

        private static void OnSceneOpened(Scene scene, OpenSceneMode mode)
        {
            SceneWasOpened?.Invoke(scene.name);
        }

        private static void OnSceneClosed(Scene scene)
        {
            SceneWasClosed?.Invoke(scene.name);
        }

        private static void DuringSceneGui(SceneView view)
        {
            OnSceneGui?.Invoke();
        }

        private static void DispatchForOpenScenes()
        {
            for(int i = 0; i < SceneManager.sceneCount; i++)
            {
                OnSceneOpened(SceneManager.GetSceneAt(i), OpenSceneMode.Single);
            }
        }
        #endregion
    }
}
