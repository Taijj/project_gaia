﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Taijj.ProjectGaia
{
	public class AssetsHelper : MonoBehaviour
	{
		// https://answers.unity.com/questions/1425758/how-can-i-find-all-instances-of-a-scriptable-objec.html
		public static List<T> GetScriptableObjects<T>() where T : ScriptableObject
		{
			string[] guids = AssetDatabase.FindAssets("t:"+ typeof(T).Name);
			T[] instances = new T[guids.Length];
			for(int i =0; i < guids.Length; i++)
			{
				string path = AssetDatabase.GUIDToAssetPath(guids[i]);
				instances[i] = AssetDatabase.LoadAssetAtPath<T>(path);
			}

			return instances.ToList();
		}
	}
}