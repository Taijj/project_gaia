﻿using Taijj.ProjectGaia.Attributes;
using UnityEditor;
using UnityEngine;

namespace Taijj.ProjectGaia.Editor.Attributes
{
	[CustomPropertyDrawer(typeof(DisabledAttribute))]
	public class DisabledDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect propertyRect, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginDisabledGroup(true);
			EditorGUI.PropertyField(propertyRect, property, label, true);
			EditorGUI.EndDisabledGroup();
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return EditorGUI.GetPropertyHeight(property);
		}
	}
}