﻿using System;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData;
using Taijj.ProjectGaia.StaticData.FighterStats;
using UnityEditor;

namespace Taijj.ProjectGaia.Editor.World
{
	[CanEditMultipleObjects]
	[CustomEditor(typeof(Fighter))]
	public class CustomFighterEditor : UnityEditor.Editor
	{
		public void OnEnable()
		{
			Database.Load();
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			EditorGUILayout.Space();
			EditorGUILayout.Space();
			if(!EditorApplication.isPlaying)
			{
				EditorGUILayout.LabelField("To show current stats and buffs, start playmode!");
				return;
			}

			Fighter = (Fighter)target;
			DrawStats();
			DrawBuffs();
		}

		private void DrawStats()
		{
			EditorGUILayout.Space();
			EditorGUILayout.Space();

			EditorGUILayout.LabelField("Current Stats", EditorStyles.boldLabel);
			foreach(Stat stat in Enum.GetValues(typeof(Stat)))
			{
				EditorGUILayout.LabelField(stat + ": " + Fighter.GetStat(stat));
			}
		}

		private void DrawBuffs()
		{
			EditorGUILayout.Space();
			EditorGUILayout.Space();
			EditorGUILayout.LabelField("Current Buffs", EditorStyles.boldLabel);

			foreach(Buff buff in Fighter.BuffTracker.Buffs)
			{
				EditorGUILayout.LabelField(buff.BuffId + ": " + buff.Stat + " = " + buff.Value);
			}
		}

		private Fighter Fighter { get; set; }
	}
}