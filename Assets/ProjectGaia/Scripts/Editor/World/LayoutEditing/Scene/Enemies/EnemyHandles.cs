﻿using System;
using UnityEditor;
using UnityEngine;

namespace Taijj.ProjectGaia.Editor.World.LayoutEditing.Scene.Enemies
{
    public static class EnemyHandles
    {
		public static event Action<int> OnPositionChanged;

        public static void CreateHandle(EnemiesEditor.IndexedEnemy enemy)
		{
		    CurrentHandlePosition = enemy.enemy.position;

			CreateLabel($"{enemy.index.ToString()}: {enemy.enemy.config.name}");
			CreateCenter();
			ApplyChanges(enemy, DoCircleHandle());
		}

		private static Vector3 DoCircleHandle()
		{
            Handles.color = HandlesColor;
			CurrentHandlePosition = Handles.FreeMoveHandle(
				CurrentHandlePosition,
				Quaternion.identity,
				HandlesSize,
				BattleLayoutEditor.HANDLE_SNAP,
				Handles.CircleHandleCap);

			return CurrentHandlePosition;
		}

		private static void CreateLabel(string text)
		{
			Handles.color = Color.gray;
			Handles.Label(
				CurrentHandlePosition + Vector3.down*HandlesSize,
				text);
		}

		private static void CreateCenter()
		{
            Handles.color = HandlesColor;
			Handles.DotHandleCap(0,
				CurrentHandlePosition,
				Quaternion.identity,
				0.1f * HandlesSize,
				EventType.Repaint);
		}

		private static void ApplyChanges(EnemiesEditor.IndexedEnemy enemy, Vector3 newPosition)
		{
			if(enemy.enemy.position != newPosition)
			{
				enemy.enemy.position = newPosition;
				OnPositionChanged?.Invoke(enemy.index);
			}
		}


		private static Vector3 CurrentHandlePosition { get; set; }
        private static float HandlesSize => HandleUtility.GetHandleSize(CurrentHandlePosition)*BattleLayoutEditor.HANDLES_SIZE;
        private static Color HandlesColor => Color.red;
    }
}