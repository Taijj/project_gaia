﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Taijj.ProjectGaia.Editor.World.LayoutEditing.Scene.Enemies
{
    public static class IndicesCalculator
    {
        public static void Calculate(List<EnemiesEditor.IndexedEnemy> originalPositions)
        {
            List<EnemiesEditor.IndexedEnemy> sortingList = new List<EnemiesEditor.IndexedEnemy>(originalPositions);
            sortingList.Sort(SortOnGrid);
            for(int i = 0; i < sortingList.Count; i++)
            {
                EnemiesEditor.IndexedEnemy pos = originalPositions.First(p => p == sortingList[i]);
                pos.index = i;
            }
        }

        private static int SortOnGrid(EnemiesEditor.IndexedEnemy e1, EnemiesEditor.IndexedEnemy e2)
        {
            Vector3 position1 = e1.enemy.position;
            Vector3 position2 = e2.enemy.position;

            if(position1 == position2)
            {
                return 1;
            }

            if(IsRoughlyTheSame(position1.y, position2.y))
            {
                return position1.x > position2.x ? 1 : -1;
            }
            else
            {
                return position1.y < position2.y ? 1 : -1;
            }
        }

        private static bool IsRoughlyTheSame(float a, float b)
        {
            return a > b-0.5f && a < b+0.5f;
        }
    }
}