﻿using System.Collections.Generic;
using System.Linq;
using Taijj.ProjectGaia.Battle.World.Layout;
using Taijj.ProjectGaia.Battle.World.Layout.Previews;
using Taijj.ProjectGaia.StaticData.Fighters;
using UnityEngine;

namespace Taijj.ProjectGaia.Editor.World.LayoutEditing.Scene.Enemies
{
    public static class EnemiesEditor
    {
        #region Init
        public class IndexedEnemy
        {
            public IndexedEnemy(FighterConfig config, Vector3 position)
            {
                enemy = new PositionedEnemy();
                enemy.config = config;
                enemy.position = position;
                index = 0;
            }

            public PositionedEnemy enemy;
            public int index;
        }



		public static void Enable()
		{
            InitializeComponents();
            RegisterEvents();
		}

        private static void InitializeComponents()
        {
            Lineup = Object.FindObjectOfType<EnemiesLineup>();
            Enemies = new List<IndexedEnemy>();
            SyncFromScene();
        }

        private static void RegisterEvents()
        {
            BattleLayoutEditor.OnSceneGui += OnSceneGui;
            BattleLayoutEditor.OnScriptsReloaded += SyncFromScene;
            SceneDropHandler.OnEnemyDropped += OnEnemyDropped;
            EnemyHandles.OnPositionChanged += OnHandlePositionChanged;
            UserInterface.OnRemoveButtonPressed += DeleteFocussedEnemy;
        }

        public static void Disable()
		{
            DestroyComponents();
            RemoveEvents();
		}

        private static void DestroyComponents()
        {
            Lineup = null;
            Enemies = null;
        }

        private static void RemoveEvents()
        {
            BattleLayoutEditor.OnSceneGui -= OnSceneGui;
            BattleLayoutEditor.OnScriptsReloaded -= SyncFromScene;
            SceneDropHandler.OnEnemyDropped -= OnEnemyDropped;
            EnemyHandles.OnPositionChanged -= OnHandlePositionChanged;
            UserInterface.OnRemoveButtonPressed -= DeleteFocussedEnemy;
        }



        public static void OnSceneGui()
        {
            IndicesCalculator.Calculate(Enemies);
            CreateHandles();
            SyncFromEditor();
        }
        #endregion



        #region Main
     	private static void CreateHandles()
		{
            Enemies.ForEach( e => EnemyHandles.CreateHandle(e) );
		}

        public static void OnEnemyDropped(FighterConfig enemy)
        {
            AddEnemy(enemy, Vector3.zero);
            DeletionIndex = Enemies[Enemies.Count-1].index;
        }

        public static void AddEnemy(FighterConfig enemy, Vector3 position)
        {
            IndexedEnemy newEnemy = new IndexedEnemy(enemy, position);

            Enemies.Add(newEnemy);
            IndicesCalculator.Calculate(Enemies);
        }

        public static void Set(List<PositionedEnemy> enemies)
        {
            Enemies.Clear();
            enemies.ForEach(e => AddEnemy(e.config, e.position));
            SyncFromEditor();
        }
        #endregion



        #region Deletion
        private static void DeleteFocussedEnemy()
        {
            if(DeletionIndex < 0 || Enemies.Count == 0)
            {
                return;
            }

            Enemies.RemoveAt(DeletionIndex);
            DeletionIndex = Enemies.Count-1;
            IndicesCalculator.Calculate(Enemies);
            SyncFromEditor();
        }

        private static void OnHandlePositionChanged(int index)
        {
            DeletionIndex = index;
        }
        #endregion



        #region Syncing
        private static void SyncFromScene()
        {
            Lineup.Enemies.ForEach(e => AddEnemy(e.config, e.position));
            DeletionIndex = Enemies.Count > 0 ? 0 : -1;
        }

        private static void SyncFromEditor()
        {
            if(Lineup == null)
            {
                return;
            }

            List<IndexedEnemy> sortedPositions = Enemies.OrderBy(e => e.index).ToList();
            Lineup.Clear();
            sortedPositions.ForEach(c => Lineup.AddEnemy(c.enemy));

            Lineup.GetComponent<EnemiesPreview>().Enemies = Enemies.Select(e => e.enemy).ToList();
            UnityEditor.EditorApplication.QueuePlayerLoopUpdate();
        }
        #endregion



        #region Properties
        private static List<IndexedEnemy> Enemies { get; set; } // Unsorted
        private static int DeletionIndex { get; set; }
        public static EnemiesLineup Lineup { get; private set; }
        #endregion
    }
}