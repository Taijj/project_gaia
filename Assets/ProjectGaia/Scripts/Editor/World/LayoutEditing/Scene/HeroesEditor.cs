﻿using Taijj.ProjectGaia.Battle.World.Layout;
using Taijj.ProjectGaia.Utils.Numerical;
using UnityEditor;
using UnityEngine;

namespace Taijj.ProjectGaia.Editor.World.LayoutEditing.Scene
{
    public static class HeroesEditor
    {
        #region Main
		public static void Enable()
		{
            Lineup = Object.FindObjectOfType<HeroesLineup>();
			BattleLayoutEditor.OnSceneGui += CreateHandles;
		}

		public static void Disable()
		{
			Lineup = null;
			BattleLayoutEditor.OnSceneGui -= CreateHandles;
		}

        public static HeroesLineup Lineup { get; private set; }
		#endregion



		#region Handles
		private static void CreateHandles()
		{
			Vector3Range newLayoutPoints = new Vector3Range();
			newLayoutPoints.min = CreateHandle(Lineup.LayoutPoints.min, Color.green, "Min");
			newLayoutPoints.max = CreateHandle(Lineup.LayoutPoints.max, Color.cyan, "Max");
			Lineup.LayoutPoints = newLayoutPoints;
		}

		private static Vector3 CreateHandle(Vector3 position, Color color, string handleName)
		{
			CurrentHandleColor = color;
            CurrentHandlePosition = position;
			CurrentHandleSize = HandleUtility.GetHandleSize(CurrentHandlePosition)*BattleLayoutEditor.HANDLES_SIZE;

			CreateHandleLabel(handleName);
			CreateHandleCenter();
			return DoCircleHandle();
		}

		private static Vector3 DoCircleHandle()
		{
			Handles.color = CurrentHandleColor;
			CurrentHandlePosition = Handles.FreeMoveHandle(
				CurrentHandlePosition,
				Quaternion.identity,
				CurrentHandleSize,
				BattleLayoutEditor.HANDLE_SNAP,
				Handles.CircleHandleCap);

			return CurrentHandlePosition;
		}

		private static void CreateHandleLabel(string text)
		{
			Handles.color = Color.gray;
			Handles.Label(
				CurrentHandlePosition + Vector3.down*CurrentHandleSize,
				text);
		}

		private static void CreateHandleCenter()
		{
			Handles.color = CurrentHandleColor;
			Handles.DotHandleCap(0,
				CurrentHandlePosition,
				Quaternion.identity,
				0.1f * CurrentHandleSize,
				EventType.Repaint);
		}

		private static Vector3 CurrentHandlePosition { get; set; }
		private static float CurrentHandleSize { get; set; }
		private static Color CurrentHandleColor { get; set; }
		#endregion
    }
}