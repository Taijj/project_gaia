﻿
using Taijj.ProjectGaia.Battle.World.Layout;
using Taijj.ProjectGaia.Editor.World.LayoutEditing.Scene.Enemies;
using Taijj.ProjectGaia.StaticData;

namespace Taijj.ProjectGaia.Editor.World.LayoutEditing.Scene
{
    public static class SceneIO
    {
        public static void Enable()
        {
            HeroesEditor.Enable();
            EnemiesEditor.Enable();

            Persistance.OnConfigChanged += OnConfigChanged;
        }

        public static void Disable()
        {
            HeroesEditor.Disable();
            EnemiesEditor.Disable();

            Persistance.OnConfigChanged -= OnConfigChanged;
        }

        private static void OnConfigChanged(LineupConfig config)
        {
            HeroesEditor.Lineup.LayoutPoints = config.HeroesLayoutPoints;
            EnemiesEditor.Set(config.PositionedEnemies);
        }

        public static HeroesLineup HeroesLineup => HeroesEditor.Lineup;
        public static EnemiesLineup EnemiesLineup => EnemiesEditor.Lineup;
    }
}