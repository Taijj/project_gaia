﻿using System;
using Taijj.ProjectGaia.StaticData;
using UnityEditor;
using UnityEngine;

namespace Taijj.ProjectGaia.Editor.World.LayoutEditing
{
    public static class UserInterface
    {
        #region Main
        private static readonly Rect RECT = new Rect(0, 0, 500, 50);
        private const string NOTHING_LOADED_LABEL = "---";

        public static void Enable()
        {
            BattleLayoutEditor.OnSceneGui += OnSceneGui;
            Persistance.OnConfigChanged += OnConfigWasLoaded;
        }

        public static void Disable()
        {
            BattleLayoutEditor.OnSceneGui -= OnSceneGui;
            Persistance.OnConfigChanged -= OnConfigWasLoaded;
            LoadedConfigName = NOTHING_LOADED_LABEL;
        }

        private static void OnSceneGui()
        {
            Handles.BeginGUI();
                DoUserInterface();
            Handles.EndGUI();
        }

        private static void DoUserInterface()
        {
            GUILayout.BeginArea(RECT);
                DoButtons();
                DoLabel();
            GUILayout.EndArea();
        }
        #endregion



        #region Buttons&Labels
        public static event Action OnLoadButtonPressed;
        public static event Action OnSaveButtonPressed;
        public static event Action OnSaveAsNewButtonPressed;

        public static event Action OnRemoveButtonPressed;

        private static void DoButtons()
        {
            GUILayout.BeginHorizontal();
                if(GUILayout.Button("Load") )
                {
                    OnLoadButtonPressed?.Invoke();
                }
                if(GUILayout.Button("Save"))
                {
                    OnSaveButtonPressed?.Invoke();
                }
                if(GUILayout.Button("SaveAsNew"))
                {
                    OnSaveAsNewButtonPressed?.Invoke();
                }
                if(GUILayout.Button("RemoveEnemy"))
                {
                    OnRemoveButtonPressed?.Invoke();
                }
            GUILayout.EndHorizontal();
        }

        private static void DoLabel()
        {
            GUIStyle style = new GUIStyle();
            style.fontSize = 15;
            style.fontStyle = FontStyle.Bold;
            style.normal.textColor = Color.white;

            string configName = string.IsNullOrEmpty(LoadedConfigName) ? NOTHING_LOADED_LABEL : LoadedConfigName;
            GUILayout.Label(configName, style );
        }

        private static void OnConfigWasLoaded(LineupConfig config)
        {
            LoadedConfigName = config.name;
        }

        private static string LoadedConfigName { get; set; }
        #endregion
    }
}