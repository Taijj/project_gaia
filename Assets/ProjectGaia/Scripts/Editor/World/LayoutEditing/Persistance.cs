﻿using System;
using Taijj.ProjectGaia.Editor.World.LayoutEditing.Scene;
using Taijj.ProjectGaia.StaticData;
using UnityEditor;
using UnityEngine;

namespace Taijj.ProjectGaia.Editor.World.LayoutEditing
{
    public static class Persistance
    {
        #region Init
        private const string DEFAULT_PATH = "D:/Projects/Unity/PG Battle Prototype/Assets/ProjectGaia/Database/Lineups/";

        public static event Action<LineupConfig> OnConfigChanged;

        public static void Enable()
        {
            UserInterface.OnLoadButtonPressed += Load;
            UserInterface.OnSaveButtonPressed += Save;
            UserInterface.OnSaveAsNewButtonPressed += SaveAsNew;
            SceneDropHandler.OnLineupDropped += SetConfig;
        }

        public static void Disable()
        {
            UserInterface.OnLoadButtonPressed -= Load;
            UserInterface.OnSaveButtonPressed -= Save;
            UserInterface.OnSaveAsNewButtonPressed -= SaveAsNew;
            SceneDropHandler.OnLineupDropped -= SetConfig;
            Config = null;
        }

        private static void SetConfig(LineupConfig config)
        {
            Config = config;
            OnConfigChanged?.Invoke(Config);
        }

        private static LineupConfig Config { get; set; }
        #endregion



        #region Loading
        private static void Load()
        {
            string path = EditorUtility.OpenFilePanel("Load Lineup Config", DEFAULT_PATH, "asset");
            if(string.IsNullOrEmpty(path))
            {
                return;
            }

            string assetPath = ConvertToAssetPath(path);
            LineupConfig loadedConfig = (LineupConfig)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(LineupConfig));
            SetConfig(loadedConfig);
        }

        private static string ConvertToAssetPath(string path)
        {
            return "Assets" + path.Substring(Application.dataPath.Length);
        }
        #endregion



        #region Saving
        private static void Save()
        {
            if(Config != null)
            {
                QuickSave();
            }
            else
            {
                SaveAsNew();
            }
        }

        private static void QuickSave()
        {
            Config.HeroesLayoutPoints = SceneIO.HeroesLineup.LayoutPoints;
            Config.PositionedEnemies = SceneIO.EnemiesLineup.Enemies.Copy();
            EditorUtility.SetDirty(Config);
            AssetDatabase.SaveAssets();
        }

        private static void SaveAsNew()
        {
            string path = EditorUtility.SaveFilePanelInProject("Save LineupConfig", "NewLineup", "asset", "I don't know when this appears...", DEFAULT_PATH);
            if(string.IsNullOrEmpty(path))
            {
                return;
            }

            LineupConfig newConfig = (LineupConfig)ScriptableObject.CreateInstance(typeof(LineupConfig));
            newConfig.HeroesLayoutPoints = SceneIO.HeroesLineup.LayoutPoints;
            newConfig.PositionedEnemies = SceneIO.EnemiesLineup.Enemies.Copy();

            AssetDatabase.CreateAsset(EditorUtility.InstanceIDToObject(newConfig.GetInstanceID()), path);
            SetConfig(newConfig);
        }
        #endregion
    }
}