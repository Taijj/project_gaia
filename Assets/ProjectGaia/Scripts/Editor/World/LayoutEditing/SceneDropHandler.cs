﻿using System;
using Taijj.ProjectGaia.StaticData;
using Taijj.ProjectGaia.StaticData.Fighters;
using UnityEditor;
using UnityEngine;

namespace Taijj.ProjectGaia.Editor.World.LayoutEditing
{
    public static class SceneDropHandler
    {
        #region Main
        public static event Action<FighterConfig> OnEnemyDropped;
        public static event Action<LineupConfig> OnLineupDropped;

        public static void Enable()
        {
            BattleLayoutEditor.OnSceneGui += OnSceneGui;
        }

        public static void Disable()
        {
            BattleLayoutEditor.OnSceneGui -= OnSceneGui;
        }

        private static void OnSceneGui()
        {
            bool mouseIsOverSceneView = EditorWindow.mouseOverWindow == SceneView.currentDrawingSceneView;
            if(!mouseIsOverSceneView)
            {
                return;
            }

            HandleSceneDrop();
        }
        #endregion



        private static void HandleSceneDrop()
        {
            if(!DragContainsValidTypes)
            {
                return;
            }

            if(Event.current.type == EventType.DragUpdated)
            {
                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
            }

            if(Event.current.type == EventType.DragExited)
            {
                Drop();
            }
        }

        private static bool DragContainsValidTypes
        {
            get
            {
                foreach(object o in DragAndDrop.objectReferences)
                {
                    if(o.GetType() == typeof(LineupConfig))
                    {
                        continue;
                    }

                    if(o.GetType() == typeof(FighterConfig))
                    {
                        continue;
                    }
                    return false;
                }
                return true;
            }
        }

        private static void Drop()
        {
            foreach(object o in DragAndDrop.objectReferences)
            {
                if(o.GetType() == typeof(LineupConfig))
                {
                    OnLineupDropped?.Invoke((LineupConfig)o);
                }

                if(o.GetType() == typeof(FighterConfig))
                {
                    OnEnemyDropped?.Invoke((FighterConfig)o);
                }
            }
            DragAndDrop.AcceptDrag();
        }
    }
}