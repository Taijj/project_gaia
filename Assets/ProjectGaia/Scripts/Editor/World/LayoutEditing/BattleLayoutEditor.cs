﻿using System;
using Taijj.ProjectGaia.Editor.Events;
using Taijj.ProjectGaia.Editor.World.LayoutEditing.Scene;
using UnityEditor;
using UnityEngine;

namespace Taijj.ProjectGaia.Editor.World.LayoutEditing
{
    [InitializeOnLoad]
    public class BattleLayoutEditor : EditorWindow
    {
        #region Init
        public const float HANDLES_SIZE = 0.25f;
		public static readonly Vector3 HANDLE_SNAP = Vector3.one*0.5f;
        private const string SCENE_NAME = "BattleLayoutEditor";

        public static event Action OnSceneGui;
        public static event Action OnScriptsReloaded;

        static BattleLayoutEditor()
        {
            EditorEventDispatcher.SceneWasOpened += OnUnitySceneOpened;
        }
        #endregion



        #region Enable/Disable
        public static void OnUnitySceneOpened(string sceneName)
        {
            if(sceneName == SCENE_NAME && !Enabled)
            {
                Enable();
            }
            else if(sceneName != SCENE_NAME && Enabled)
            {
                Disable();
            }
        }

        private static void Enable()
        {
            Enabled = true;
            EditorEventDispatcher.OnSceneGui += DuringSceneGui;

            UserInterface.Enable();
            Persistance.Enable();
            SceneDropHandler.Enable();
            SceneIO.Enable();
        }

        private static void Disable()
        {
            Enabled = false;
            EditorEventDispatcher.OnSceneGui -= DuringSceneGui;

            UserInterface.Disable();
            Persistance.Disable();
            SceneDropHandler.Disable();
            SceneIO.Disable();
        }

        private static void DispatchScriptsReloaded()
        {
            OnScriptsReloaded?.Invoke();
        }

        private static bool Enabled { get; set; }
        #endregion



        #region SceneGui Update
        private static void DuringSceneGui()
        {
            EditorGUI.BeginChangeCheck();
                OnSceneGui?.Invoke();
            if(EditorGUI.EndChangeCheck())
            {
                HandleUndo();
            }
        }

        private static void HandleUndo()
        {
            if(SceneIO.HeroesLineup == null || SceneIO.EnemiesLineup == null)
            {
                return;
            }

            Undo.RegisterCompleteObjectUndo(SceneIO.HeroesLineup, "BattleLayoutEditor Changed");
            Undo.RegisterCompleteObjectUndo(SceneIO.EnemiesLineup, "BattleLayoutEditor Changed");
            Undo.FlushUndoRecordObjects();
            EditorUtility.SetDirty(SceneIO.HeroesLineup);
            EditorUtility.SetDirty(SceneIO.EnemiesLineup);
        }
        #endregion
    }
}