﻿using System.Collections.Generic;
using Taijj.ProjectGaia.StaticData;
using Taijj.ProjectGaia.StaticData.Fighters;
using Taijj.ProjectGaia.StaticData.Skills.Types;
using UnityEditor;
using UnityEngine;

namespace Taijj.ProjectGaia.Editor.StaticData
{
	[CustomEditor(typeof(Database))]
	public class CustomDatabaseEditor : DatabaseDependendEditor
	{
		#region Unity
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			if(GUI.changed)
			{
				UpdateAssets();
			}
		}
		#endregion



		#region Asset Updates
		private void UpdateAssets()
		{
			UpdateFighterConfigs();
			UpdateAnimationSkillConfigs();
 			AssetDatabase.SaveAssets();
		}

		private void UpdateFighterConfigs()
		{
			List<FighterConfig> configs = AssetsHelper.GetScriptableObjects<FighterConfig>();
			foreach(FighterConfig config in configs)
			{
				config.Stats.UpdateFromDatabase(Database.StatSection);
				EditorUtility.SetDirty(config);
			}
		}

		private void UpdateAnimationSkillConfigs()
		{
			List<AnimationSkillConfig> configs = AssetsHelper.GetScriptableObjects<AnimationSkillConfig>();
			foreach(AnimationSkillConfig config in configs)
			{
				config.DamageSettings.SetForms(config);
				config.HealingSettings.SetForms(config);
				EditorUtility.SetDirty(config);
			}
		}
		#endregion
	}
}