﻿using System.Linq;
using System.Collections.Generic;
using Taijj.ProjectGaia.StaticData.Strategies.Ai.Behaviors;
using UnityEditor;
using UnityEngine;

namespace Taijj.ProjectGaia.Editor.StaticData
{
	[CustomPropertyDrawer(typeof(AiBehavior))]
	public class AiBehaviorDrawer : PropertyDrawer
	{
		#region Maps
		private struct Map
		{
			public Map(AiBehavior.Kind kind, List<string> propertyNames)
			{
				Kind = kind;
				PropertyNames = propertyNames;
			}

			public AiBehavior.Kind Kind { get; private set; }
			public List<string> PropertyNames { get; private set; }
		}

		private static readonly List<Map> MAPS = new List<Map>()
		{
			new Map(AiBehavior.Kind.FullyRandom, new List<string>{}),
			new Map(AiBehavior.Kind.StatDependend, new List<string>{"_statDependendKind", "_targetStat", "_skill"}),
			new Map(AiBehavior.Kind.FixedTarget, new List<string>{"_skill", "_target"}),
		};
		#endregion



		#region Main
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			Property = property;
			int lines = GetMap().PropertyNames.Count + 1;
			return EditorGUIUtility.singleLineHeight * (float)lines;
    	}

		public override void OnGUI(Rect propertyRect, SerializedProperty property, GUIContent label)
		{
			Property = property;
			PropertyRect = propertyRect;
			DrawCommonFields();
			DrawDynamicFields();
		}
		#endregion



		#region Drawing
		private void DrawCommonFields()
		{
			Rect weightRect = new Rect(PropertyRect.x, PropertyRect.y, PropertyRect.width*0.2f, EditorGUIUtility.singleLineHeight);
			Rect kindRect = new Rect(PropertyRect.x+weightRect.width, PropertyRect.y, PropertyRect.width*0.8f, EditorGUIUtility.singleLineHeight);

			EditorGUI.PropertyField(weightRect, WeightProperty, GUIContent.none);
			EditorGUI.PropertyField(kindRect, KindProperty, GUIContent.none);
		}

		private void DrawDynamicFields()
		{
			Map map = GetMap();
			for(int i = 0; i < map.PropertyNames.Count; i++)
			{
				Rect rect = new Rect(PropertyRect.x,
					PropertyRect.y + (float)(i+1)*EditorGUIUtility.singleLineHeight,
					PropertyRect.width, EditorGUIUtility.singleLineHeight);

				EditorGUI.PropertyField(rect, Property.FindPropertyRelative(map.PropertyNames[i]));
			}
		}
		#endregion



		#region Helpers
		private Map GetMap()
		{
			AiBehavior.Kind kind = GetKind(Property);
			return MAPS.First(m => m.Kind == kind);
		}

		private AiBehavior.Kind GetKind(SerializedProperty property)
		{
			return (AiBehavior.Kind)KindProperty.intValue;
		}

		private SerializedProperty Property { get; set; }
		private Rect PropertyRect { get; set; }
		private SerializedProperty WeightProperty => Property.FindPropertyRelative("_weight");
		private SerializedProperty KindProperty => Property.FindPropertyRelative("_kind");
		#endregion
	}
}