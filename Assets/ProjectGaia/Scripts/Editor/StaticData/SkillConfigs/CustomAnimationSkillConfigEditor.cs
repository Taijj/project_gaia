using Taijj.ProjectGaia.StaticData.Skills.Types;
using UnityEditor;
using UnityEngine;

namespace Taijj.ProjectGaia.Editor.StaticData.SkillConfigs
{
    [CustomEditor(typeof(AnimationSkillConfig))]
	public class CustomDatabaseEditor : UnityEditor.Editor
	{
		#region Unity
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			if(GUI.changed)
			{
                AnimationSkillConfig config = target as AnimationSkillConfig;
                config.DamageSettings.SetForms(config);
                config.HealingSettings.SetForms(config);
			}
		}
		#endregion
    }
}