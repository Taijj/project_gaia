﻿using System;
using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.FighterMovement;
using Taijj.ProjectGaia.StaticData;
using UnityEditor;
using UnityEngine;

namespace Taijj.ProjectGaia.Editor.StaticData
{
	[CustomPropertyDrawer(typeof(SkillMovementSection))]
	public class SkillMovementSectionDrawer : PropertyDrawer
	{
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			if(property.isExpanded)
			{
				return EditorGUIUtility.singleLineHeight * (MovementKinds.Length+1);
			}
			else
			{
				return base.GetPropertyHeight(property, label);
			}
    	}

		public override void OnGUI(Rect propertyRect, SerializedProperty property, GUIContent label)
		{
			DoFoldout(propertyRect, property, label);
			if(property.isExpanded)
			{
				SkillMovementKind[] kinds = MovementKinds;
				SerializedProperty configsProperty = GetConfigsProperty(property, kinds.Length);

				EditorGUI.indentLevel = 2;
				for(int i = 0; i < kinds.Length; i++)
				{
					string fieldLabel = kinds[i].ToString()+" Config";
					CreateConfigField(propertyRect, i, configsProperty, fieldLabel);
				}
			}
		}

		private void DoFoldout(Rect propertyRect, SerializedProperty property, GUIContent label)
		{
			// EditorGUI.Foldout() needs it's own single line height Rect, otherwise it will consume
			// click events of propertyfields inside the foldout.
			Rect foldoutRect = new Rect(propertyRect);
			foldoutRect.height = EditorGUIUtility.singleLineHeight;
			property.isExpanded = EditorGUI.Foldout(foldoutRect, property.isExpanded, label);
		}

		private SerializedProperty GetConfigsProperty(SerializedProperty sectionProperty, int movementKindCount)
		{
			SerializedProperty configsProperty = sectionProperty.FindPropertyRelative("_movementConfigsByKind");
			if(configsProperty.arraySize != movementKindCount)
			{
				configsProperty.arraySize = movementKindCount;
			}
			return configsProperty;
		}

		private void CreateConfigField(Rect sectionRect, int listIndex, SerializedProperty configsProperty, string labelText)
		{
			Rect rect = new Rect(sectionRect);
			rect.height = EditorGUIUtility.singleLineHeight;
			rect.y = sectionRect.y + EditorGUIUtility.singleLineHeight*(float)(listIndex+1);

			EditorGUI.PropertyField(
				rect,
				configsProperty.GetArrayElementAtIndex(listIndex),
				new GUIContent(labelText));
		}

		private SkillMovementKind[] MovementKinds => (SkillMovementKind[])Enum.GetValues(typeof(SkillMovementKind));
	}
}