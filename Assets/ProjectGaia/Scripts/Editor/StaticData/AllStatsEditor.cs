﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Taijj.ProjectGaia
{
	public class AllStatsEditor
	{
		#region Lifecycle
		public AllStatsEditor(SerializedObject target)
		{
			Target = target;

			SerializedProperty statsListProperty = Target.FindProperty("_statsSection").FindPropertyRelative("_allStats");
			StatsList = new ReorderableList(Target, statsListProperty, true, false, true, true);
			StatsList.headerHeight = 7f;
			StatsList.onRemoveCallback += OnDeleteFromList;
			StatsList.drawElementCallback += OnDrawListItem;

			StatsAreShown = true;
		}

		public void OnDestoy()
		{
			if(StatsList == null)
			{
				return;
			}

			StatsList.onRemoveCallback -= OnDeleteFromList;
			StatsList.drawElementCallback -= OnDrawListItem;
		}
		#endregion



		#region Reorderable List
		private void OnDeleteFromList(ReorderableList list)
		{
			ReorderableList.defaultBehaviours.DoRemoveButton(list);
		}

		private void OnDrawListItem(Rect rect, int index, bool isActive, bool isFocused)
		{
			SerializedProperty statProperty = StatsList.serializedProperty.GetArrayElementAtIndex(index);
			EditorGUI.PropertyField(rect, statProperty);
		}

		public void DrawStats()
		{
			StatsAreShown = EditorGUILayout.Foldout(StatsAreShown, new GUIContent("Stats Order"));
			if(StatsAreShown)
			{
				DrawList();
			}
		}

		private void DrawList()
		{
			Target.Update();
			StatsList.DoLayoutList();
			Target.ApplyModifiedProperties();
		}
		#endregion



		#region Properties
		private SerializedObject Target { get; set; }
		private ReorderableList StatsList { get; set; }
		private bool StatsAreShown { get; set; }
		#endregion
	}
}