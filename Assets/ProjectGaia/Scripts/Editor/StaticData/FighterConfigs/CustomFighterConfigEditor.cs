﻿using Taijj.ProjectGaia.StaticData.Fighters;
using UnityEditor;

namespace Taijj.ProjectGaia.Editor.StaticData.Fighters
{
	[CanEditMultipleObjects]
	[CustomEditor(typeof(FighterConfig))]
	public class CustomFighterConfigEditor : DatabaseDependendEditor
	{
		public override void OnEnable()
		{
			base.OnEnable();
			StatsEditor = new StatsEditor(serializedObject);
			ResistancesEditor = new ResistancesEditor(serializedObject);
		}

		public void OnDestroy()
		{
			ResistancesEditor.OnDestoy();
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			StatsEditor.DrawStats();
			ResistancesEditor.DrawResistances();
		}


		private StatsEditor StatsEditor { get; set; }
		private ResistancesEditor ResistancesEditor { get; set; }
	}
}