﻿using UnityEngine;
using UnityEditor;
using Taijj.ProjectGaia.StaticData.Fighters;

namespace Taijj.ProjectGaia.Editor.StaticData.Fighters
{
	[CustomPropertyDrawer(typeof(Resistance))]
	public class ResistanceDrawer : PropertyDrawer
	{
		private const float PROPERTY_MARGINS = 5f;

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
        	return 0f;
    	}

		public override void OnGUI(Rect propertyRect, SerializedProperty property, GUIContent label)
		{
			Rect formRect = GetTopLevelRect(propertyRect, 0);
			Rect modificationRect = GetTopLevelRect(propertyRect, 1);
			SerializedProperty formProperty = property.FindPropertyRelative("_form");
			SerializedProperty modificationProperty = property.FindPropertyRelative("_modification");

			DrawProperty(formProperty, formRect, "Form");
			DrawProperty(modificationProperty, modificationRect, "Modification");
		}

		private Rect GetTopLevelRect(Rect propertyRect, int index)
		{
			Rect newRect = new Rect(propertyRect);
			newRect.width = propertyRect.width/2f -2f*PROPERTY_MARGINS;
			newRect.height = EditorGUIUtility.singleLineHeight;
			newRect.x = propertyRect.x + PROPERTY_MARGINS + (newRect.width + 2f*PROPERTY_MARGINS) * (float)index;
			return newRect;
		}

		private void DrawProperty(SerializedProperty property, Rect rect, string labelText)
		{
			Rect labelRect = new Rect(rect);
			labelRect.width = 0.4f*rect.width;
			EditorGUI.LabelField(labelRect, labelText);

			Rect secondaryRect = new Rect(rect);
			secondaryRect.x = labelRect.x + labelRect.width;
			secondaryRect.width = 0.6f*rect.width;
			EditorGUI.PropertyField(secondaryRect, property, GUIContent.none);
		}
	}
}