﻿using UnityEngine;
using UnityEditor;
using Taijj.ProjectGaia.StaticData;
using System.Collections.Generic;
using Taijj.ProjectGaia.StaticData.FighterStats;
using System;

namespace Taijj.ProjectGaia.Editor.StaticData.Fighters
{
	public class StatsEditor
	{
		#region Main
		public StatsEditor(SerializedObject target)
		{
			Target = target;
			StatsValuesProperty = Target.FindProperty("_stats").FindPropertyRelative("_values");
			ShowStats = true;
		}

		public void DrawStats()
		{
			Target.Update();
			TryInitializeStatValues();
			TryShowStats();
			Target.ApplyModifiedProperties();
		}

		private void TryInitializeStatValues()
		{
			if(StatsValuesProperty.arraySize != AllStats.Count)
			{
				StatsValuesProperty.arraySize = AllStats.Count;
			}
		}
		#endregion



		#region Stats Drawing
		private void TryShowStats()
		{
			ShowStats = EditorGUILayout.Foldout(ShowStats, "Stats");
			if(ShowStats)
			{
				DrawStatValues();
			}
		}

		private void DrawStatValues()
		{
			EditorGUI.indentLevel++;
			ListStatValueProperties();
			EditorGUI.indentLevel--;
		}

		private void ListStatValueProperties()
		{
			int statsEnumCount = Enum.GetValues(typeof(Stat)).Length;
			for(int i = 0; i < statsEnumCount; i++)
			{
				SerializedProperty valueProperty = StatsValuesProperty.GetArrayElementAtIndex(i);
				TryDrawValueProperty(valueProperty, i);
			}
		}

		private void TryDrawValueProperty(SerializedProperty valueProperty, int index)
		{
			try
			{
				EditorGUILayout.PropertyField(valueProperty, new GUIContent( ((Stat)index).ToString()) );
			}
			catch
			{
				// Okay, this is stupid! Since valueProperty is an array element it might also not be assigned in the inspector, i.e. it can be null.
				// BUT, SerializedProperty is a UnityEngine.Object and not a System.Object so Unity cannot check for null for some reason.
				// I tried several casts, and they all throw a strange error that has to do with casting from one Object to another.
				// If the Object gets Debug.Logged it will show "Null" but it is not really null for some reason. Very very strange.
				// Anyway, catching this strange Unity error is currently the only way I came up with to fix this issue.
			}
		}
		#endregion



		#region Properties
		private SerializedObject Target { get; set; }
		private SerializedProperty StatsValuesProperty { get; set; }
		private bool ShowStats { get; set; }

		private List<StatConfig> AllStats => Database.StatSection.StatConfigs;
		#endregion
	}
}