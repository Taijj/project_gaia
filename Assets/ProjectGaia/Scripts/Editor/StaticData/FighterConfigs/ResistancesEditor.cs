﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace Taijj.ProjectGaia.Editor.StaticData.Fighters
{
	public class ResistancesEditor
	{
		#region Lifecycle
		public ResistancesEditor(SerializedObject target)
		{
			Target = target;
			ResistancesList = new ReorderableList(Target, Target.FindProperty("_resistances"), true, false, true, true);
			ResistancesList.headerHeight = 7f;
			ResistancesList.onRemoveCallback += OnDeleteFromList;
			ResistancesList.drawElementCallback += OnDrawListItem;

			ResistancesAreShown = true;
		}

		public void OnDestoy()
		{
			if(ResistancesList == null)
			{
				return;
			}

			ResistancesList.onRemoveCallback -= OnDeleteFromList;
			ResistancesList.drawElementCallback -= OnDrawListItem;
		}
		#endregion



		#region Reorderable List
		private void OnDeleteFromList(ReorderableList list)
		{
			ReorderableList.defaultBehaviours.DoRemoveButton(list);
		}

		private void OnDrawListItem(Rect rect, int index, bool isActive, bool isFocused)
		{
			SerializedProperty resistanceProperty = ResistancesList.serializedProperty.GetArrayElementAtIndex(index);
			EditorGUI.PropertyField(rect, resistanceProperty);
		}

		public void DrawResistances()
		{
			ResistancesAreShown = EditorGUILayout.Foldout(ResistancesAreShown, new GUIContent("Resistances"));
			if(ResistancesAreShown)
			{
				DrawList();
			}
		}

		private void DrawList()
		{
			Target.Update();
			ResistancesList.DoLayoutList();
			Target.ApplyModifiedProperties();
		}
		#endregion



		#region Properties
		private SerializedObject Target { get; set; }
		private ReorderableList ResistancesList { get; set; }
		private bool ResistancesAreShown { get; set; }
		#endregion
	}
}