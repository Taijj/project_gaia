﻿using Taijj.ProjectGaia.StaticData;

namespace Taijj.ProjectGaia.Editor.StaticData
{
	public class DatabaseDependendEditor : UnityEditor.Editor
	{
		public virtual void OnEnable()
		{
			Database.Load();
		}
	}
}