using System;
using System.Collections.Generic;
using System.Linq;
using Taijj.ProjectGaia.StaticData;

namespace Taijj.ProjectGaia.DynamicData
{
    public class PartyModel
    {
        public PartyModel()
        {
            All = Database.FightersSection.Heroes.Select(c => new FighterModel(c)).ToList();
            Current = new List<FighterModel>();
        }

        public void Join(string heroConfigId)
        {
            FighterModel heroModel = All.First(h => h.Config.Id == heroConfigId);
            if(heroModel == null)
            {
                throw new Exception($"Hero with id {heroConfigId} does not exist!");
            }
            if(!Current.Contains(heroModel))
            {
                Current.Add(heroModel);
            }
        }

        public void Leave(string heroConfigId)
        {
            FighterModel heroModel = Current.First(h => h.Config.Id == heroConfigId);
            if(heroModel == null)
            {
                Logger.Log($"Hero with id {heroConfigId} is currently not in the party!");
                return;
            }
            Current.Remove(heroModel);
        }

        public void Reset()
        {
            All.ForEach(m => m.Reset());
            Current.Clear();
        }



        public List<FighterModel> All { get; private set; }
        public List<FighterModel> Current { get; private set; }
    }
}