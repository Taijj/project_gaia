﻿using System;
using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.Fighters;
using Taijj.ProjectGaia.StaticData.Skills;

namespace Taijj.ProjectGaia.DynamicData
{
    public class FighterModel
    {
        public FighterModel(FighterConfig config)
        {
            Config = config;
            DefaultStats = Config.Stats.Copy();
            Skills = new List<SkillConfig>();

            Config.ValidateRsistances();
            Reset();
        }

        public void Reset()
        {
            CurrentStats = DefaultStats.Copy();
            Skills.Clear();
        }

        public void Learn(SkillConfig skill)
        {
            if(!Config.Skills.Contains(skill))
            {
                throw new Exception($"{Config.name} cannot learn {skill.name}!");
            }
            if(!Skills.Contains(skill))
            {
                Skills.Add(skill);
            }
        }

        public void Forget(SkillConfig skill)
        {
            if(!Skills.Contains(skill))
            {
                return;
            }
            Skills.Remove(skill);
        }

        public Stats DefaultStats { get; private set; }
        public Stats CurrentStats { get; private set; }
        public List<SkillConfig> Skills { get; private set; }
        public FighterConfig Config { get; private set; }
    }
}