﻿namespace Taijj.ProjectGaia.DynamicData
{
    // DataModel is where all the dynamic data is.
    public static class DataModel
    {
        public static void Wake()
        {
            Party = new PartyModel();
        }

        public static PartyModel Party { get; private set; }
    }
}