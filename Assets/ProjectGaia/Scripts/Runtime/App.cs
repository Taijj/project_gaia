﻿using UnityEngine;
using Taijj.ProjectGaia.Input;
using Taijj.ProjectGaia.StaticData;
using Taijj.ProjectGaia.Battle;
using Taijj.ProjectGaia.DynamicData;
using Taijj.ProjectGaia.Utils.SceneSingletons;

namespace Taijj.ProjectGaia
{
    public class App : MonoBehaviour
    {
        [SerializeField] private Database _database;
        [SerializeField] private BattleStage _stage;

        public void Start()
        {
            if(UnitTestChecker.IsTest)
            {
                return;
            }

            WakeComponents();
            InitializeComponents();

            CoroutineRunner.Instance.WaitForSeconds(1f, StartBattles);
        }

        private void WakeComponents()
        {
            _database.AssignStaticProperties();
            DataModel.Wake();
            _stage.Wake();
        }

        private void InitializeComponents()
        {
            _stage.Initialize();
        }

        private void StartBattles()
        {
            PlayerInput.Instance.Enable();
            _stage.StartBattles();
        }
    }
}