﻿using Taijj.ProjectGaia.StaticData.Skills;
using Taijj.ProjectGaia.StaticData.FighterStats;
using UnityEngine;

namespace Taijj.ProjectGaia.StaticData
{
    // Database is where all the static game data is stored
    public class Database : ScriptableObject
    {
        [SerializeField] private StatsSection _statsSection;
        [SerializeField] private SkillsSection _skillsSection;
        [SerializeField] private FightersSection _fightersSection;

        #if UNITY_EDITOR
        public static void Load()
        {
            Database instance = (Database)UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/ProjectGaia/Database/Database.asset", typeof(Database));
            instance.AssignStaticProperties();
        }
        #endif

        public void AssignStaticProperties()
        {
            StatSection = _statsSection;
            SkillsSection = _skillsSection;
            FightersSection = _fightersSection;
        }

        public static StatsSection StatSection { get; private set; }
        public static SkillsSection SkillsSection { get; private set; }
        public static FightersSection FightersSection { get; private set; }
    }
}