﻿using System.Collections.Generic;
using Taijj.ProjectGaia.Attributes;
using Taijj.ProjectGaia.Battle.World.Layout;
using Taijj.ProjectGaia.Utils.Numerical;
using UnityEngine;

namespace Taijj.ProjectGaia.StaticData
{
    public class LineupConfig : ScriptableObject
    {
        [SerializeField] [Disabled] private Vector3Range _heroesLayoutPoints;
        [SerializeField] [Disabled] private List<PositionedEnemy> _positionedEnemies;

        public Vector3Range HeroesLayoutPoints
        {
            get => _heroesLayoutPoints;
            set => _heroesLayoutPoints = value;
        }

        public List<PositionedEnemy> PositionedEnemies
        {
            get => _positionedEnemies;
            set => _positionedEnemies = value;
        }
    }
}