﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Taijj.ProjectGaia.StaticData.Fighters;

namespace Taijj.ProjectGaia.StaticData.FighterStats
{
	[Serializable]
    public class FightersSection
    {
        [SerializeField] private List<FighterConfig> _heroes;

        public List<FighterConfig> Heroes => _heroes;
    }
}