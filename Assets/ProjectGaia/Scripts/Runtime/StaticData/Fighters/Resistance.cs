﻿using UnityEngine;
using System;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;

namespace Taijj.ProjectGaia.StaticData.Fighters
{
	[Serializable]
	public class Resistance
	{
		public enum Modification
		{
			Normal = 0,
			Double = 1,
			Strong = 2,
			Weak = 3,
			Nullify = 4,
			AbsorbHalf = 5,
			AbsorbFull = 6
		}

		[SerializeField] private FormConfig _form;
		[SerializeField] private Modification _modification;



		public FormConfig Form => _form;
		public Modification Mod => _modification;
	}
}