﻿using UnityEngine;
using System.Collections.Generic;
using Taijj.ProjectGaia.StaticData.Skills;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.Strategies;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;

namespace Taijj.ProjectGaia.StaticData.Fighters
{
	public class FighterConfig : ScriptableObject
	{
		#region ID
        private const string ID_BASE = "Hero.";
        public string Id { get { return ID_BASE + name; } }
        #endregion



        #region Injections
        [SerializeField] private string _displayName;
        [SerializeField] private Sprite _sprite;
        [SerializeField] private Color _color = Color.white;
        [HideInInspector] [SerializeField] private Stats _stats;
        [HideInInspector] [SerializeField] private List<Resistance> _resistances;
        [SerializeField] private List<SkillConfig> _skills;
        [SerializeField] private StrategyConfig _strategy;
		#endregion



        #region Tools
        public void ValidateRsistances()
        {
            List<FormConfig> foundForms = new List<FormConfig>();
            foreach(Resistance resistance in _resistances)
            {
                if(foundForms.Contains(resistance.Form))
                {
                    Logger.LogWarning($"{name} already has a resistance of Form {resistance.Form.name} injected. " +
                        "Non-unique forms will be ignored in skill calculation!");
                }
                foundForms.Add(resistance.Form);
            }
        }
        #endregion



        #region Properties
		public string DisplayName => _displayName;
        public Sprite Sprite => _sprite;
        public Color Color => _color;

        public Stats Stats { get { return _stats; } set { _stats = value; } }
        public List<Resistance> Resistances => _resistances;
        public List<SkillConfig> Skills => _skills;
        public StrategyConfig Strategy => _strategy;
        #endregion
	}
}