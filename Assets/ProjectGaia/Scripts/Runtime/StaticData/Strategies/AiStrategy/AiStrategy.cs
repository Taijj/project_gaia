﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Taijj.ProjectGaia.StaticData.Strategies.Ai
{
    public class AiStrategy : StrategyConfig
    {
        [SerializeField] private List<AiPhase> _phases;

        public void Execute()
        {
            if(_phases.Count == 0)
            {
                throw new Exception("Strategy has no phases!");
            }

            UpdatePhase();
            CurrentPhase.Execute();
        }

        private void UpdatePhase()
        {
            for(int i = _phases.Count-1; i >= 0; i--)
            {
                if(_phases[i].IsActivated())
                {
                    CurrentPhase = _phases[i];
                    break;
                }
            }
        }

        private AiPhase CurrentPhase { get; set; }
        public override bool IsControlledByPlayer { get { return false; } }
    }
}