using System;
using System.Collections.Generic;
using System.Linq;
using Taijj.ProjectGaia.StaticData.Strategies.Ai.Behaviors;
using Taijj.ProjectGaia.Utils.Numerical;
using UnityEngine;

namespace Taijj.ProjectGaia.StaticData.Strategies.Ai
{
    [Serializable]
    public class AiPhase
    {
        [SerializeField] private List<AiBehavior> _behaviors;

        public void Execute()
        {
            SelectBehavior().Execute();
        }

        private AiBehavior SelectBehavior()
        {
            List<AiBehavior> shuffledBehaviors = new List<AiBehavior>(_behaviors);
            shuffledBehaviors.Shuffle();

            int weightSum = shuffledBehaviors.Sum(b => b.Weight);
            int random = NumberUtils.GetRandomInclusive(0, weightSum-1);

            for(int i = 0; i < shuffledBehaviors.Count; i++)
            {
                AiBehavior behavior= shuffledBehaviors[i];
                if(random < behavior.Weight)
                {
                    return behavior;
                }
                random -= behavior.Weight;
            }
            throw new Exception("No Behavior was found!");
        }

        public bool IsActivated()
        {
            //TODO add configurable conditions.
            return true;
        }
    }
}