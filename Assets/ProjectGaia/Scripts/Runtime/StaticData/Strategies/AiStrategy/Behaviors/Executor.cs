using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.Flow;
using Taijj.ProjectGaia.Battle.Flow.Data;
using Taijj.ProjectGaia.Battle.Flow.Utils;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.Skills;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;
using Taijj.ProjectGaia.Utils.Numerical;

namespace Taijj.ProjectGaia.StaticData.Strategies.Ai.Behaviors
{
    public abstract class Executor
    {
        #region Main
        public virtual void Execute(AiBehavior behavior)
        {
            Behavior = behavior;

            Scheme scheme = new Scheme();
            scheme.selectedSkill = SelectSkill();
            scheme.targets = SelectTargets(scheme.selectedSkill);
            BattleData.CurrentScheme = scheme;
        }

        protected abstract SkillConfig SelectSkill();

        protected BattleData BattleData => BattleFlow.BattleData;
        protected AiBehavior Behavior { get; private set; }
        #endregion



        #region TargetsSelection
        protected List<Fighter> SelectTargets(SkillConfig selectedSkill)
        {
            Skill = selectedSkill;
            if(AimHelper.IsAutoAim(selectedSkill.Aim))
            {
                return GetAutoSelectedTargets();
            }
            return GetChosenTargets();
        }

        private List<Fighter> GetAutoSelectedTargets()
        {
            switch(Skill.Aim)
            {
                case AimKind.Self:  return BattleData.CurrentFighter.ToSingleList();
                default:            return GetFullRandomTargets();
            }
        }

        private List<Fighter> GetFullRandomTargets()
        {
            Fighter randomTarget = BattleData.Roster.AliveFighters.GetRandomElement();
            bool isSingle = NumberUtils.GetRandomInclusive(0, 100) <= 50;
            if(isSingle)
            {
                return randomTarget.ToSingleList();
            }
            return randomTarget.IsHero ? BattleData.Roster.AliveHeroes : BattleData.Roster.AliveEnemies;
        }

        private List<Fighter> GetChosenTargets()
        {
            switch(Skill.Aim)
            {
                case AimKind.Animation:  return GetSingleOrPartyTargets();
                case AimKind.Single:
                default:                 return GetSingleTargets();
            }
        }

        protected abstract List<Fighter> GetSingleTargets();
        protected abstract List<Fighter> GetPartyTargets();
        protected abstract List<Fighter> GetSingleOrPartyTargets();

        protected SkillConfig Skill { get; private set; }
        #endregion
    }
}