using System.Linq;
using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.Skills;

namespace Taijj.ProjectGaia.StaticData.Strategies.Ai.Behaviors
{
    public class FixedTargetExecutor : Executor
    {
        protected override SkillConfig SelectSkill()
        {
            return Behavior.Skill;
        }

        protected override List<Fighter> GetSingleTargets()
        {
            Fighter target = BattleData.Roster.AliveFighters.FirstOrDefault( f => f.Model.Config == Behavior.Target);
            if(target == null)
            {
                target = BattleData.CurrentFighter;
            }
            return target.ToSingleList();
        }

        protected override List<Fighter> GetPartyTargets()
        {
            if(BattleData.Roster.AliveFighters.Any(f => f.Model.Config == Behavior.Target))
            {
                return BattleData.Roster.AliveFighters.Where( f => f.Model.Config == Behavior.Target).ToList();
            }
            return BattleData.CurrentFighter.ToSingleList();
        }

        protected override List<Fighter> GetSingleOrPartyTargets()
        {
            return GetSingleTargets();
        }
    }
}