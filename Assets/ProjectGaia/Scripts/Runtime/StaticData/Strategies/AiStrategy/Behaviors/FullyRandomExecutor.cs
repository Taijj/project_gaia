using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.Flow.Data;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.Skills;
using Taijj.ProjectGaia.Utils.Numerical;

namespace Taijj.ProjectGaia.StaticData.Strategies.Ai.Behaviors
{
    public class FullyRandomExecutor : Executor
    {
        protected override SkillConfig SelectSkill()
        {
            return BattleData.CurrentFighter.Model.Skills.GetRandomElement();
        }

        protected override List<Fighter> GetSingleTargets()
        {
            return Skill.AimAtOpponentByDefault
                ? BattleData.Roster.AliveHeroes.GetRandomElement().ToSingleList()
                : BattleData.Roster.AliveEnemies.GetRandomElement().ToSingleList();
        }

        protected override List<Fighter> GetPartyTargets()
        {
            return Skill.AimAtOpponentByDefault ? BattleData.Roster.AliveHeroes : BattleData.Roster.AliveEnemies;
        }

        protected override List<Fighter> GetSingleOrPartyTargets()
        {
            bool isSingle = NumberUtils.GetRandomInclusive(0, 100) <= 50;
            return isSingle ? GetSingleTargets() : GetPartyTargets();
        }
    }
}