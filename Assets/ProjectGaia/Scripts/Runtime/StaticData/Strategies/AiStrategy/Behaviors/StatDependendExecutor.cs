using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.Skills;
using Taijj.ProjectGaia.StaticData.FighterStats;

namespace Taijj.ProjectGaia.StaticData.Strategies.Ai.Behaviors
{
    public class StatDependendExecutor : Executor
    {
        #region Main
        public enum Kind
        {
            HeighestAbsolute = 0,
            LowestAbsolute = 1,
            HeighestPercent = 2,
            LowestPercent = 3
        }

        protected override SkillConfig SelectSkill()
        {
            if(!BattleData.CurrentFighter.Model.Config.Skills.Contains(Behavior.Skill))
            {
                Logger.LogWarning($"{Behavior.Skill.name} is not in {BattleData.CurrentFighter.name}'s skill list!");
            }
            return Behavior.Skill;
        }

        protected override List<Fighter> GetSingleTargets()
        {
            Fighter target = Skill.AimAtOpponentByDefault ? GetFrom(BattleData.Roster.AliveHeroes) : GetFrom(BattleData.Roster.AliveEnemies);
            return target.ToSingleList();
        }

        protected override List<Fighter> GetPartyTargets()
        {
            return Skill.AimAtOpponentByDefault ? BattleData.Roster.AliveHeroes : BattleData.Roster.AliveEnemies;
        }

        protected override List<Fighter> GetSingleOrPartyTargets()
        {
            return GetSingleTargets();
        }
        #endregion



        #region Focus Target
        private Fighter GetFrom(List<Fighter> party)
        {
            Fighter result = party[0];
            foreach(Fighter fighter in party)
            {
                if(IsEligible(fighter, result))
                {
                    result = fighter;
                }
            }
            return result;
        }

        private bool IsEligible(Fighter currentFighter, Fighter currentResult)
        {
            switch(Behavior.StatDependendKind)
            {
                case Kind.HeighestAbsolute:     return IsHigherAbsolute(currentFighter, currentResult);
                case Kind.HeighestPercent:      return IsHigherFraction(currentFighter, currentResult);
                case Kind.LowestPercent:        return IsLowerFraction(currentFighter, currentResult);
                default:                        return IsLowerAbsolute(currentFighter, currentResult);
            }
        }

        private bool IsHigherAbsolute(Fighter f1, Fighter f2)
        {
            return f1.GetStat(Stat) > f2.GetStat(Stat);
        }

        private bool IsLowerAbsolute(Fighter f1, Fighter f2)
        {
            return f1.GetStat(Stat) < f2.GetStat(Stat);
        }

        private bool IsHigherFraction(Fighter f1, Fighter f2)
        {
            return f1.GetStatFraction(Stat) > f2.GetStatFraction(Stat);
        }

        private bool IsLowerFraction(Fighter f1, Fighter f2)
        {
            return f1.GetStatFraction(Stat) < f2.GetStatFraction(Stat);
        }

        private Stat Stat => Behavior.TargetStat;
        #endregion
    }
}