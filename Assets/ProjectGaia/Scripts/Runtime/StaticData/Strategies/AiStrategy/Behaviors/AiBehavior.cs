using System;
using Taijj.ProjectGaia.StaticData.Fighters;
using Taijj.ProjectGaia.StaticData.FighterStats;
using Taijj.ProjectGaia.StaticData.Skills;
using Taijj.ProjectGaia.Utils.Factories;
using UnityEngine;

namespace Taijj.ProjectGaia.StaticData.Strategies.Ai.Behaviors
{
    [Serializable]
    public class AiBehavior
    {
        #region Common
        public enum Kind
        {
            FullyRandom = 0,
            StatDependend = 1,
            FixedTarget = 2
        }

        [SerializeField] private int _weight = 1;
        [SerializeField] private Kind _kind;

        public void Execute()
        {
            if(Executor == null)
            {
                Executor = EnumReliantFactory<Executor>.Create(_kind);
            }
            Executor.Execute(this);
        }

        private Executor Executor { get; set; }
        public int Weight => _weight;
        #endregion



        #region Differently Used Injections
        [SerializeField] private StatDependendExecutor.Kind _statDependendKind;
        [SerializeField] private Stat _targetStat;
        [SerializeField] private SkillConfig _skill;
        [SerializeField] private FighterConfig _target;

        public StatDependendExecutor.Kind StatDependendKind => _statDependendKind;
        public Stat TargetStat => _targetStat;
        public SkillConfig Skill => _skill;
        public FighterConfig Target => _target;
        #endregion
    }
}