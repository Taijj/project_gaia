﻿namespace Taijj.ProjectGaia.StaticData.Strategies
{
    public class PlayerStrategy : StrategyConfig
    {
        public override bool IsControlledByPlayer { get { return true; } }
    }
}