﻿using UnityEngine;

namespace Taijj.ProjectGaia.StaticData.Strategies
{
    public abstract class StrategyConfig : ScriptableObject
    {
        public abstract bool IsControlledByPlayer { get; }
    }
}