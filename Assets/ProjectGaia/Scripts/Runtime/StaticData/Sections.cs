﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.FighterMovement;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;

namespace Taijj.ProjectGaia.StaticData
{
	[Serializable]
    public class SkillsSection
    {
        [SerializeField] private SkillGamesSection _gamesSection;
        [SerializeField] private SkillMovementSection _movementSection;

        public SkillGamesSection GamesSection => _gamesSection;
        public SkillMovementSection MovementSection => _movementSection;
    }

    [Serializable]
    public class SkillGamesSection
    {
        [SerializeField] private float _skillStrengthMultiplier;
        [SerializeField] private float _perfectTreshold;
        [SerializeField] private float _goodTreshold;
        [SerializeField] private float _badTreshold;

        public float PerfectTreshold => _perfectTreshold;
        public float GoodTreshold => _goodTreshold;
        public float BadTreshold => _badTreshold;
        public float MaxSkillStrengthMultiplier => _skillStrengthMultiplier;
    }

    [Serializable]
    public class SkillMovementSection
    {
        [HideInInspector] [SerializeField] private List<SkillMovementConfig> _movementConfigsByKind;

        public SkillMovementConfig GetConfig(SkillMovementKind kind)
        {
            return _movementConfigsByKind[(int)kind];
        }
    }
}