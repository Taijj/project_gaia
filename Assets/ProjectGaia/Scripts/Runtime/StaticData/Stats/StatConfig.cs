﻿using UnityEngine;

namespace Taijj.ProjectGaia.StaticData.FighterStats
{
    public class StatConfig : ScriptableObject
    {
        #region Injections
        [SerializeField] private Stat _stat;
        [SerializeField] private string _displayName;
        [SerializeField] private int _maxValue = 300;
        [SerializeField] private Sprite _icon;
        [SerializeField] private Color _color = Color.white;
        #endregion



        #region Properties
        public Stat Stat => _stat;
        public string DisplayName => _displayName;
        public int MaxValue => _maxValue;
        public Sprite Icon => _icon;
        public Color Color => _color;
        #endregion
    }
}