﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Taijj.ProjectGaia.StaticData.FighterStats
{
	[Serializable]
    public class StatsSection
    {
        [SerializeField] private List<StatConfig> _statConfigs;
        [SerializeField] private List<Regeneration> _regenerations;
        [SerializeField] private float _defendingRegenerationMultiplier;

        public StatConfig GetConfig(Stat stat)
        {
            return _statConfigs.First(c => c.Stat == stat);
        }

        public List<StatConfig> StatConfigs => _statConfigs;
        public List<Regeneration> Regenerations => _regenerations;
        public float DefendingRegenerationMultiplier => _defendingRegenerationMultiplier;
    }

    [Serializable]
    public class Regeneration
    {
        [SerializeField] private Stat _stat;
        [SerializeField] private int _amount;

        public Stat Stat => _stat;
        public int Amount => _amount;
    }
}