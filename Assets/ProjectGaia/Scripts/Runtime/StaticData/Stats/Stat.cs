using UnityEngine;

namespace Taijj.ProjectGaia.StaticData.FighterStats
{
    public enum Stat
    {
        Vitality = 0,   // Physical Health
        Sanity = 1,     // Mental Health

        Power = 2,      // Physical Strength
        Sturdiness = 3, // Physical Defense

        Temper = 4,     // Mental Strength
        Coolness = 5,   // Mental Defense

        Speed = 6,      // Defines Fighter Order
        Energy = 7,     // Ressource
        Proficiency = 8 // Determines Hardness of SkillGames
    }

    static class StatExtensions
    {
        public static Sprite Icon(this Stat stat)
        {
            return Database.StatSection.GetConfig(stat).Icon;
        }

        public static Color Color(this Stat stat)
        {
            return Database.StatSection.GetConfig(stat).Color;
        }

        public static int MaxValue(this Stat stat)
        {
            return Database.StatSection.GetConfig(stat).MaxValue;
        }
    }
}