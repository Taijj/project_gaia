using Taijj.ProjectGaia.StaticData.Skills.Appendages;

namespace Taijj.ProjectGaia.StaticData.Skills.Types
{
    public class AnalyzeSkillConfig : SkillConfig
    {
        public override SkillKind Kind => SkillKind.Analyze;
        public override bool AimAtOpponentByDefault => true;
    }
}