using System;
using System.Collections.Generic;
using Taijj.ProjectGaia.StaticData.FighterStats;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;
using UnityEngine;

namespace Taijj.ProjectGaia.StaticData.Skills.Types
{
    public class AnimationSkillConfig : SkillConfig, INumbersDisplaying
    {
        [Header("Animation Settings")]
        [SerializeField] private DamageSettings _damageSettings;
        [SerializeField] private HealingSettings _healingSettings;
        [SerializeField] private GameObject _allyHitPrefab;

        public DamageSettings DamageSettings => _damageSettings;
        public HealingSettings HealingSettings => _healingSettings;
        public GameObject AllyHitPrefab => _allyHitPrefab;
        public Stat TargetStat => _damageSettings.TargetStat;

        public override SkillKind Kind => SkillKind.Animate;
        public override bool AimAtOpponentByDefault => true;
    }

    [Serializable]
    public class DamageSettings : IDamaging
    {
        #region Configuration
        [SerializeField] private DamageConfiguration _configuration;

        public void SetForms(AnimationSkillConfig parent)
        {
            Forms = parent.Forms;
        }
        #endregion



        #region Interface Implementation
        public int Power => _configuration.Power;
        public float Might => _configuration.Might;
        public Stat TargetStat => _configuration.TargetStat;
        public Stat AttackStat => _configuration.AttackStat;
        public Stat DefenseStat => _configuration.DefenseStat;
        public List<FormConfig> Forms { get; private set; }
        #endregion
    }

    [Serializable]
    public class HealingSettings : IHealing
    {
        #region Configuration
        [SerializeField] private HealingConfiguration _configuration;

        public void SetForms(AnimationSkillConfig parent)
        {
            Forms = parent.Forms;
        }
        #endregion



        #region Interface Implementation
        public int Power => _configuration.Power;
        public float Might => _configuration.Might;
        public Stat TargetStat => _configuration.TargetStat;
        public Stat StrengthStat => _configuration.StrengthStat;
        public List<FormConfig> Forms { get; private set; }
        #endregion
    }
}