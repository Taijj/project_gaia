using Taijj.ProjectGaia.StaticData.FighterStats;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;
using UnityEngine;

namespace Taijj.ProjectGaia.StaticData.Skills.Types
{
    public class HealSkillConfig : SkillConfig, INumbersDisplaying, IHealing
    {
        [Header("Healing Settings")]
        [SerializeField] HealingConfiguration _configuration;

        public int Power => _configuration.Power;
        public float Might => _configuration.Might;
        public Stat TargetStat => _configuration.TargetStat;
        public Stat StrengthStat => _configuration.StrengthStat;

        public override SkillKind Kind => SkillKind.Heal;
        public override bool AimAtOpponentByDefault => false;
    }
}