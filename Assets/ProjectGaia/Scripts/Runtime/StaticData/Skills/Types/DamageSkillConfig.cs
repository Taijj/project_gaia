using Taijj.ProjectGaia.StaticData.FighterStats;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;
using UnityEngine;

namespace Taijj.ProjectGaia.StaticData.Skills.Types
{
    public class DamageSkillConfig : SkillConfig, INumbersDisplaying, IDamaging
    {
        [Header("Damaging Settings")]
        [SerializeField] DamageConfiguration _configuration;

        public int Power => _configuration.Power;
        public float Might => _configuration.Might;
        public Stat TargetStat => _configuration.TargetStat;
        public Stat AttackStat => _configuration.AttackStat;
        public Stat DefenseStat => _configuration.DefenseStat;
        public override SkillKind Kind => SkillKind.Damage;
        public override bool AimAtOpponentByDefault => true;
    }
}