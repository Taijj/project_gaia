using Taijj.ProjectGaia.StaticData.FighterStats;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;
using UnityEngine;

namespace Taijj.ProjectGaia.StaticData.Skills.Types
{
    public class DefendSkillConfig : SkillConfig
    {
        [Header("Defending Settings")]
        [SerializeField] private Stat _defenseStat;
        [SerializeField] [Range(0f, 10f)] private float _defenseMultiplier = 1f;

        public Stat DefenseStat { get { return _defenseStat; } }
        public float DefenseMultiplier { get { return _defenseMultiplier; } }
        public override SkillKind Kind => SkillKind.Defend;
        public override bool AimAtOpponentByDefault => false;
    }
}