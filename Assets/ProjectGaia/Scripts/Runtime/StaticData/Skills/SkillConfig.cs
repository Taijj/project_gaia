﻿using System.Collections.Generic;
using UnityEngine;
using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.FighterMovement;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;

namespace Taijj.ProjectGaia.StaticData.Skills
{
    public abstract class SkillConfig : ScriptableObject
    {
        #region Injections
        [SerializeField] private string _displayName;
        [SerializeField] private string _description;
        [SerializeField] private GameObject _hitPrefab;
        [SerializeField] private AimKind _aim;
        [SerializeField] private SkillMovementKind _skillMovement;
        [SerializeField] private List<FormConfig> _forms;
        [SerializeField] private SkillCost _cost;
        [SerializeField] private GameObject _gamePrefab;
        #endregion



        #region Properties
        public string DisplayName => _displayName;
        public string Description => _description;
        public virtual GameObject HitPrefab => _hitPrefab;
        public AimKind Aim => _aim;
        public SkillMovementKind SkillMovement => _skillMovement;
        public List<FormConfig> Forms => _forms;

        public SkillCost Cost => _cost;
        public bool HasCost => _cost.Amount > 0;

        public GameObject GamePrefab => _gamePrefab;
        public bool HasGame => _gamePrefab != null;

        public abstract SkillKind Kind { get; }
        public abstract bool AimAtOpponentByDefault { get; }
        #endregion
    }
}