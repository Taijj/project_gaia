﻿using UnityEngine;

namespace Taijj.ProjectGaia.StaticData.Skills.Appendages
{
	public class FormConfig : ScriptableObject
	{
		[SerializeField] private string _displayName;
		[SerializeField] private Sprite _icon;
		[SerializeField] private Color _color = Color.white;
		[SerializeField] private int _textSpriteIndex;

		public string DisplayName => _displayName;
		public Sprite Icon => _icon;
		public Color Color => _color;
	}
}