﻿using UnityEngine;

namespace Taijj.ProjectGaia.StaticData.Skills.Appendages
{
	public class SkillMovementConfig : ScriptableObject
	{
		[Tooltip("Curve that defines the easing of the 'to' movement Tween.")]
		[SerializeField] private AnimationCurve _inCurve;
		[Tooltip("Curve that defines the easing of the 'back' movement Tween.")]
		[SerializeField] private AnimationCurve _outCurve;
		[Tooltip("Defines how long the 'to' movement will take.")]
		[SerializeField] private float _inSeconds;
		[Tooltip("Defines how long the 'back' movement will take.")]
		[SerializeField] private float _outSeconds;
		[Tooltip("This is used differently for different Movementkinds. It generally stands for the distance to the Tween target position.")]
		[SerializeField] private float _verticalOffset;

		public AnimationCurve InCurve => _inCurve;
		public AnimationCurve OutCurve => _outCurve;
		public float InSeconds => _inSeconds;
		public float OutSeconds => _outSeconds;
		public float VerticalOffset => _verticalOffset;
	}
}