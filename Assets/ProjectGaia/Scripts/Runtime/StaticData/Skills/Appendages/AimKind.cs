namespace Taijj.ProjectGaia.StaticData.Skills.Appendages
{
    public enum AimKind
    {
        Single = 0,
        Animation = 1,
        Self = 2
    }
}