﻿using System;
using Taijj.ProjectGaia.StaticData.FighterStats;
using UnityEngine;

namespace Taijj.ProjectGaia.StaticData.Skills.Appendages
{
	[Serializable]
	public class SkillCost
	{
		public enum DueMoment
		{
			AfterSkillSelection = 0,
			AtTurnEnd = 1
		}

		[SerializeField] private Stat _stat;
		[SerializeField] private int _amount;
		[SerializeField] DueMoment _momentOfPay;
		[SerializeField] bool _allowOvercheck;

		public Stat Stat => _stat;
		public int Amount => _amount;
		public DueMoment MomentOfPay => _momentOfPay;
		public bool AllowOvercheck => _allowOvercheck;
	}
}