using System;
using Taijj.ProjectGaia.StaticData.FighterStats;
using UnityEngine;

namespace Taijj.ProjectGaia.StaticData.Skills.Appendages
{
    [Serializable]
    public class HealingConfiguration
    {
       [SerializeField] private int _power;
        [SerializeField] [Range(1f, 10f)] private float _might = 1f;
        [SerializeField] private Stat _targetStat;
        [SerializeField] private Stat _strengthStat;

        public int Power => _power;
        public float Might => _might;
        public Stat TargetStat => _targetStat;
        public Stat StrengthStat => _strengthStat;
    }
}