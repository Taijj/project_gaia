namespace Taijj.ProjectGaia.StaticData.Skills.Appendages
{
    public enum SkillKind
    {
        Damage = 0,
        Heal = 1,
        Animate = 2,
        Defend = 3,
        Analyze = 4
    }
}