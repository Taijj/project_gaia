using System.Collections.Generic;
using Taijj.ProjectGaia.StaticData.FighterStats;

namespace Taijj.ProjectGaia.StaticData.Skills.Appendages
{
    public interface INumbersDisplaying
    {
        Stat TargetStat { get; }
    }

    public interface IDamaging
    {
        int Power { get; }
        float Might { get; }
        Stat TargetStat { get; }
        Stat AttackStat { get; }
        Stat DefenseStat { get; }
        List<FormConfig> Forms { get; }
    }

    public interface IHealing
    {
        int Power { get; }
        float Might { get; }
        Stat TargetStat { get; }
        Stat StrengthStat { get; }
        List<FormConfig> Forms { get; }
    }
}