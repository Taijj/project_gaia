using System;
using Taijj.ProjectGaia.StaticData.FighterStats;
using UnityEngine;

namespace Taijj.ProjectGaia.StaticData.Skills.Appendages
{
    [Serializable]
    public class DamageConfiguration
    {
        [SerializeField] private int _power;
        [SerializeField] [Range(1f, 10f)] private float _might = 1f;
        [SerializeField] private Stat _targetStat;
        [SerializeField] private Stat _attackStat;
        [SerializeField] private Stat _defenseStat;

        public int Power => _power;
        public float Might => _might;
        public Stat TargetStat => _targetStat;
        public Stat AttackStat =>_attackStat;
        public Stat DefenseStat => _defenseStat;
    }
}