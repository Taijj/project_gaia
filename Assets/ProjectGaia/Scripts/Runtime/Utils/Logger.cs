﻿using System.Collections.Generic;
using UnityEngine;

namespace Taijj.ProjectGaia
{
    public class Logger
    {
        public static void Log(object o)
        {
            #if UNITY_EDITOR
                Debug.Log(o);
            #endif
        }

        public static void LogError(object o)
        {
            #if UNITY_EDITOR
                Debug.LogError(o);
            #endif
        }

        public static void LogWarning(object o)
        {
            #if UNITY_EDITOR
                Debug.LogWarning(o);
            #endif
        }



        public static void LogList<T> (List<T> list, string listName)
        {
            #if UNITY_EDITOR
                string log = "Logging list " + listName;
                list.ForEach(e => log += "\n"+e);
                Debug.Log(log);
            #endif
        }

        public static void LogHierarchy(Component component)
        {
            #if UNITY_EDITOR
                string log = "Logging Object Hierarchy " + component.gameObject.name + "\n";
                Transform current = component.transform;

                WhileCounter.OnWhileLoopStart(20);
                do
                {
                    WhileCounter.OnWhileLoopIteration();

                    log += current.gameObject.name + "/";
                    current = current.parent;
                }
                while(current.parent != null);
                Debug.Log(log);
            #endif
        }
    }
}