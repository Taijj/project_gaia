﻿using UnityEngine;

namespace Taijj.ProjectGaia
{
    public static class ExtensionMethods
    {
        public static Color CloneAndSetA(this Color color, float newA)
        {
            return new Color(color.r, color.g, color.b, newA);
        }

        public static void DestructAndNullify(this MonoBehaviour behaviour)
        {
            GameObject.Destroy(behaviour.gameObject);
            behaviour = null;
        }

        public static string ToTitleCase(this string s)
        {
            if(s.Length == 0)
            {
                return "";
            }

            string firstChar = s[0].ToString();
            return firstChar.ToUpper() + s.Substring(1);
        }
    }
}