﻿using UnityEngine;

namespace Taijj.ProjectGaia
{
    public static class VectorExtensionMethods
    {
        public static Vector3 ToVector3(this Vector2 vector)
        {
            return new Vector3(vector.x, vector.y, 0f);
        }

        public static Vector3 CloneAndSetX(this Vector3 vector, float newX)
        {
            return new Vector3(newX, vector.y, vector.z);
        }

        public static Vector3 CloneAndSetY(this Vector3 vector, float newY)
        {
            return new Vector3(vector.x, newY, vector.z);
        }

        public static Vector3 CloneAndSetZ(this Vector3 vector, float newZ)
        {
            return new Vector3(vector.x, vector.y, newZ);
        }

        public static Vector2 CloneAndSetX(this Vector2 vector, float newX)
        {
            return new Vector2(newX, vector.y);
        }

        public static Vector2 CloneAndSetY(this Vector2 vector, float newY)
        {
            return new Vector2(vector.x, newY);
        }
    }
}