﻿using System.Linq;
using System;
using System.Collections.Generic;
using UnityEngine;
using Taijj.ProjectGaia.Utils.Numerical;

namespace Taijj.ProjectGaia
{
    public static class CollectionExtensionMethods
    {
        #region Lists
        public static T GetRandomElement<T>(this List<T> list)
        {
            return list[ GetRandomIndex(list.Count) ];
        }

        public static List<T> Copy<T>(this List<T> list)
        {
            return new List<T>(list);
        }

        public static void Log<T>(this List<T> list, string listName = "")
        {
            string log = listName + "\n";
            for(int i = 0; i < list.Count; i++)
            {
                log += String.Format("[{0}] - {1}\n", i, list[i]);
            }
            Logger.Log(log);
        }

        public static void DestroyGameObjects<T>(this List<T> list) where T : MonoBehaviour
        {
            while(list.Count > 0)
            {
                T element = list[0];
                list.RemoveAt(0);
                element.DestructAndNullify();
            }
        }

        //by Smooth-P: https://forum.unity.com/threads/clever-way-to-shuffle-a-list-t-in-one-line-of-c-code.241052/
        public static void Shuffle<T>(this List<T> list)
        {
            var count = list.Count;
            var last = count - 1;
            for (var i = 0; i < last; ++i)
            {
                var r = UnityEngine.Random.Range(i, count);
                var tmp = list[i];
                list[i] = list[r];
                list[r] = tmp;
            }
        }
        #endregion



        #region Arrays
        public static T GetRandomElement<T>(this T[] array)
        {
            return array[ GetRandomIndex(array.Length) ];
        }

        public static T GetRandomElement<T>(this Array array)
        {
            return (T)array.GetValue( GetRandomIndex(array.Length) );
        }

        public static void ForEach<T>(this T[] array, Action<T> action)
        {
            foreach(T item in array)
            {
                action(item);
            }
        }
        #endregion



        #region Misc
        public static void For<T>(this IEnumerable<T> sequence, Action<int, T> action)
        {
            int i = 0;
            foreach(T item in sequence)
            {
                action(i, item);
                i++;
            }
        }

        public static void DestroyGameObjects<T, U>(this Dictionary<T, U> dictionary) where U : MonoBehaviour
        {
            while(dictionary.Count > 0)
            {
                KeyValuePair<T,U> element = dictionary.First();
                dictionary.Remove(element.Key);
                element.Value.DestructAndNullify();
            }
        }

        private static int GetRandomIndex(int listLength)
        {
            if(listLength == 0)
            {
                throw new Exception("List has no elements!");
            }
            return NumberUtils.GetRandomInclusive(0, listLength-1);
        }
        #endregion
    }
}