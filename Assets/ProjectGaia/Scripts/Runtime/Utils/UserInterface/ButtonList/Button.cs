﻿using UnityEngine;

namespace Taijj.ProjectGaia.Utils.UI.Buttons
{
	[RequireComponent(typeof(Animator))]
	public class Button : UiElement
	{

		#region Init
		private const string UP_TRIGGER = "Up";
		private const string PRESS_TRIGGER = "Press";
		private const string SELECT_TRIGGER = "Select";
		private const string DISABLED_BOOL = "Disabled";

		[SerializeField] private bool _pressable = true;

		public virtual void Wake()
		{
			Animator = gameObject.GetComponent<Animator>();
			Animator.SetBool(DISABLED_BOOL, _pressable);
		}
		#endregion



		#region Main
		public virtual void Select()
		{
			ResetTriggers();
			RectTransform.SetAsFirstSibling();
			Animator.SetTrigger(SELECT_TRIGGER);
		}

		public virtual void Deselect()
		{
			ResetTriggers();
			Animator.SetTrigger(UP_TRIGGER);
		}

		public virtual void Press()
		{
			ResetTriggers();
			Animator.SetTrigger(PRESS_TRIGGER);
		}

		private void ResetTriggers()
		{
			Animator.ResetTrigger(SELECT_TRIGGER);
			Animator.ResetTrigger(UP_TRIGGER);
			Animator.ResetTrigger(PRESS_TRIGGER);
		}

		private Animator Animator { get; set; }
		#endregion



		#region Disabling
		public void SetPressable(bool value, bool updateGraphics = false)
		{
			_pressable = value;
			Animator.SetBool(DISABLED_BOOL, !_pressable);

			if(updateGraphics)
			{
				Animator.SetTrigger(UP_TRIGGER);
			}
		}

		public bool Pressable => _pressable;
		#endregion
	}
}