﻿using System;
using System.Collections.Generic;
using Taijj.ProjectGaia.Utils.Factories;
using UnityEngine;

namespace Taijj.ProjectGaia.Utils.UI.Buttons
{
	[Serializable]
	public class ButtonPool<T> where T:DataButton
	{
		#region Main
		public ButtonPool(GameObject buttonPrefab, Transform buttonsContainer)
		{
			ButtonPrefab = buttonPrefab;
			ButtonsContainer = buttonsContainer;

			UsedButtons = new List<T>();
			UnusedButtons = new List<T>();
		}

		public void PoolButtons(int neededButtonsCount)
		{
			NeededCount = neededButtonsCount;

			if(UsedButtons.Count < NeededCount)
			{
				StockUpOnButtons();
			}
			if(UsedButtons.Count > NeededCount)
			{
				PutButtonsAway();
			}
		}
		#endregion



		#region Pooling
		private void StockUpOnButtons()
		{
			TakeButtonsFromPool();
			CreateNewButtons();
		}

		private void TakeButtonsFromPool()
		{
			while(UnusedButtons.Count != 0)
			{
				T button = UnusedButtons[0];
				button.Active = true;

				UsedButtons.Add(button);
				UnusedButtons.RemoveAt(0);
			}
		}

		private void CreateNewButtons()
		{
			while(UsedButtons.Count < NeededCount)
			{
				T button = GameObjectFactory.CreateAs<T>(ButtonPrefab, ButtonsContainer, "Button" + UsedButtons.Count+1);
				button.Wake();
				UsedButtons.Add(button);
			}
		}



		private void PutButtonsAway()
		{
			while(UsedButtons.Count > NeededCount)
			{
				T button = UsedButtons[0];
				button.Active = false;

				UnusedButtons.Add(button);
				UsedButtons.Remove(button);
			}
		}
		#endregion



		#region Properties
		private int NeededCount { get; set; }
		public List<T> UsedButtons { get; set; }
		private List<T> UnusedButtons { get; set; }

		private GameObject ButtonPrefab { get; set; }
		private Transform ButtonsContainer { get; set; }
		#endregion
	}
}