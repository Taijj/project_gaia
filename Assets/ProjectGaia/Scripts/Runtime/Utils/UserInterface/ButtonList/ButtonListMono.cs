﻿using System;
using System.Collections.Generic;
using Taijj.ProjectGaia.Utils.Numerical;
using UnityEngine;

namespace Taijj.ProjectGaia.Utils.UI.Buttons
{
	#region Monobehaviour
	public class ButtonListMono : UiElement
	{
		[SerializeField] private GameObject _buttonPrefab;

		public ButtonList<T> Create<T>() where T: DataButton
		{
			return new ButtonList<T>(this);
		}

		public GameObject ButtonPrefab { get { return _buttonPrefab; } }
	}
	#endregion



	#region Generic Class
	public class ButtonList<T> where T:DataButton
	{
		#region Fields
		public enum Direction
		{
			Vertical = 0,
			Horizontal = 1
		}

		public Direction direction;
		public float buttonsMargin;
		#endregion



		#region Main
		public ButtonList(ButtonListMono mono)
		{
			Mono = mono;

			ButtonPool = new ButtonPool<T>(Mono.ButtonPrefab, Mono.transform);
			Layouter = new ButtonListLayouter<T>();
			Layouter.Margin = buttonsMargin;
		}

		public void Layout(List<ButtonData> buttonDataSet)
		{
			DataSet = buttonDataSet;
			ButtonPool.PoolButtons(DataSet.Count);
			Layouter.Layout(ButtonPool.UsedButtons, direction);
			UpdateSize();
			Feed();
		}

		private void Feed()
		{
			for(int i = 0; i < DataSet.Count; i++)
			{
				ButtonPool.UsedButtons[i].Feed(DataSet[i]);
			}
		}
		#endregion



		#region Size
		private void UpdateSize()
        {
            switch(direction)
			{
				case Direction.Vertical: SetSizeVertically(); break;
				case Direction.Horizontal: SetSizeHorizontally(); break;
			}
        }

		private void SetSizeVertically()
		{
			Mono.Height = Layouter.TotalHeight;
			Mono.Width = Layouter.SingleButtonWidth;
		}

		private void SetSizeHorizontally()
		{
			Mono.Width = Layouter.TotalWidth;
			Mono.Height = Layouter.SingleButtonHeight;
		}

		public float GetWidth() { return Mono.Width; }
		public float GetHeight() { return Mono.Height; }
		#endregion



		#region Selection
		public void SelectButton(T button)
		{
			SelectButtonAt( ButtonPool.UsedButtons.IndexOf(button) );
		}

		private void SelectButtonAt(int index)
		{
			ButtonPool.UsedButtons.ForEach( b => b.Deselect());
			SelectedButtonIndex = NumberUtils.FitIntoRangeLooped(index, 0, ButtonPool.UsedButtons.Count-1);
			ButtonPool.UsedButtons[SelectedButtonIndex].Select();
		}


		public void SelectNextButton()
		{
			SelectButtonAt(SelectedButtonIndex+1);
		}

		public void SelectPreviousButton()
		{
			SelectButtonAt(SelectedButtonIndex-1);
		}



		public T GetSelectedButton()
		{
			return ButtonPool.UsedButtons[SelectedButtonIndex];
		}

		public int SelectedButtonIndex { get; private set; }
		#endregion



		#region Properties
		private ButtonListMono Mono { get; set; }
		private ButtonPool<T> ButtonPool { get; set; }
		private ButtonListLayouter<T> Layouter { get; set; }
		private List<ButtonData> DataSet { get; set; }

		public List<T> UsedButtons { get { return ButtonPool.UsedButtons; } }
		#endregion
	}
	#endregion
}