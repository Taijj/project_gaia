﻿using System.Collections.Generic;
using UnityEngine;

namespace Taijj.ProjectGaia.Utils.UI.Buttons
{
	public class ButtonListLayouter<T> where T:DataButton
	{
		#region Beginning
		public void Layout(List<T> buttons, ButtonList<T>.Direction direction)
		{
			if(buttons.Count == 0)
			{
				return;
			}
			Buttons = buttons;

			GetSingleButtonSize();
			LayoutButtons(direction);
		}

		private void GetSingleButtonSize()
		{
			RectTransform blueprintTransform = Buttons[0].RectTransform;
			SingleButtonHeight = blueprintTransform.rect.height;
			SingleButtonWidth = blueprintTransform.rect.width;
		}
		#endregion



		#region Layouting
		private void LayoutButtons(ButtonList<T>.Direction direction)
		{
			switch(direction)
			{
				case ButtonList<T>.Direction.Vertical: LayoutVertically(); break;
				case ButtonList<T>.Direction.Horizontal: LayoutHorizontally(); break;
			}
		}

		private void LayoutVertically()
		{
			for(int i = 0; i < Buttons.Count; i++)
			{
				Buttons[i].LocalPosition = Vector3.down * i * (SingleButtonHeight + Margin);
			}
		}

		private void LayoutHorizontally()
		{
			for(int i = 0; i < Buttons.Count; i++)
			{
				Buttons[i].LocalPosition = Vector3.right * i * (SingleButtonWidth + Margin);
			}
		}
		#endregion



		#region Properties
		private List<T> Buttons { get; set; }

		public float SingleButtonHeight { get; set; }
		public float SingleButtonWidth { get; set; }
		public float Margin { private get; set; }
		public float TotalHeight { get { return (SingleButtonHeight + Margin) * Buttons.Count; } }
		public float TotalWidth { get { return (SingleButtonWidth + Margin) * Buttons.Count; } }
		#endregion
	}
}