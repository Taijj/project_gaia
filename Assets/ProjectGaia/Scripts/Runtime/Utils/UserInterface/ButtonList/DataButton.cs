﻿namespace Taijj.ProjectGaia.Utils.UI.Buttons
{
	public abstract class DataButton : Button
	{
		public void Feed(ButtonData data)
		{
			Data = data;
			UpdateDisplay();
		}

		protected abstract void UpdateDisplay();

		protected ButtonData Data { get; private set; }
	}

	public abstract class ButtonData
	{}
}