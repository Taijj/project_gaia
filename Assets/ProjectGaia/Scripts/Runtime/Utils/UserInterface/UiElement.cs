﻿using UnityEngine;

namespace Taijj.ProjectGaia.Utils.UI
{
	public class UiElement : MonoBehaviour
	{
		public bool Active
		{
			get { return gameObject.activeInHierarchy; }
			set { gameObject.SetActive(value); }
		}



		public RectTransform RectTransform { get { return gameObject.GetComponent<RectTransform>(); } }

		public Vector3 LocalPosition
		{
			get { return transform.localPosition; }
			set { transform.localPosition = value; }
		}

		public Vector3 GlobalPosition
		{
			get { return transform.position; }
			set { transform.position = value; }
		}



		public float Width
		{
			get { return RectTransform.rect.width; }
			set { RectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, value); }
		}

		public float Height
		{
			get { return RectTransform.rect.height; }
			set { RectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, value); }
		}



		public static void Mirror(Transform transform)
		{
			transform.localScale = Vector3.one.CloneAndSetX(-1);
		}
		public static void Demirror(Transform transform)
		{
			transform.localScale = Vector3.one.CloneAndSetX(1);
		}
	}
}