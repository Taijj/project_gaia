﻿using UnityEngine;
using UnityEngine.UI;

namespace Taijj.ProjectGaia.Utils.UI
{
	public class Progressbar : UiElement
	{
		[SerializeField] private Transform _fillTransform;

		public float Progress
		{
			get { return _fillTransform.localScale.x; }
			set { _fillTransform.localScale = Vector3.one.CloneAndSetX(value); }
		}

		public Color Color
		{
			set
			{
				Image fillImage = _fillTransform.GetComponent<Image>();
				fillImage.color = value;
			}
		}
	}
}