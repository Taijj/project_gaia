﻿using System;
using Taijj.ProjectGaia.Utils.SceneSingletons;
using UnityEngine;

namespace Taijj.ProjectGaia
{
	public class ScreenSizeObserver : SceneSingleton<ScreenSizeObserver>
	{
		public event Action OnScreenSizeChange;
		private Vector2 LastScreenSize;

		protected override void Wake()
		{
			base.Wake();
			LastScreenSize = new Vector2(Screen.width, Screen.height);
			UpdateCaller.Instance.StartCallingOnUpdate(ScreenUpdate);
		}

		private void ScreenUpdate()
		{
			Vector2 screenSize = new Vector2(Screen.width, Screen.height);
			if(LastScreenSize != screenSize)
			{
				OnScreenSizeChange?.Invoke();
				LastScreenSize = screenSize;
			}
		}
	}
}