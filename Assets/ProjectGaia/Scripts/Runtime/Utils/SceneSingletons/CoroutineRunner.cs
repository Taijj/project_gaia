﻿using System.Collections;
using UnityEngine;
using System;

namespace Taijj.ProjectGaia.Utils.SceneSingletons
{
    public class CoroutineRunner : SceneSingleton<CoroutineRunner>
    {
        #region WaitForSeconds
        public Coroutine WaitForSeconds(float seconds, Action method)
        {
            return StartCoroutine(WaitForSecondsInternal(seconds, method));
        }

        private IEnumerator WaitForSecondsInternal(float seconds, Action method)
        {
            yield return new WaitForSecondsRealtime(seconds);
            method();
        }
        #endregion



        #region Animations
        public Coroutine WaitForAnimationCompletion(AnimationClip clip, Action method)
        {
            return WaitForSeconds(clip.length, method);
        }

        public Coroutine WaitForAnimationEvent(AnimationClip clip, int eventIndex, Action method)
        {
            return WaitForSeconds(clip.events[eventIndex].time, method);
        }
        #endregion



        public Coroutine StartCoroutine(Func<IEnumerator> method)
        {
            return StartCoroutine(method());
        }
    }
}