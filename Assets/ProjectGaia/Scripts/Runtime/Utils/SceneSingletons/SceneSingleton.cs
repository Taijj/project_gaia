﻿using UnityEngine;

namespace Taijj.ProjectGaia.Utils.SceneSingletons
{
    public class SceneSingleton<T> : MonoBehaviour where T: SceneSingleton<T>
    {
        protected virtual void Wake()
        {
            IsAwake = true;
        }

        public static T Instance
        {
            get
            {
                TrySetFromScene();
                TryCreateAndSet();
                TryWaking();
                return InstanceInternal;
            }

            private set
            {
                InstanceInternal = value;
            }
        }

        private static void TrySetFromScene()
        {
            if(InstanceInternal == null)
            {
                InstanceInternal = GameObject.FindObjectOfType<T>();
            }
        }

        private static void TryCreateAndSet()
        {
            if(InstanceInternal == null)
            {
                GameObject newObject = new GameObject(typeof(T).ToString());
                InstanceInternal = newObject.AddComponent<T>();
            }
        }

        private static void TryWaking()
        {
            if(!IsAwake)
            {
                InstanceInternal.Wake();
            }
        }

        private static T InstanceInternal { get; set;}
        private static bool IsAwake { get; set; }
    }
}