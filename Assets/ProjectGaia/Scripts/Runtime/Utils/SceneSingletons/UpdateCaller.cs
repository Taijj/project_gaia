﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;

namespace Taijj.ProjectGaia.Utils.SceneSingletons
{
	public class UpdateCaller : SceneSingleton<UpdateCaller>
	{
		#region Main
		protected override void Wake()
		{
			base.Wake();
			UpdateCalls = new List<Action>();
			_registeredCallers = new List<string>();
		}

		public void Update()
		{
			if(UpdateCalls == null)
			{
				return;
			}

			List<Action> currentUpdates = UpdateCalls.Copy();
			currentUpdates.ForEach(a => a());
		}

		private List<Action> UpdateCalls { get; set; }
		#endregion



		#region Update
		public void StartCallingOnUpdate(Action method)
		{
			if(UpdateCalls.Contains(method))
			{
				return;
			}

			RegisterCaller(method);
			UpdateCalls.Add(method);
		}

		public void StopCallingOnUpdate(Action method)
		{
			if(UpdateCalls.Contains(method))
			{
				UpdateCalls.Remove(method);
				DeregisterCaller(method);
			}
		}
		#endregion



		#region Debug
		[SerializeField] private List<string> _registeredCallers;

		private void RegisterCaller(Action method)
		{
			#if UNITY_EDITOR
				MethodInfo info = method.Method;
				string register = info.DeclaringType.ToString() + info.Name;
				_registeredCallers.Add(register);
			#endif
		}

		private void DeregisterCaller(Action method)
		{
			#if UNITY_EDITOR
				MethodInfo info = method.Method;
				string register = info.DeclaringType.ToString() + info.Name;
				_registeredCallers.Remove(register);
			#endif
		}
		#endregion
	}
}