﻿using System.Diagnostics;
using System;
using Taijj.ProjectGaia.Utils.SceneSingletons;

namespace Taijj.ProjectGaia.Utils.Time
{
    public class Timer
    {
        public event Action OnCompleted;
        public event Action OnTick;

        public Timer(float seconds)
        {
            TotalSeconds = seconds;
        }

        public void Start()
        {
            Watch = Stopwatch.StartNew();
            UpdateCaller.Instance.StartCallingOnUpdate(Tick);
        }

        public void Stop()
        {
            if(Watch == null)
            {
                return;
            }

            UpdateCaller.Instance.StopCallingOnUpdate(Tick);
            Watch.Stop();
            Watch = null;
        }

        private void Tick()
        {
            if(Watch == null)
            {
                return;
            }

            OnTick?.Invoke();

            if(ElapsedSeconds >= TotalSeconds)
            {
                Stop();
                OnCompleted?.Invoke();
            }
        }


        private Stopwatch Watch { get; set; }
        private float ElapsedSeconds => Watch.ElapsedMilliseconds/1000f;
        private float TotalSeconds { get; set; }
        public float ElapsedNormalized => ElapsedSeconds/TotalSeconds;
    }
}