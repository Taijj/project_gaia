﻿using System;

namespace Taijj.ProjectGaia.Utils.Numerical
{
    [Serializable]
    public struct IntRange
    {
        public int min;
        public int max;
    }

    [Serializable]
    public struct FloatRange
    {
        public float min;
        public float max;

        public float Delta { get { return max - min; } }
        public float Middle { get { return min + Delta/2f; } }
    }

    [Serializable]
    public struct Vector3Range
    {
        public UnityEngine.Vector3 min;
        public UnityEngine.Vector3 max;

        public float Distance { get { return UnityEngine.Vector3.Distance(min, max); } }
        public UnityEngine.Vector3 Middle { get { return min + (max-min)/2f; } }
    }
}