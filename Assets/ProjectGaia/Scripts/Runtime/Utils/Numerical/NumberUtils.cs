﻿namespace Taijj.ProjectGaia.Utils.Numerical
{
	public static class NumberUtils
	{
		#region Ranges
		public static int FitIntoRangeLooped(int number, int minValue, int maxValue)
		{
			int range = maxValue-minValue;
			if(range == 0)
			{
				return minValue;
			}
			else
			{
				range += 1;
			}
			if(number > maxValue)
            {
                return number - range;
            }
            else if(number < minValue)
            {
                return number + range;
            }
            else
            {
                return number;
            }
		}

		public static int FitIntoRange(int number, int minValue, int maxValue)
		{
			if(number < minValue)
			{
				return minValue;
			}
			else if(number > maxValue)
			{
				return maxValue;
			}
			else
			{
				return number;
			}
		}
		#endregion



		#region Clamp
		public static float Clamp(float number, float minValue, float maxValue)
		{
			return DoClamping(number, minValue, maxValue);
		}

		public static int Clamp(int number, int minValue, int maxValue)
		{
			return (int)DoClamping(number, minValue, maxValue);
		}

		private static float DoClamping(float number, float minValue, float maxValue)
		{
			if(number < minValue)
			{
				return minValue;
			}
			if(number > maxValue)
			{
				return maxValue;
			}
			return number;
		}
		#endregion



		#region Checks
		public static bool IsBetween(float number, float min, float max)
		{
			return number >= min && number <= max;
		}

		public static bool IsBetween(int number, int min, int max)
		{
			return number >= min && number <= max;
		}
		#endregion



		#region Random
		public static int GetRandomInclusive(int min, int max)
		{
			// Unity documentation lies! For Random.Range min is included, but max is not!
            return UnityEngine.Random.Range(min, max+1);
		}
		#endregion
	}
}