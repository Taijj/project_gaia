﻿using System;

namespace Taijj.ProjectGaia
{
    public class WhileCounter
    {
        public static void OnWhileLoopStart(int maxIterations)
        {
            Count = maxIterations;
        }

        public static void OnWhileLoopIteration()
        {
            Count--;
            if(Count <= 0)
            {
                throw new Exception("WhileCounter reached zero. While loop busted!");
            }
        }

        private static int Count { get; set; }
    }
}