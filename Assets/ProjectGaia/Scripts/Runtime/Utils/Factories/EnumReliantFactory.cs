﻿using System;

namespace Taijj.ProjectGaia.Utils.Factories
{
	public class EnumReliantFactory<T>
	{
		#region Main
		public static T Create(IConvertible enumKind, bool includeEnumInTargetNamespace = false)
        {
            IncludeEnumInTargetNamespace = includeEnumInTargetNamespace;

			CheckEnumKind(enumKind);
            SetProperties(enumKind);
            CheckTargetType();
            CheckTargetInheritance();
            return CreateTargetInstance();
        }

        private static void SetProperties(IConvertible kind)
        {
            AbstractType = typeof(T);
            KindName = kind.ToString();

            TargetTypeFullName = GetTargetNamespace(kind) + "." + KindName + AbstractType.Name;
            TargetType = Type.GetType(TargetTypeFullName);
        }

        private static string GetTargetNamespace(IConvertible kind)
        {
            string targetNamespace = AbstractType.Namespace;
            if(IncludeEnumInTargetNamespace)
            {
                targetNamespace +=  "." + kind.ToString();
            }
            return targetNamespace;
        }

		private static T CreateTargetInstance()
        {
            return (T)Activator.CreateInstance(TargetType);
        }
		#endregion



		#region Validation
		private static void CheckEnumKind(IConvertible enumKind)
		{
			if(!enumKind.GetType().IsEnum)
			{
				throw new ArgumentException("EnumKind must be an enumerated type");
			}
		}

        private static void CheckTargetType()
        {
            if (TargetType == null)
            {
                throw new InvalidOperationException("Creation of " + TargetTypeFullName + " has failed! Make sure the abstract type, sub type, and enum kinds match!");
            }
        }

        private static void CheckTargetInheritance()
        {
            if (!AbstractType.IsAssignableFrom(TargetType))
            {
                throw new InvalidOperationException("Type for enum kind " + KindName + " must inherit from " + AbstractType + "!");
            }
        }
		#endregion



		#region Properties
		private static Type AbstractType { get; set; }
        private static Type TargetType { get; set; }
		private static string TargetTypeFullName { get; set; }
        private static string KindName { get; set; }
        private static bool IncludeEnumInTargetNamespace { get; set; }
		#endregion
	}
}