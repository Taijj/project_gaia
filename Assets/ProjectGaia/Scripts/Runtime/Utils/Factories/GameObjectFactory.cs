﻿using UnityEngine;

namespace Taijj.ProjectGaia.Utils.Factories
{
	public static class GameObjectFactory
	{
		public static T CreateAs<T>(GameObject prefab, Transform container, string name)
		{
			return Create(prefab, container, name).GetComponent<T>();
		}

		public static GameObject Create(GameObject prefab, Transform container, string name)
		{
			GameObject obj = GameObject.Instantiate(prefab);
			obj.transform.SetParent(container, false);
			obj.transform.localPosition = Vector3.zero;
			obj.transform.localScale = Vector3.one;
			obj.transform.localRotation = Quaternion.identity;
			obj.name = name;

			return obj;
		}
	}
}