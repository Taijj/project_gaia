﻿using System;
using Taijj.ProjectGaia.Input;
using Taijj.ProjectGaia.Utils.UI;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.UserInterface
{
    public class TutorialDisplay : UiElement
    {
        public event Action Completed;

        public enum Kind
        {
            None = 0,
            AttackParade = 1,
            AnimationEnergy = 2,
            LifeResistance = 3,
            DefaultActions = 4,
            Analyzing = 5,
            DefendStrategy = 6
        }

        public void Wake()
        {
            Active = false;
            HideAll();
        }

        public void Show(Kind kind)
        {
            Active = true;
            HideAll();
            transform.GetChild((int)kind).gameObject.SetActive(true);
            PlayerInput.Instance.Menu.Decide.OnConfirmed += Complete;
        }

        private void Complete()
        {
            PlayerInput.Instance.Menu.Decide.OnConfirmed -= Complete;
            Completed?.Invoke();
        }

        public void Hide()
        {
            HideAll();
            Active = false;
        }

        private void HideAll()
        {
            for(int i = 1; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }
        }
    }
}