﻿using UnityEngine;
using TMPro;
using Taijj.ProjectGaia.Input;
using System;
using Taijj.ProjectGaia.Utils.UI;

namespace Taijj.ProjectGaia.Battle.UserInterface
{
	public class EndDisplay : UiElement
	{
		public event Action Completed;
		[SerializeField] private TextMeshProUGUI _resultText;

		public void Wake()
		{
			Active = false;
		}

		public void Show(string text)
		{
			PlayerInput.Instance.Menu.Decide.OnConfirmed += OnConfirm;
			_resultText.text = text;
			Active = true;
		}

		private void OnConfirm()
		{
			Hide();
			Completed?.Invoke();
		}

		public void Hide()
		{
			PlayerInput.Instance.Menu.Decide.OnConfirmed -= OnConfirm;
			Active = false;
		}
	}
}