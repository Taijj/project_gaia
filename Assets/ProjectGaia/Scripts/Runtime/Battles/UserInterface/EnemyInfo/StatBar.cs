﻿using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.FighterStats;
using Taijj.ProjectGaia.Utils.UI;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.UserInterface.EnemyInfo
{
	public class StatBar : UiElement
	{
		[SerializeField] private Stat _stat;
		[SerializeField] private Progressbar _progressbar;

		public void Wake()
		{
			_progressbar.Color = _stat.Color();
		}

		public void Refresh(Fighter hero)
		{
			_progressbar.Progress = hero.GetStatFraction(_stat);
		}
	}
}