﻿using System.Linq;
using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillExecution;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.Utils.Factories;
using Taijj.ProjectGaia.Utils.UI;
using UnityEngine;
using Taijj.ProjectGaia.Battle.Flow.Utils;

namespace Taijj.ProjectGaia.Battle.UserInterface.EnemyInfo
{
    public class EnemyInfoDisplay : UiElement
    {
        [SerializeField] private GameObject _panelPrefab;

        public void Wake()
        {
            PanelsByFighter = new Dictionary<Fighter, EnemyPanel>();
            AnalyzeSkillExecutor.OnFirstTier += CreatePanelFor;
            AnalyzeSkillExecutor.OnSecondTier += UpgradePanelFor;

            HealthObserver.WasKilled += DestroyPanelFor;
        }

        public void CreatePanelFor(Fighter enemy)
        {
            EnemyPanel newPanel = GameObjectFactory.CreateAs<EnemyPanel>(_panelPrefab, transform, enemy.name + "Panel");
            newPanel.transform.position = enemy.transform.position;
            newPanel.Wake(enemy);
            PanelsByFighter.Add(enemy, newPanel);
        }

        public void UpgradePanelFor(Fighter enemy)
        {
            PanelsByFighter[enemy].Upgrade();
        }

        public void DestroyPanels()
        {
            while(PanelsByFighter.Count != 0)
            {
                KeyValuePair<Fighter, EnemyPanel> element = PanelsByFighter.First();
                DestroyPanelFor(element.Key);
            }
        }

        private void DestroyPanelFor(Fighter fighter)
        {
            if(!PanelsByFighter.ContainsKey(fighter))
            {
                return;
            }

            EnemyPanel panel = PanelsByFighter[fighter];
            PanelsByFighter.Remove(fighter);
            panel.OnDestruct();
            panel.DestructAndNullify();
        }

        private Dictionary<Fighter, EnemyPanel> PanelsByFighter { get; set; }
    }
}