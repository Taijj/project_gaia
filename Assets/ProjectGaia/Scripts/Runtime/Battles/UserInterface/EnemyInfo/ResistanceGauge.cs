﻿using System.Collections.Generic;
using Taijj.ProjectGaia.StaticData.Fighters;
using Taijj.ProjectGaia.Utils.UI;
using UnityEngine;
using TMPro;
using Taijj.ProjectGaia.Battle.UserInterface.Utils;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;

namespace Taijj.ProjectGaia.Battle.UserInterface.EnemyInfo
{
    public class ResistanceGauge : UiElement
    {
        #region Init
        [SerializeField] private List<FormIcon> _icons;
        [SerializeField] private TextMeshProUGUI _label;

        public void Wake(Resistance.Modification modification)
        {
            Modification = modification;
            Forms = new List<FormConfig>();

            _label.text = GetLabel();
            SetIcons();
        }

        private string GetLabel()
        {
            switch(Modification)
            {
                case Resistance.Modification.Double:    return "2x";
                case Resistance.Modification.Strong:    return "1.5x";
                case Resistance.Modification.Nullify:   return "0x";
                case Resistance.Modification.Weak:      return "0.5x";
                case Resistance.Modification.AbsorbFull:
                case Resistance.Modification.AbsorbHalf: return "+";
                default: return "";
            }
        }

        public Resistance.Modification Modification { get; private set; }
        #endregion



        #region Forms
        public void AddForm(FormConfig form)
        {
            Forms.Add(form);
            SetIcons();
        }

        private void SetIcons()
        {
            FormIcon.SetAll(Forms, _icons);
        }

        private List<FormConfig> Forms { get; set; }
        #endregion
    }
}