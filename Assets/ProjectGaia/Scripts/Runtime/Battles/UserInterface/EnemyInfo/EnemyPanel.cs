﻿using System.Collections.Generic;
using System.Linq;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.Fighters;
using Taijj.ProjectGaia.Utils.Factories;
using Taijj.ProjectGaia.Utils.UI;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.UserInterface.EnemyInfo
{
    public class EnemyPanel : UiElement
    {
        #region Init
		[SerializeField] private StatBar[] _statBars;
		[SerializeField] private Transform _resistancesContainer;
		[SerializeField] private GameObject _resistanceGaugePrefab;


		public void Wake(Fighter enemy)
		{
			Enemy = enemy;
			Enemy.StatsChanged += Refresh;

			StatBars = _statBars.ToList();
			StatBars.ForEach(b => b.Wake());

			InitializeResistances();
			Refresh();
		}

		private void InitializeResistances()
		{
			GaugesByModification = new Dictionary<Resistance.Modification, ResistanceGauge>();
			foreach(Resistance resistance in Enemy.Resistances)
			{
				TryCreateGaugeFor(resistance.Mod);
				GaugesByModification[resistance.Mod].AddForm(resistance.Form);
			}
			_resistancesContainer.gameObject.SetActive(false);
		}

		private void TryCreateGaugeFor(Resistance.Modification mod)
		{
			if(GaugesByModification.ContainsKey(mod))
			{
				return;
			}

			ResistanceGauge newGauge = GameObjectFactory.CreateAs<ResistanceGauge>(_resistanceGaugePrefab, _resistancesContainer, mod + "Gauge");
			newGauge.LocalPosition = Vector3.down * newGauge.Height * GaugesByModification.Count;
			newGauge.Wake(mod);
			GaugesByModification.Add(mod, newGauge);
		}
		#endregion



		#region Behavior
		public void OnDestruct()
		{
			GaugesByModification.DestroyGameObjects();

			Enemy.StatsChanged -= Refresh;
			Enemy = null;
		}

		private void Refresh()
		{
			StatBars.ForEach(h => h.Refresh(Enemy));
		}

		public void Upgrade()
		{
			_resistancesContainer.gameObject.SetActive(true);
		}
		#endregion



		#region Properties
		private Fighter Enemy { get; set; }
		private List<StatBar> StatBars { get; set; }
		private Dictionary<Resistance.Modification, ResistanceGauge> GaugesByModification { get; set; }
		#endregion
    }
}
