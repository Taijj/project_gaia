﻿using Taijj.ProjectGaia.Battle.UserInterface.EnemyInfo;
using Taijj.ProjectGaia.Battle.UserInterface.Footer;
using Taijj.ProjectGaia.Battle.UserInterface.Selections.Skills;
using Taijj.ProjectGaia.Battle.UserInterface.Selections.Targets;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.UserInterface
{
    public class BattleUserInterface : MonoBehaviour
    {
        [SerializeField] private SkillSelection _skillSelection;
        [SerializeField] private TargetsSelection _targetSelection;
        [SerializeField] private BattleFooter _footer;
        [SerializeField] private EnemyInfoDisplay _enemyInfoDisplay;
        [SerializeField] private EndDisplay _endDisplay;
        [SerializeField] private IntroDisplay _introDisplay;
        [SerializeField] private TutorialDisplay _tutorialDisplay;



        public void Wake()
        {
            _skillSelection.Wake();
            _targetSelection.Wake();
            _footer.Wake();
            _enemyInfoDisplay.Wake();
            _endDisplay.Wake();
            _tutorialDisplay.Wake();
        }

        public void OnBattleStart()
        {
            _targetSelection.OnBattleStart();
            _footer.CreateHeroPanels();
        }

        public void OnBattleEnd()
        {
            _targetSelection.OnBattleEnd();
            _footer.DestroyHeroPanels();
            _enemyInfoDisplay.DestroyPanels();
        }


        public SkillSelection SkillSelection => _skillSelection;
        public TargetsSelection TargetsSelection => _targetSelection;
        public EndDisplay EndDisplay => _endDisplay;
        public IntroDisplay IntroDisplay => _introDisplay;
        public TutorialDisplay TutorialDisplay => _tutorialDisplay;
    }
}