﻿using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.Flow.Turn.Order.Clairvoyance;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.Utils.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Taijj.ProjectGaia.Battle.UserInterface.Footer.Order
{
	public class OrderDisplay : UiElement
	{
		[SerializeField] private Image _currenImage;
		[SerializeField] private Image[] _nextImages;

		public void Wake()
		{
			Oracle.OnCurrentFighterUpdated += OnCurrentFighterUpdated;
			Oracle.OnOrderSimulated += OnFighterOrderSimulated;
		}

		private void OnCurrentFighterUpdated(Fighter currentFighter)
		{
			_currenImage.sprite = currentFighter.Model.Config.Sprite;
			_currenImage.color = currentFighter.Model.Config.Color;
		}

		private void OnFighterOrderSimulated(List<Fighter> nextFighters)
		{
			for(int i = 0; i < _nextImages.Length; i++)
			{
				_nextImages[i].sprite = nextFighters[i].Model.Config.Sprite;
				_nextImages[i].color = nextFighters[i].Model.Config.Color;
			}
		}
	}
}