﻿using UnityEngine;
using Taijj.ProjectGaia.Battle.UserInterface.Footer.Heroes;
using Taijj.ProjectGaia.Battle.UserInterface.Footer.Order;
using Taijj.ProjectGaia.Battle.Flow;
using Taijj.ProjectGaia.Utils.UI;

namespace Taijj.ProjectGaia.Battle.UserInterface.Footer
{
	public class BattleFooter : UiElement
	{
		#region Init
		[SerializeField] private HeroesDisplay _heroesDisplay;
		[SerializeField] private OrderDisplay _orderDisplay;

		public void Wake()
		{
			_heroesDisplay.Wake();
			_orderDisplay.Wake();
			Active = false;
		}
		#endregion



		#region Lifecycle
		public void CreateHeroPanels()
        {
			Active = true;
            _heroesDisplay.CreatePanels(BattleFlow.BattleData.Roster.HeroParty);
			Layout();

			ScreenSizeObserver.Instance.OnScreenSizeChange += Layout;
        }

        public void DestroyHeroPanels()
        {
			ScreenSizeObserver.Instance.OnScreenSizeChange -= Layout;
            _heroesDisplay.DestroyPanels();
			Active = false;
        }
		#endregion



		#region Layouting
		private void Layout()
		{
			_heroesDisplay.LayoutPanels();
		}
		#endregion
	}
}