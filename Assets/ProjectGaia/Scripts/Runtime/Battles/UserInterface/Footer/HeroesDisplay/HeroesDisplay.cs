﻿using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.Utils.Factories;
using Taijj.ProjectGaia.Utils.UI;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.UserInterface.Footer.Heroes
{
	public class HeroesDisplay : UiElement
	{
		[SerializeField] private GameObject _panelPrefab;

		public void Wake()
		{
			Panels = new List<HeroPanel>();
		}

		public void CreatePanels(List<Fighter> heroes)
		{
			heroes.ForEach(h => CreatePanel(h));
		}

		private void CreatePanel(Fighter hero)
		{
			HeroPanel newPanel = GameObjectFactory.CreateAs<HeroPanel>(_panelPrefab, transform, hero.name + "Panel");
			newPanel.Wake(hero);
			Panels.Add(newPanel);
		}

		public void LayoutPanels()
		{
			float panelWidth = Width/(float)Panels.Count;
			for(int i = 0; i < Panels.Count; i++)
			{
				HeroPanel panel = Panels[i];
				panel.Width = panelWidth;
				panel.LocalPosition = Vector3.right*panelWidth*i;
			}
		}

		public void DestroyPanels()
		{
			while(Panels.Count > 0)
			{
				HeroPanel panel = Panels[0];
				Panels.RemoveAt(0);

				panel.OnIsDestroyed();
				panel.DestructAndNullify();
			}
		}

		public List<HeroPanel> Panels { get; set; }
	}
}