﻿using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData;
using Taijj.ProjectGaia.StaticData.FighterStats;
using Taijj.ProjectGaia.Utils.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Taijj.ProjectGaia.Battle.UserInterface.Footer.Heroes
{
	public class StatBar : UiElement
	{
		[SerializeField] private Stat _stat;
		[SerializeField] private Progressbar _progressbar;
		[SerializeField] private TextMeshProUGUI _textMesh;
		[SerializeField] private Image _icon;

		public void Wake()
		{
			_icon.sprite = _stat.Icon();
			_icon.color = _stat.Color();
			_progressbar.Color = _stat.Color();
		}

		public void Refresh(Fighter hero)
		{
			_progressbar.Progress = hero.GetStatFraction(_stat);
			_textMesh.text = hero.GetStat(_stat).ToString();
		}
	}
}