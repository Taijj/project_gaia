﻿using TMPro;
using UnityEngine;
using System.Collections.Generic;
using Taijj.ProjectGaia.Utils.UI;
using Taijj.ProjectGaia.Battle.World.Fighters;
using System.Linq;
using UnityEngine.UI;
using Taijj.ProjectGaia.StaticData.Fighters;

namespace Taijj.ProjectGaia.Battle.UserInterface.Footer.Heroes
{
	public class HeroPanel : UiElement
	{
		#region Injections
		[SerializeField] private TextMeshProUGUI _heroNameTextMesh;
		[SerializeField] private Image _heroImage;
		[SerializeField] private StatBar[] _statBars;
		#endregion



		#region Attachment
		public void Wake(Fighter hero)
		{
			Hero = hero;
			Hero.StatsChanged += Refresh;

			FighterConfig config = hero.Model.Config;
			_heroNameTextMesh.text = config.DisplayName;
			_heroImage.sprite = config.Sprite;
			_heroImage.color = config.Color.CloneAndSetA(0.5f);

			StatBars = _statBars.ToList();
			StatBars.ForEach(b => b.Wake());

			Refresh();
		}

		public void OnIsDestroyed()
		{
			Hero.StatsChanged -= Refresh;
			Hero = null;
		}

		private void Refresh()
		{
			StatBars.ForEach(h => h.Refresh(Hero));
		}

		private Fighter Hero { get; set; }
		private List<StatBar> StatBars { get; set; }
		#endregion
	}
}