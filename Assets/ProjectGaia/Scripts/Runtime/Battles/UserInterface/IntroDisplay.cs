﻿using System;
using Taijj.ProjectGaia.Utils.SceneSingletons;
using Taijj.ProjectGaia.Utils.UI;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.UserInterface
{
    public class IntroDisplay : UiElement
    {
        public void Show(Action onCompleted)
        {
            Active = true;

            Animation animation = gameObject.GetComponent<Animation>();
            CoroutineRunner.Instance.WaitForSeconds(animation.clip.length + 1f, delegate()
            {
                Active = false;
                onCompleted();
            });
        }
    }
}