﻿using System.Collections.Generic;
using System;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.Battle.Flow;
using Taijj.ProjectGaia.Battle.UserInterface.Selections.Targets.Flow.LockingOn;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;
using Taijj.ProjectGaia.Battle.Flow.Utils;

namespace Taijj.ProjectGaia.Battle.UserInterface.Selections.Targets.Flow
{
	public class TargetsSelectionFlow
	{
		#region Beginning
		public event Action OnSelectionUpdated;

		public TargetsSelectionFlow()
		{
			HeroesLockOn = new LockOn();
			EnemiesLockOn = new LockOn();
			ModeSwitcher = new ModeSwitcher();
		}

		public void OnBattleStart()
		{
			HeroesLockOn.OnBattleStart(BattleFlow.BattleData.Roster.HeroParty);
			EnemiesLockOn.OnBattleStart(BattleFlow.BattleData.Roster.EnemyParty);

			HeroesLockOn.OnMoveOff += OnLockOnMovedOff;
			EnemiesLockOn.OnMoveOff += OnLockOnMovedOff;
		}

		public void OnBattleEnd()
		{
			HeroesLockOn.OnMoveOff -= OnLockOnMovedOff;
			EnemiesLockOn.OnMoveOff -= OnLockOnMovedOff;
		}
		#endregion



		#region Mode
		public void Prepare(AimKind aim, Fighter initialFocusTarget)
		{
			ModeSwitchIsAllowed = AimHelper.SingleOrPartySwitchIsAllowed(aim);
			ModeSwitcher.SetDefault();
			CurrentLockOn = initialFocusTarget.IsHero ? HeroesLockOn : EnemiesLockOn;
			CurrentLockOn.Prepare(initialFocusTarget);
			DispatchSelectionUpdate();
		}

		public void ToggleMode()
		{
			if(!ModeSwitchIsAllowed)
			{
				return;
			}
			ModeSwitcher.ToggleMode();
			DispatchSelectionUpdate();
		}
		#endregion



		#region Directional Input
		public void SelectUp() { SelectVertical(-1); }
		public void SelectDown()  { SelectVertical(1); }

		private void SelectVertical(int direction)
		{
			if(ModeSwitcher.IsWholePartySelection)
			{
				return;
			}
			CurrentLockOn.SelectVertical(direction);
			DispatchSelectionUpdate();
		}



		public void SelectRight() { SelectHorizontal(1); }
		public void SelectLeft() { SelectHorizontal(-1); }

		private void SelectHorizontal(int direction)
		{
			if(ModeSwitcher.IsWholePartySelection)
			{
				OnLockOnMovedOff(direction, 0f);
				return;
			}
			CurrentLockOn.SelectHorizontal(direction);
			DispatchSelectionUpdate();
		}
		#endregion



		#region Special
		private void OnLockOnMovedOff(int direction, float rowFraction)
		{
			CurrentLockOn = CurrentLockOn == HeroesLockOn ? EnemiesLockOn : HeroesLockOn;
			CurrentLockOn.OnMovedOn(direction, rowFraction);
			DispatchSelectionUpdate();
		}

		private void DispatchSelectionUpdate()
		{
			OnSelectionUpdated?.Invoke();
		}
		#endregion



		#region Properties
		private LockOn HeroesLockOn { get; set; }
		private LockOn EnemiesLockOn { get; set; }
		private LockOn CurrentLockOn { get; set; }
		private ModeSwitcher ModeSwitcher { get; set; }
		private bool ModeSwitchIsAllowed { get; set; }

		public List<Fighter> SelectedFighters => ModeSwitcher.IsWholePartySelection ? CurrentLockOn.AliveOfParty : CurrentLockOn.Target.ToSingleList();
		#endregion
	}
}