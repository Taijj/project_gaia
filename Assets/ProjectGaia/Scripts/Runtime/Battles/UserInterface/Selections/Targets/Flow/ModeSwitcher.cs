﻿
namespace Taijj.ProjectGaia.Battle.UserInterface.Selections.Targets.Flow
{
	public class ModeSwitcher
	{
		public void SetDefault()
		{
			IsWholePartySelection = false;
		}

		public void ToggleMode()
		{
			IsWholePartySelection = !IsWholePartySelection;
		}

		public bool IsWholePartySelection { get; private set; }
	}
}