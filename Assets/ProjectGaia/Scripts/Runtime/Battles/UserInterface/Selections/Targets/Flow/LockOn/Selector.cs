﻿using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.Utils.Numerical;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.UserInterface.Selections.Targets.Flow.LockingOn
{
	// TODO Rethink this! Maybe use angle calculations again
    // Sorry for the mess...
    public class Selector
    {
		private const float TOLERANCE = 1f;
        public Selector(LockOn lockOn)
        {
            LockOn = lockOn;
            FoundInGeneralDirection = new List<Fighter>();
			FoundInDirection = new List<Fighter>();
        }



		public Fighter Select(int direction, bool horizontal)
		{
			IsHorizontalNavigation = horizontal;
			NavigationDirection = direction;

			FindTarget();
			if(FoundTarget == null && horizontal)
			{
				LockOn.MoveOff(NavigationDirection, Target.Y);
			}
            return FoundTarget == null ? Target : FoundTarget;
		}

		private void FindTarget()
		{
			FoundTarget = null;
			FoundInGeneralDirection.Clear();
			FoundInDirection.Clear();

			foreach(Fighter fighter in LockOn.Party)
			{
				bool skipFighter = fighter == Target || !fighter.IsAlive;
				if(skipFighter)
				{
					continue;
				}

				if(IsInGeneralDirection(fighter))
				{
					FoundInGeneralDirection.Add(fighter);
				}
				else if(IsInDirection(fighter))
				{
					FoundInDirection.Add(fighter);
				}
			}

            FoundTarget = FindClosest(Target.transform.position,
                FoundInGeneralDirection.Count != 0
                    ? FoundInGeneralDirection
                    : FoundInDirection);
		}


		private bool IsInGeneralDirection(Fighter fighter)
		{
			return IsInDirection(fighter) && IsRoughlyOnSameAxis(fighter);
		}

		private bool IsInDirection(Fighter fighter)
		{
			if(IsHorizontalNavigation)
			{
				return NavigationDirection < 0 ? fighter.X <= Target.X : fighter.X >= Target.X;
			}
			else
			{
				return NavigationDirection < 0 ? fighter.Y >= Target.Y : fighter.Y <= Target.Y;
			}
		}

		private bool IsRoughlyOnSameAxis(Fighter fighter)
		{
			if(IsHorizontalNavigation)
			{
				return NumberUtils.IsBetween(fighter.Y, Target.Y-TOLERANCE, Target.Y+TOLERANCE);
			}
			else
			{
				return NumberUtils.IsBetween(fighter.X, Target.X-TOLERANCE, Target.X+TOLERANCE);
			}
		}



		public static Fighter FindClosest(Vector3 position, List<Fighter> fighters)
		{
            Fighter result = null;
			float closestDistance = float.MaxValue;
			foreach(Fighter fighter in fighters)
			{
				if(!fighter.IsAlive)
				{
					continue;
				}

				float distance = Vector3.Distance(fighter.transform.position, position);
				if(distance < closestDistance)
				{
					closestDistance = distance;
					result = fighter;
				}
			}
            return result;
		}



		private int NavigationDirection { get; set; }
		private bool IsHorizontalNavigation { get; set; }

		private List<Fighter> FoundInGeneralDirection { get; set; }
		private List<Fighter> FoundInDirection { get; set; }
		private Fighter FoundTarget { get; set; }

        private Fighter Target => LockOn.Target;
        private LockOn LockOn { get; set; }
    }
}