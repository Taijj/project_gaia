﻿using System;
using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.Flow.Utils;
using Taijj.ProjectGaia.Battle.UserInterface.Selections.Targets.Cursors;
using Taijj.ProjectGaia.Battle.UserInterface.Selections.Targets.Flow;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.Skills;
using Taijj.ProjectGaia.Utils.UI;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.UserInterface.Selections.Targets
{
	public class TargetsSelection : UiElement
	{
		#region Beginning
		[SerializeField] private CursorSet _cursorSet;

		public void Wake()
		{
			_cursorSet.Wake();
			CreateComponents();
			AddComponentEvents();
			Active = false;
		}

		private void CreateComponents()
		{
			Flow = new TargetsSelectionFlow();
			Inputs = new Inputs(Flow);
		}

		private void AddComponentEvents()
		{
			Flow.OnSelectionUpdated += OnSelectionUpdated;
			Inputs.OnSelected += DispatchTargetsSelected;
			Inputs.OnCanceled += DispatchSelectionCanceled;
		}



		public void OnBattleStart()
		{
			Flow.OnBattleStart();
		}

		public void OnBattleEnd()
		{
			Flow.OnBattleEnd();
		}
		#endregion



		#region Showing
		public void Show(SkillConfig skill, Fighter initialFocusTarget)
		{
			Skill = skill;

			Active = true;
			ValidateTargeting();
			Flow.Prepare(Skill.Aim, initialFocusTarget);
			Inputs.Enable();
		}

		private void ValidateTargeting()
		{
			if(AimHelper.IsAutoAim(Skill.Aim))
			{
				throw new ArgumentException($"Choosing targets for AimKind.{Skill.Aim} is not supported! ");
			}
		}

		public void Hide()
		{
			Inputs.Disable();
			Active = false;
		}
		#endregion



		#region Selection
		private void OnSelectionUpdated()
		{
			_cursorSet.UpdateCursors(Flow.SelectedFighters);
		}
		#endregion



		#region Events
		public event Action<List<Fighter>> OnTargetsSelected;
		public event Action OnSelectionCanceled;

		private void DispatchTargetsSelected()
		{
			OnTargetsSelected?.Invoke(Flow.SelectedFighters);
		}

		private void DispatchSelectionCanceled()
		{
			OnSelectionCanceled?.Invoke();
		}
		#endregion



		#region Properties
		private TargetsSelectionFlow Flow { get; set; }
		private Inputs Inputs { get; set; }
		private SkillConfig Skill { get; set; }
		#endregion
	}
}