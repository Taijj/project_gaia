﻿using System.Collections.Generic;
using UnityEngine;
using System;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.Utils.Factories;
using Taijj.ProjectGaia.Battle.Flow;

namespace Taijj.ProjectGaia.Battle.UserInterface.Selections.Targets.Cursors
{
	[Serializable]
	public class CursorSet
	{
		#region Beginning
		[SerializeField] private GameObject _cursorPrefab;
		[SerializeField] private Transform _container;

		public void Wake()
		{
			Cursors = new List<Cursor>();
		}
		#endregion



		#region Update
		public void UpdateCursors(List<Fighter> selectedFighters)
		{
			SelectedFighters = selectedFighters;
			TryStockingUpCursors();
			PositionCursors();
			UpdateCursorsVisibility();
		}

		private void TryStockingUpCursors()
		{
			WhileCounter.OnWhileLoopStart(Everyone.Count+1);
			while(Cursors.Count < Everyone.Count)
			{
				Cursor newCursor = GameObjectFactory.CreateAs<Cursor>(_cursorPrefab, _container, "Cursor"+Cursors.Count);
				newCursor.Active = false;
				Cursors.Add(newCursor);

				WhileCounter.OnWhileLoopIteration();
			}
		}

		private void PositionCursors()
		{
			for(int i = 0; i < Everyone.Count; i++)
			{
				Cursors[i].PointTo(Everyone[i]);
			}
		}

		private void UpdateCursorsVisibility()
		{
			Cursors.ForEach(c => c.Active = false);
			SelectedFighters.ForEach(f => Cursors[Everyone.IndexOf(f)].Active = true);
		}
		#endregion



		#region Properties
		private List<Cursor> Cursors { get; set; }
		private List<Fighter> Everyone { get { return BattleFlow.BattleData.Roster.AllFighters; } }
		private List<Fighter> SelectedFighters { get; set; }
		#endregion
	}
}