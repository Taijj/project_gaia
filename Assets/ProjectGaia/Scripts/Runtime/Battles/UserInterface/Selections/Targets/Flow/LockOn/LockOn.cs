﻿using System.Linq;
using System;
using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.World.Fighters;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.UserInterface.Selections.Targets.Flow.LockingOn
{
	public class LockOn
	{
		#region Init
		public event Action<int, float> OnMoveOff;

		public LockOn()
		{
			Selector = new Selector(this);
		}

		public void OnBattleStart(List<Fighter> party)
		{
			Party = party;
		}

		public void Prepare(Fighter initialTarget)
		{
			if(!initialTarget.IsAlive)
			{
				Target = Party.First(f => f.IsAlive);
			}
			else
			{
				Target = initialTarget;
			}
		}
		#endregion



		#region Main
		public void SelectHorizontal(int direction)
		{
			Target = Selector.Select(direction, true);
		}

		public void SelectVertical(int direction)
		{
			Target = Selector.Select(direction, false);
		}

		public void MoveOff(int direction, float yPosition)
		{
			OnMoveOff?.Invoke(direction, yPosition);
		}

		public void OnMovedOn(int direction, float yPosition)
		{
			Vector3 dummyPosition = new Vector3(-100*direction, yPosition, 0f);
			Target = Selector.FindClosest(dummyPosition, Party);
		}

		public List<Fighter> Party { get; set; }
		public List<Fighter> AliveOfParty => Party.Where(f => f.IsAlive).ToList();
		public Fighter Target { get; private set; }

		private Selector Selector { get; set; }
		#endregion
	}
}