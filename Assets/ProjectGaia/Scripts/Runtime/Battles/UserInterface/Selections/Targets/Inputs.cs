﻿using System;
using Taijj.ProjectGaia.Battle.UserInterface.Selections.Targets.Flow;
using Taijj.ProjectGaia.Input;
using Taijj.ProjectGaia.Input.Menu;

namespace Taijj.ProjectGaia.Battle.UserInterface.Selections.Targets
{
	public class Inputs
	{
		#region Init
		public event Action OnSelected;
		public event Action OnCanceled;

		public Inputs(TargetsSelectionFlow flow)
		{
			Flow = flow;
		}

		private TargetsSelectionFlow Flow
		{
			get;
			set;
		}
		#endregion



		#region Enable/Disable
		public void Enable()
        {
			MenuActions menu = PlayerInput.Instance.Menu;
            menu.Cursor.Up += Flow.SelectUp;
            menu.Cursor.Down += Flow.SelectDown;
			menu.Cursor.Left += Flow.SelectLeft;
			menu.Cursor.Right += Flow.SelectRight;
			menu.Multi.Up += Flow.ToggleMode;
			menu.Multi.Down += Flow.ToggleMode;
            menu.Decide.OnConfirmed += OnConfirm;
			menu.Decide.OnCanceled += OnCancel;
        }

        public void Disable()
        {
            MenuActions menu = PlayerInput.Instance.Menu;
            menu.Cursor.Up -= Flow.SelectUp;
            menu.Cursor.Down -= Flow.SelectDown;
			menu.Cursor.Left -= Flow.SelectLeft;
			menu.Cursor.Right -= Flow.SelectRight;
			menu.Multi.Up -= Flow.ToggleMode;
			menu.Multi.Down -= Flow.ToggleMode;
            menu.Decide.OnConfirmed -= OnConfirm;
			menu.Decide.OnCanceled -= OnCancel;
        }
		#endregion



		#region Decision Inputs
		private void OnConfirm()
		{
			OnSelected?.Invoke();
		}

		private void OnCancel()
		{
			OnCanceled?.Invoke();
		}
		#endregion
	}
}