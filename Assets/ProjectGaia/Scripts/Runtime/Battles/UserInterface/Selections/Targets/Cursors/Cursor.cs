﻿using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.Utils.UI;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.UserInterface.Selections.Targets.Cursors
{
	public class Cursor : UiElement
	{
		[SerializeField] private Vector2 _offset;

		public void PointTo(Fighter fighter)
		{
			Vector3 position = transform.parent.InverseTransformPoint(fighter.transform.position);
            LocalPosition = new Vector3(position.x, position.y, 0.0f);

			if(fighter.IsHero)
			{
				LocalPosition += _offset.ToVector3();
				Demirror(transform);
			}
			else
			{
				LocalPosition -= _offset.ToVector3();
				Mirror(transform);
			}
		}
	}
}