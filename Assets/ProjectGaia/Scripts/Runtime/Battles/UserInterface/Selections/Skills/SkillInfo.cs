﻿using System.Collections.Generic;
using Taijj.ProjectGaia.Utils.UI;
using UnityEngine;
using TMPro;
using Taijj.ProjectGaia.StaticData.Skills;
using Taijj.ProjectGaia.Battle.UserInterface.Utils;

namespace Taijj.ProjectGaia.Battle.UserInterface.Selections.Skills
{
    public class SkillInfo : UiElement
    {
        [SerializeField] private List<FormIcon> _icons;
        [SerializeField] private TextMeshProUGUI _descriptionTextMesh;

        public void Set(SkillConfig skill)
        {
            _descriptionTextMesh.text = skill.Description;
            FormIcon.SetAll(skill.Forms, _icons);
        }
    }
}