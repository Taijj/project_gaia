﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using Taijj.ProjectGaia.StaticData.Skills;
using Taijj.ProjectGaia.Utils.UI;
using Taijj.ProjectGaia.Utils.UI.Buttons;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.Input;
using Taijj.ProjectGaia.Utils.SceneSingletons;
using Taijj.ProjectGaia.Input.Menu;

namespace Taijj.ProjectGaia.Battle.UserInterface.Selections.Skills
{
    public class SkillSelection : UiElement
    {
        #region Init
        [SerializeField] private ButtonListMono _buttonListMono;

        public void Wake()
        {
            ButtonList = _buttonListMono.Create<SkillButton>();
            Active = false;
        }
        #endregion



        #region Visibility
        public void ShowFor(Fighter fighter, SkillConfig initialSelection)
        {
            Active = true;

            Fighter = fighter;
            SetPosition();
            Layout();
            UpdateButtonsPressable();
            EnableInput();
            MakeInitialSelection(initialSelection);
        }

        private void SetPosition()
        {
            Vector3 fighterPosition = transform.parent.InverseTransformPoint(Fighter.transform.position);
            LocalPosition = new Vector3(fighterPosition.x, fighterPosition.y, 0.0f);
        }

        private void Layout()
        {
            List<ButtonData> buttonDataSet = Fighter.Model.Skills.Select(s => new SkillButtonData(s) as ButtonData).ToList();
            ButtonList.Layout(buttonDataSet);

            Width = ButtonList.GetWidth();
            Height = ButtonList.GetHeight();
        }

        private void UpdateButtonsPressable()
        {
            ButtonList.UsedButtons.ForEach(b => b.SetPressable( Fighter.CanUse(b.SkillData.Skill)) );
        }

        private void MakeInitialSelection(SkillConfig initialSelection)
        {
            SkillButton initialButton = ButtonList.UsedButtons.First(b => b.SkillData.Skill == initialSelection);
            ButtonList.SelectButton(initialButton);
            OnSelect();
        }

        public void Hide()
        {
            DisableInput();
            Active = false;
        }
        #endregion



        #region Input
        private void EnableInput()
        {
            MenuActions menu = PlayerInput.Instance.Menu;
            menu.Cursor.Up += OnUp;
            menu.Cursor.Down += OnDown;
            menu.Decide.OnConfirmed += OnConfirm;
            menu.Decide.OnCanceled += OnCancel;
        }

        private void DisableInput()
        {
            MenuActions menu = PlayerInput.Instance.Menu;
            menu.Cursor.Up -= OnUp;
            menu.Cursor.Down -= OnDown;
            menu.Decide.OnConfirmed -= OnConfirm;
            menu.Decide.OnCanceled -= OnCancel;
        }

        private void OnUp()
        {
            ButtonList.SelectPreviousButton();
            OnSelect();
        }

        private void OnDown()
        {
            ButtonList.SelectNextButton();
            OnSelect();
        }

        private void OnSelect()
        {
            SkillButton button = ButtonList.GetSelectedButton();
            OnSkillSelected?.Invoke(button.SkillData.Skill);
        }

        private void OnConfirm()
        {
            SkillButton button = ButtonList.GetSelectedButton();
            button.Press();

            if(button.Pressable)
            {
                DispatchSelectedEventDelayed(button.SkillData.Skill);
            }
        }

        private void OnCancel()
        {
            OnCanceled?.Invoke();
        }
        #endregion



        #region Events
        public event Action<SkillConfig> OnSkillChosen;
        public event Action<SkillConfig> OnSkillSelected;
        public event Action OnCanceled;

        private void DispatchSelectedEventDelayed(SkillConfig selectedSkill)
        {
            DisableInput();
            CoroutineRunner.Instance.WaitForSeconds(0.3f, delegate()
            {
                OnSkillChosen?.Invoke(selectedSkill);}
            );
        }
        #endregion



        #region Properties
        private ButtonList<SkillButton> ButtonList { get; set; }
        private Fighter Fighter { get; set; }
        #endregion
    }
}
