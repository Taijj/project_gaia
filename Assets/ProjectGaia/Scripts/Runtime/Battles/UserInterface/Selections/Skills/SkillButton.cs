﻿using Taijj.ProjectGaia.StaticData;
using Taijj.ProjectGaia.StaticData.FighterStats;
using Taijj.ProjectGaia.StaticData.Skills;
using Taijj.ProjectGaia.Utils.UI.Buttons;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Taijj.ProjectGaia.Battle.UserInterface.Selections.Skills
{
	public class SkillButton : DataButton
	{
		[SerializeField] private TextMeshProUGUI _skillNameTextMesh;
		[SerializeField] private TextMeshProUGUI _skillCostTextMesh;
		[SerializeField] private Image _costImage;
		[SerializeField] private SkillInfo _info;

		public override void Wake()
		{
			base.Wake();
			_info.Active = false;
		}

		public override void Select()
		{
			base.Select();
			RectTransform.SetAsLastSibling();
			_info.Active = true;
		}

		public override void Deselect()
		{
			base.Deselect();
			_info.Active = false;
		}

		protected override void UpdateDisplay()
		{
			_info.Set(SkillData.Skill);
			_skillNameTextMesh.text = SkillData.Skill.DisplayName;

			if(SkillData.Skill.HasCost)
			{
				_skillCostTextMesh.text = SkillData.Skill.Cost.Amount.ToString();
				_costImage.gameObject.SetActive(true);
				_costImage.sprite = SkillData.Skill.Cost.Stat.Icon();
				_costImage.color = SkillData.Skill.Cost.Stat.Color();
			}
			else
			{
				_skillCostTextMesh.text = "";
				_costImage.gameObject.SetActive(false);
			}
		}

		public SkillButtonData SkillData { get { return Data as SkillButtonData; } }
	}

	public class SkillButtonData : ButtonData
	{
		public SkillButtonData(SkillConfig skill)
		{
			Skill = skill;
		}

		public SkillConfig Skill { get; private set; }
	}
}