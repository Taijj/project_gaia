﻿using System.Collections.Generic;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;
using Taijj.ProjectGaia.Utils.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Taijj.ProjectGaia.Battle.UserInterface.Utils
{
    public class FormIcon : UiElement
    {
        [SerializeField] private Image _image;

        public void Set(FormConfig form = null)
        {
            if(form != null)
            {
                _image.sprite = form.Icon;
                _image.color = form.Color;
                Active = true;
            }
            else
            {
                Active = false;
            }
        }

        public static void SetAll(List<FormConfig> forms, List<FormIcon> icons)
        {
            for(int i = 0; i < icons.Count; i++)
            {
                if(i < forms.Count)
                {
                    icons[i].Set(forms[i]);
                }
                else
                {
                    icons[i].Set();
                }
            }

            if(forms.Count > icons.Count)
            {
                Logger.LogWarning("There are more forms than icons! Add more icons to display them all!");
            }
        }
    }
}