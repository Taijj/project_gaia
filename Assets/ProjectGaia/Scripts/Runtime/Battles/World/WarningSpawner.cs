using System.Collections;
using System.Collections.Generic;
using Taijj.ProjectGaia.Utils.Factories;
using Taijj.ProjectGaia.Utils.SceneSingletons;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.World
{
	public class WarningSpawner : MonoBehaviour
	{
        [SerializeField] private GameObject _warningPrefab;

        public void Wake()
        {
            Warnings = new List<GameObject>();
        }

        public void ShowWarning(Vector3 position, float seconds)
        {
            SpawnWarning(position);
            CoroutineRunner.Instance.StartCoroutine(Warn(seconds));
        }

        private void SpawnWarning(Vector3 position)
        {
            GameObject warning = GameObjectFactory.Create(_warningPrefab, transform, $"Warning {Warnings.Count}");
            warning.transform.position = position;
            Warnings.Add(warning);
        }

        private IEnumerator Warn(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            DestroyLastWarning();
        }

        private void DestroyLastWarning()
        {
            GameObject warning = Warnings[0];
            Warnings.RemoveAt(0);
            GameObject.Destroy(warning);
        }

        private List<GameObject> Warnings { get; set; }
	}
}