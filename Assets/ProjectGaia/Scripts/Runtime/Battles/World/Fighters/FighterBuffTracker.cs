﻿using System.Collections.Generic;
using System;
using System.Linq;
using Taijj.ProjectGaia.StaticData.FighterStats;

namespace Taijj.ProjectGaia.Battle.World.Fighters
{
    public class FighterBuffTracker
    {
        #region Declarations
        private const int BUFF_LIMIT = 50;

        public void Wake(Fighter fighter)
        {
            Fighter = fighter;
            Buffs = new List<Buff>();
        }
        #endregion



        #region Add Buff
        public int RegisterBuff(Stat stat, int value)
        {
            Buff buff = new Buff(
                GetNextAvailableBuffId(),
                stat,
                value);

            Buffs.Add(buff);
            return buff.BuffId;
        }

        private int GetNextAvailableBuffId()
        {
            for(int i = 0; i < BUFF_LIMIT; i++)
            {
                int foundBuffIndex = Buffs.FindIndex(b => b.BuffId == i);
                if(foundBuffIndex == -1)
                {
                    return i;
                }
            }
            throw new Exception("Buff Limit breached!");
        }
        #endregion



        #region Remove Buff
        public void DeregisterBuff(int buffId)
        {
            Buff buff = GetBuff(buffId);
            Buffs.Remove(buff);
        }

        private Buff GetBuff(int id)
        {
            if(!HasBuff(id))
            {
                throw new ArgumentException("Buffs doesn't hold a buff with id " + id);
            }
            return Buffs[IndexOf(id)];
        }
        #endregion



        #region Misc
        public int GetBuffedValue(Stat stat)
        {
            List<Buff> statBuffs = Buffs.Where(b => b.Stat == stat).ToList();

            int buffedValue = 0;
            statBuffs.ForEach(b => buffedValue += b.Value);
            return buffedValue;
        }

        public bool HasBuff(int id)
        {
            return IndexOf(id) > -1;
        }

        public void Clear()
        {
            Buffs.Clear();
        }



        private int IndexOf(int id)
        {
            return Buffs.FindIndex(b => b.BuffId == id);
        }
        #endregion



        #region Properties
        private Fighter Fighter { get; set; }
        public List<Buff> Buffs { get; private set; }
        #endregion
    }
}