﻿using System;
using System.Collections.Generic;
using Taijj.ProjectGaia.StaticData.FighterStats;
using Taijj.ProjectGaia.Utils.Numerical;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.World.Fighters
{
    [Serializable]
    public class Stats
    {
        #region Main
        [HideInInspector] [SerializeField] private List<int> _values; // by Stat enum

        public int Get(Stat stat)
        {
            return _values[(int)stat];
        }

        public void Set(Stat stat, int value)
        {
            int clampedValue = NumberUtils.Clamp(value, 0, 9999);
            _values[(int)stat] = value;
        }
        #endregion



        #region Misc
        public Stats Copy()
        {
            Stats copy = new Stats();
            copy._values = new List<int>(this._values);
            return copy;
        }

        public void UpdateFromDatabase(StatsSection statsSection)
        {
            if(_values == null)
            {
                _values = new List<int>(statsSection.StatConfigs.Count);
                return;
            }

            WhileCounter.OnWhileLoopStart(30);
            while(_values.Count < statsSection.StatConfigs.Count)
            {
                WhileCounter.OnWhileLoopIteration();
                _values.Add(10); // default value;
            }
        }
        #endregion
    }
}