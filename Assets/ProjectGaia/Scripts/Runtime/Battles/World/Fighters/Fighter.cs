﻿using UnityEngine;
using System.Collections.Generic;
using Taijj.ProjectGaia.StaticData.Skills;
using System;
using Taijj.ProjectGaia.StaticData.Fighters;
using Taijj.ProjectGaia.Battle.Flow;
using Taijj.ProjectGaia.DynamicData;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;
using Taijj.ProjectGaia.StaticData.FighterStats;

namespace Taijj.ProjectGaia.Battle.World.Fighters
{
    public class Fighter : MonoBehaviour
    {
        #region Init
        public event Action StatsChanged;

        public void Wake(FighterModel model)
        {
            Model = model;

            BuffTracker = new FighterBuffTracker();
            BuffTracker.Wake(this);

            gameObject.AddComponent<SpriteRenderer>();
            SpriteRenderer.sortingLayerID = SortingLayer.NameToID("BattleScene");
            SpriteRenderer.sortingOrder = 1;
            SpriteRenderer.sprite = Model.Config.Sprite;
            SpriteRenderer.color = Model.Config.Color;
        }

        public void OnBattleStart()
        {
            BuffTracker.Clear();
            SpriteRenderer.color = SpriteRenderer.color.CloneAndSetA(1f);
        }
        #endregion



        #region Stats
        public int GetStat(Stat stat)
        {
            int statValue = Model.CurrentStats.Get(stat);
            int buffValue = BuffTracker.GetBuffedValue(stat);
            return statValue + buffValue;
        }

        public void SetStat(Stat stat, int value)
        {
            Model.CurrentStats.Set(stat, value);
            StatsChanged?.Invoke();
        }

        public float GetStatFraction(Stat stat)
        {
            return (float)GetStat(stat)/(float)DefaultStats.Get(stat);
        }
        #endregion



        #region Liveliness
        public void Die()
        {
            SpriteRenderer.color = SpriteRenderer.color.CloneAndSetA(0.2f);
        }

        public void Resurrect()
        {
            SpriteRenderer.color = SpriteRenderer.color.CloneAndSetA(1f);
        }
        #endregion



        #region Helpers
        public bool CanUse(SkillConfig skill)
        {
            if(!Model.Skills.Contains(skill))
            {
                return false;
            }

            SkillCost cost = skill.Cost;
            if(GetStat(cost.Stat) < cost.Amount && !cost.AllowOvercheck)
            {
                return false;
            }
            return true;
        }

        public List<Fighter> ToSingleList()
        {
            return new List<Fighter> { this };
        }
        #endregion



        #region Properties
        public FighterModel Model { get; set; }
        private SpriteRenderer SpriteRenderer => GetComponent<SpriteRenderer>();
        public FighterBuffTracker BuffTracker { get; private set; }



        public List<Resistance> Resistances => Model.Config.Resistances;
        public Stats DefaultStats => Model.DefaultStats;
        public bool IsHero => BattleFlow.BattleData.Roster.HeroParty.Contains(this);
        public bool IsAlive => GetStat(Stat.Vitality) > 0 && GetStat(Stat.Sanity) > 0;

        public float X => transform.position.x;
        public float Y => transform.position.y;
        #endregion
    }
}