﻿using Taijj.ProjectGaia.StaticData.FighterStats;

namespace Taijj.ProjectGaia.Battle.World.Fighters
{
    public struct Buff
    {
        #region Main
        public Buff(int buffId, Stat stat, int value)
        {
            BuffId = buffId;
            Stat = stat;
            Value = value;
        }

        public int BuffId { get; private set; }
        public Stat Stat { get; private set; }
        public int Value { get; private set; }
        #endregion



        #region Operator Override
        public static bool operator == (Buff b1, Buff b2)
        {
            return b1.BuffId == b2.BuffId && b1.Stat == b2.Stat && b1.Value == b2.Value;
        }

        public static bool operator != (Buff b1, Buff b2)
        {
            return b1.BuffId != b2.BuffId || b1.Stat != b2.Stat || b1.Value != b2.Value;
        }

        public override bool Equals(object obj)
        {
            return this == (Buff)obj;
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash = (hash * 7) + BuffId.GetHashCode();
            hash = (hash * 7) + Stat.GetHashCode();
            hash = (hash * 7) + Value.GetHashCode();
            return hash;
        }
        #endregion
    }
}