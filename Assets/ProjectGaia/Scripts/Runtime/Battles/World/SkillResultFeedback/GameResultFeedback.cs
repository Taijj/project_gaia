﻿using System;
using System.Collections.Generic;
using Taijj.ProjectGaia.StaticData;
using Taijj.ProjectGaia.Utils.Factories;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.World.SkillGameResult
{
    public class GameResultFeedback : MonoBehaviour
    {
        public event Action Completed;

        [SerializeField] private GameObject[] _prefabs; // By Result 0 - perfect, 1 - good, etc.

        public void Wake()
        {
            Animations = new List<ResultAnimation>();
        }

        public void ShowFeedback(Vector3 position, float result)
        {
            ResultAnimation animation = GameObjectFactory.CreateAs<ResultAnimation>(GetPrefab(result), transform, $"ResultAnimation {result}");
            animation.transform.position = position;
            animation.Completed += OnAnimationCompleted;
            animation.Play();

            Animations.Add(animation);
        }

        private GameObject GetPrefab(float result)
        {
            SkillGamesSection section = Database.SkillsSection.GamesSection;

            if(result >= section.PerfectTreshold)
            {
                return _prefabs[0];
            }
            if(result >= section.GoodTreshold)
            {
                return _prefabs[1];
            }
            if(result >= section.BadTreshold)
            {
                return _prefabs[2];
            }
            return _prefabs[3];
        }

        private void OnAnimationCompleted(ResultAnimation animation)
        {
            Animations.Remove(animation);
            animation.Completed -= OnAnimationCompleted;
            animation.DestructAndNullify();

            if(Animations.Count == 0)
            {
                Completed?.Invoke();
            }
        }

        private List<ResultAnimation> Animations { get; set; }
    }
}