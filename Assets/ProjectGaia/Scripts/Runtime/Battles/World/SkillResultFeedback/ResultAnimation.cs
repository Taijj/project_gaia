using System;
using Taijj.ProjectGaia.Utils.SceneSingletons;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.World.SkillGameResult
{
    public class ResultAnimation : MonoBehaviour
    {
        public event Action<ResultAnimation> Completed;

        public void Play()
        {
            Animation animation = gameObject.GetComponent<Animation>();
            CoroutineRunner.Instance.WaitForAnimationCompletion(animation.clip, delegate() { Completed?.Invoke(this); });
        }
    }
}