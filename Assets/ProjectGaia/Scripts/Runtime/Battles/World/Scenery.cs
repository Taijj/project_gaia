﻿using UnityEngine;
using Taijj.ProjectGaia.Battle.World.SkillEffects;
using Taijj.ProjectGaia.Battle.World.Layout;
using Taijj.ProjectGaia.Battle.World.SkillGameResult;
using UnityEngine.Serialization;

namespace Taijj.ProjectGaia.Battle.World
{
	public class Scenery : MonoBehaviour
	{
		[SerializeField] private BattleLayout _battleLayout;
		[SerializeField] private SkillVisualizer _skillVisualizer;
        [SerializeField] private Transform _skillGamesContainer;
		[SerializeField] private WarningSpawner _warningSpawner;
		[SerializeField] private GameResultFeedback _gameResultFeedback;

		public void Wake()
		{
			_skillVisualizer.Wake();
			_warningSpawner.Wake();
			_gameResultFeedback.Wake();
		}

        public Transform SkillGamesContainer => _skillGamesContainer;
		public SkillVisualizer SkillVisualizer => _skillVisualizer;
		public BattleLayout BattleLayout => _battleLayout;
		public WarningSpawner WarningSpawner => _warningSpawner;
		public GameResultFeedback GameResultFeedback => _gameResultFeedback;
	}
}