﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.World.SkillEffects.ImpactHits
{
    public class SkillHit : MonoBehaviour
    {
        public void Play(Vector3 position)
        {
            gameObject.transform.position = position;
            ParticleSystems = GetComponentsInChildren<ParticleSystem>(true).ToList();

            StartCoroutine(CheckParticles());
        }

        public IEnumerator CheckParticles()
        {
            while(true)
            {
                yield return new WaitForSeconds(0.1f);

                bool isRunning = ParticleSystems.Any(p => p.IsAlive(true));
                if(!isRunning)
                {
                    break;
                }
            }
            ParticleSystems.Clear();
            this.DestructAndNullify();
        }

        private List<ParticleSystem> ParticleSystems { get; set; }
    }
}