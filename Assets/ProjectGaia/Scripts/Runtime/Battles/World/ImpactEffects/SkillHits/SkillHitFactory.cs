﻿using Taijj.ProjectGaia.Utils.Factories;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.World.SkillEffects.ImpactHits
{
    public class SkillHitFactory : MonoBehaviour
    {
        [SerializeField] private GameObject _noEffectHitPrefab;

        public void ShowHit(Vector3 position)
        {
            GameObjectFactory.CreateAs<SkillHit>(_noEffectHitPrefab, transform, "NoHit").Play(position);
        }

        public void ShowHit(GameObject prefab, Vector3 position)
        {
            GameObjectFactory.CreateAs<SkillHit>(prefab, transform, "SkillHit").Play(position);
        }
    }
}