﻿using System;
using Taijj.ProjectGaia.Battle.World.SkillEffects.ImpactHits;
using Taijj.ProjectGaia.Battle.World.SkillEffects.Numbers;
using Taijj.ProjectGaia.StaticData;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.World.SkillEffects
{
	public class SkillVisualizer : MonoBehaviour
	{
		#region Init
		public event Action NumbersCompleted;

		[SerializeField] private NumbersAnimator _numbersAnimator;
		[SerializeField] private SkillHitFactory _hitFactory;

		public void Wake()
		{
			_numbersAnimator.Wake();
			_numbersAnimator.Completed += OnNumbersCompleted;
		}

		private void OnNumbersCompleted()
		{
			NumbersCompleted?.Invoke();
		}

		public NumbersAnimator NumbersAnimator => _numbersAnimator;
		#endregion



		#region Main
		public void VisualizeSkill(SkillVisualizationInput input)
		{
			TryShowingNumbers(input);
			ShowHit(input);
		}

		private void TryShowingNumbers(SkillVisualizationInput input)
		{
			if(input.showNumbers)
			{
				_numbersAnimator.ShowNumberAnimation(input);
				return;
			}
			OnNumbersCompleted();
		}

		private void ShowHit(SkillVisualizationInput input)
		{
			if(input.value != 0)
			{
				_hitFactory.ShowHit(input.hitPrefab, input.position);
				return;
			}
			_hitFactory.ShowHit(input.position);
		}
		#endregion
	}
}