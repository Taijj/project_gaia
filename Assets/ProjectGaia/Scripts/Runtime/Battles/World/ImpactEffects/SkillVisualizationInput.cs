using Taijj.ProjectGaia.StaticData.FighterStats;
using Taijj.ProjectGaia.StaticData.Skills;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.World.SkillEffects
{
	public class SkillVisualizationInput
    {
        public Vector3 position;
        public int value;
        public EffectKind effectKind;
        public GameObject hitPrefab;
        public bool showNumbers;
        public Stat numbersStat;

        public string AbsoluteText
        {
            get
            {
                if(value > 0)
                {
                    return value.ToString();
                }
                if(value < 0)
                {
                    return (-1*value).ToString();
                }
                return "";
            }
        }
    }
}