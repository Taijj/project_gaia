﻿using UnityEngine;
using TMPro;
using System;
using Taijj.ProjectGaia.Utils.SceneSingletons;
using Taijj.ProjectGaia.StaticData.FighterStats;

namespace Taijj.ProjectGaia.Battle.World.SkillEffects.Numbers
{
	[RequireComponent(typeof(Animation))]
	public class NumberAnimation : MonoBehaviour
	{
		public event Action<NumberAnimation> Completed;

		[SerializeField] private TextMeshPro _textMesh;
		[SerializeField] private SpriteRenderer _spriteRenderer;
		[SerializeField] private AnimationClip _clip;

		public void Play(SkillVisualizationInput input)
		{
			_textMesh.text = input.AbsoluteText;
			_spriteRenderer.sprite = input.numbersStat.Icon();
			_spriteRenderer.color = input.numbersStat.Color();
			GetComponentInChildren<Animation>().Play();

			CoroutineRunner.Instance.WaitForAnimationCompletion(_clip, Complete);
		}

		private void Complete()
		{
			Completed?.Invoke(this);
		}
	}
}