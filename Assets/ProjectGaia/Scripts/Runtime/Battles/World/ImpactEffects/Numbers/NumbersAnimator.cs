﻿using System;
using System.Collections.Generic;
using Taijj.ProjectGaia.Utils.Factories;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.World.SkillEffects.Numbers
{
	public class NumbersAnimator : MonoBehaviour
	{
		#region Fields
		public event Action Completed;
		[SerializeField] private float _spread = 0.25f;
		[SerializeField] GameObject[] prefabsByEffectKind;
		#endregion



		#region Main
		public void Wake()
		{
			NumberAnimations = new List<NumberAnimation>();
		}

		public void ShowNumberAnimation(SkillVisualizationInput input)
		{
			if(input.effectKind == EffectKind.NoEffect)
			{
				TryComplete();
				return;
			}
			int index = ((int)input.effectKind)-1;
			CreateAnimation( prefabsByEffectKind[index], input);
		}

		private void CreateAnimation(GameObject prefab, SkillVisualizationInput input)
		{
			Vector3 position = input.position + (UnityEngine.Random.insideUnitCircle*_spread).ToVector3();

			NumberAnimation newAnimation = GameObjectFactory.CreateAs<NumberAnimation>(prefab, transform, input.AbsoluteText);
			newAnimation.transform.position = position;
			newAnimation.Completed += OnAnimationCompleted;
			newAnimation.Play(input);
			NumberAnimations.Add(newAnimation);
		}

		private void OnAnimationCompleted(NumberAnimation animation)
		{
			DestroyAnimation(animation);
			TryComplete();
		}

		private void TryComplete()
		{
			if(NumberAnimations.Count == 0)
			{
				Completed?.Invoke();
			}
		}

		private void DestroyAnimation(NumberAnimation animation)
		{
			NumberAnimations.Remove(animation);
			animation.DestructAndNullify();
		}

		private List<NumberAnimation> NumberAnimations { get; set; }
		#endregion
	}
}