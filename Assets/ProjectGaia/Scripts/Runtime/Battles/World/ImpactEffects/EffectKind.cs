﻿namespace Taijj.ProjectGaia.Battle.World.SkillEffects
{
	public enum EffectKind
	{
		NoEffect = 0,
		Beneficial = 1,
		Normal = 2,
		Critical = 3,
		Weak = 4
	}
}