﻿using UnityEngine;

namespace Taijj.ProjectGaia.Battle.World.Layout.Previews
{
    [ExecuteAlways]
    public abstract class LineupPreview : MonoBehaviour
    {
        #region Init
        public void Awake()
        {
            Wake();
        }

        protected abstract void Wake();



        public void Update()
        {
            if(PreviewsCount == 0 && PreviewsCount == transform.childCount)
            {
                return;
            }

            if(transform.childCount == PreviewsCount)
            {
                LayoutPreviews();
                return;
            }

            if(transform.childCount < PreviewsCount)
            {
                StockUpPreviews();
            }
            if(transform.childCount > PreviewsCount)
            {
                StockDownPreviews();
            }
            UpdateSprites();
            LayoutPreviews();
        }

        public void OnValidate()
        {
            if(PreviewsCount == 0)
            {
                return;
            }
            UpdateSprites();
        }

        protected abstract int PreviewsCount { get; }
        #endregion



        #region Main
        private void StockDownPreviews()
        {
            WhileCounter.OnWhileLoopStart(20);
            while(transform.childCount != PreviewsCount)
            {
                WhileCounter.OnWhileLoopIteration();
                Transform child = transform.GetChild(0);
                GameObject.DestroyImmediate(child.gameObject);
            }
        }

        private void StockUpPreviews()
        {
            WhileCounter.OnWhileLoopStart(20);
            while(transform.childCount != PreviewsCount)
            {
                WhileCounter.OnWhileLoopIteration();
                GameObject preview = new GameObject("Preview");
                preview.transform.SetParent(transform);

                SpriteRenderer renderer = (SpriteRenderer)preview.AddComponent(typeof(SpriteRenderer));
                renderer.sortingOrder++;
            }
        }



        protected abstract void LayoutPreviews();
        protected abstract void UpdateSprites();
        #endregion
    }
}