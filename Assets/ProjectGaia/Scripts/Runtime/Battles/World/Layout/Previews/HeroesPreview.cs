﻿using System.Collections.Generic;
using Taijj.ProjectGaia.StaticData.Fighters;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.World.Layout.Previews
{
    [RequireComponent(typeof(HeroesLineup))]
    public class HeroesPreview : LineupPreview
    {
        #region Init
        [SerializeField] [Range(1, 10)] private int _previewsCount = 4;
        [SerializeField] private List<FighterConfig> _heroesToShow;

        protected override void Wake()
        {
            Lineup = GetComponent<HeroesLineup>();
        }

        protected override int PreviewsCount => _previewsCount;
        private HeroesLineup Lineup { get; set; }
        #endregion



        #region Main
        protected override void LayoutPreviews()
        {
            for(int i = 0; i < _previewsCount; i++)
            {
                transform.GetChild(i).position = Lineup.GetPosition(i, _previewsCount);
            }
        }

        protected override void UpdateSprites()
        {
            if(_heroesToShow.Count == 0)
            {
                return;
            }

            SpriteRenderer[] renderers = GetComponentsInChildren<SpriteRenderer>();
            foreach(SpriteRenderer renderer in renderers)
            {
                FighterConfig hero = _heroesToShow.GetRandomElement();
                if(hero == null)
                {
                    continue;
                }
                renderer.sprite = hero.Sprite;
            }
        }
        #endregion
    }
}