﻿using System.Collections.Generic;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.World.Layout.Previews
{
    public class EnemiesPreview : LineupPreview
    {
        override protected void Wake()
        {
            Enemies = new List<PositionedEnemy>();
        }

        protected override void LayoutPreviews()
        {
            for(int i = 0; i < PreviewsCount; i++)
            {
                transform.GetChild(i).position = Enemies[i].position;
            }
        }

        protected override void UpdateSprites()
        {
            SpriteRenderer[] renderers = GetComponentsInChildren<SpriteRenderer>();
            for(int i = 0; i < PreviewsCount; i++)
            {
                renderers[i].sprite = Enemies[i].config.Sprite;
            }
        }

        public List<PositionedEnemy> Enemies { private get; set; }
        protected override int PreviewsCount => Enemies != null ? Enemies.Count : 0;
    }
}