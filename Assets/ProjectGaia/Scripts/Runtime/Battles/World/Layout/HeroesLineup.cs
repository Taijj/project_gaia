﻿using Taijj.ProjectGaia.Attributes;
using Taijj.ProjectGaia.Utils.Numerical;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.World.Layout
{
    public class HeroesLineup : MonoBehaviour
    {
        [SerializeField] [Disabled] private Vector3Range _layoutPoints;

        public void Reset()
        {
            _layoutPoints.min = Vector3.up;
            _layoutPoints.max = Vector3.down;
        }

        public Vector3 GetPosition(int index, int total)
        {
            Vector3 minToMax = (LayoutPoints.max - LayoutPoints.min).normalized;
            int spacingDivisor = total > 2 ? total-1 : total+1;
            float spacing = LayoutPoints.Distance / (float)(spacingDivisor);

            float spacingMultiplier = total > 2 ? (float)index : (float)(index+1);
            return LayoutPoints.min + spacingMultiplier * spacing*minToMax;
        }

        public Vector3Range LayoutPoints { get { return _layoutPoints; } set { _layoutPoints = value; } }
    }
}