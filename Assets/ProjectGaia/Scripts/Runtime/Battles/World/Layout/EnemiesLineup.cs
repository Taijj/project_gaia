﻿using System.Collections.Generic;
using UnityEngine;
using Taijj.ProjectGaia.Attributes;
using Taijj.ProjectGaia.Utils.Numerical;

namespace Taijj.ProjectGaia.Battle.World.Layout
{
    public class EnemiesLineup : MonoBehaviour
    {
        [SerializeField] [Disabled] private List<PositionedEnemy> _enemies;

        public void Clear()
        {
            _enemies.Clear();
        }

        public void AddEnemy(PositionedEnemy enemy)
        {
            _enemies.Add(enemy);
        }



        private FloatRange GetHorizontalBorders()
        {
            FloatRange result = new FloatRange();
            result.min = float.MaxValue;
            result.max = float.MinValue;
            foreach(PositionedEnemy enemy in _enemies)
            {
                if(enemy.position.x < result.min)
                {
                    result.min = enemy.position.x;
                }
                if(enemy.position.x > result.max)
                {
                    result.max = enemy.position.x;
                }
            }
            return result;
        }

        private FloatRange GetVerticalBorders()
        {
            FloatRange result = new FloatRange();
            result.min = float.MaxValue;
            result.max = float.MinValue;
            foreach(PositionedEnemy enemy in _enemies)
            {
                if(enemy.position.y < result.min)
                {
                    result.min = enemy.position.y;
                }
                if(enemy.position.y > result.max)
                {
                    result.max = enemy.position.y;
                }
            }
            return result;
        }

        public List<PositionedEnemy> Enemies => _enemies;
    }
}