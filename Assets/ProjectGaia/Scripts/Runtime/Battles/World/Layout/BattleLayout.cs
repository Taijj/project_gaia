﻿using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.Flow;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.DynamicData;
using Taijj.ProjectGaia.StaticData.Fighters;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.World.Layout
{
    public class BattleLayout : MonoBehaviour
    {
        #region Main
        [SerializeField] private HeroesLineup _heroesLineup;
        [SerializeField] private EnemiesLineup _enemiesLineup;

        public void CreatePartiess()
        {
            CreateHeroes();
            CreateEnemies();
        }

        public void DestroyParties()
        {
            Heroes.DestroyGameObjects();
            Enemies.DestroyGameObjects();
        }

        private BattleSetup Setup => BattleFlow.BattleData.Setup;
        public List<Fighter> Heroes { get; set; }
        public List<Fighter> Enemies { get; set; }
        #endregion



        #region Fighter Creation
        private void CreateHeroes()
        {
            Heroes = new List<Fighter>();
            _heroesLineup.LayoutPoints = Setup.Lineup.HeroesLayoutPoints;
            Setup.HeroModels.For((i, m) => Heroes.Add(CreateFighter(m, _heroesLineup.GetPosition(i, Setup.HeroModels.Count), _heroesLineup.transform)));
        }

        private void CreateEnemies()
        {
            Enemies = new List<Fighter>();
            _enemiesLineup.Clear();
            Setup.Lineup.PositionedEnemies.ForEach( e => _enemiesLineup.AddEnemy(e));
            _enemiesLineup.Enemies.ForEach(e => Enemies.Add(CreateFighter( CreateEnemyModel(e.config), e.position, _enemiesLineup.transform)));
        }

        private FighterModel CreateEnemyModel(FighterConfig config)
        {
            FighterModel model = new FighterModel(config);
            config.Skills.ForEach(s => model.Learn(s));
            return model;
        }

        private Fighter CreateFighter(FighterModel model, Vector3 position, Transform container)
        {
            GameObject fighterObject = new GameObject(model.Config.name);
            fighterObject.transform.SetParent(container);
            fighterObject.transform.position = position;

            Fighter fighter = fighterObject.AddComponent<Fighter>();
            fighter.Wake(model);
            return fighter;
        }
        #endregion
    }
}