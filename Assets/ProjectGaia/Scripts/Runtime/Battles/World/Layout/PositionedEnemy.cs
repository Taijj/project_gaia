﻿using System;
using Taijj.ProjectGaia.StaticData.Fighters;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.World.Layout
{
    [Serializable]
    public class PositionedEnemy
    {
        public FighterConfig config;
        public Vector3 position;
    }
}