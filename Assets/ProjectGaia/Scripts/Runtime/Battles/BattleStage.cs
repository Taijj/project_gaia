﻿using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.Flow;
using Taijj.ProjectGaia.Battle.UserInterface;
using Taijj.ProjectGaia.Battle.World;
using Taijj.ProjectGaia.StaticData;
using Taijj.ProjectGaia.StaticData.Fighters;
using UnityEngine;
using Taijj.ProjectGaia.DynamicData;
using System;
using Taijj.ProjectGaia.StaticData.Skills;
using Taijj.ProjectGaia.StaticData.FighterStats;

namespace Taijj.ProjectGaia.Battle
{
	public class BattleStage : MonoBehaviour
	{
		[Serializable]
		public class Round
		{
			public List<FighterConfig> heroToAdd;
			public List<SkillConfig> skillsToBeLearned;
			public LineupConfig lineup;
			public TutorialDisplay.Kind tutorial;
		}


		#region Injections
		[SerializeField] private BattleUserInterface _userInterface;
		[SerializeField] private Scenery _scenery;
		[Space]
		[SerializeField] private List<Round> _rounds;
		#endregion



		#region Init
		public void Wake()
		{
			BattleFlow = new BattleFlow();
			BattleFlow.OnBattleComplete += OnBattleCompleted;

			Scenery = _scenery;
			_scenery.Wake();

			UserInterface = _userInterface;
			UserInterface.EndDisplay.Completed += StartNextRound;
			UserInterface.TutorialDisplay.Completed += StartBattle;
			_userInterface.Wake();
		}

		public void Initialize()
		{
			BattleFlow.Initialize();
		}
		#endregion



		#region Flow
		public void StartBattles()
		{
			CurrentRound = 0;
			StartNextRound();
		}

		private void StartNextRound()
		{
			ReviveDeadHeroes();

			Round round = _rounds[CurrentRound];
			round.heroToAdd.ForEach(h => DataModel.Party.Join(h.Id));
			round.skillsToBeLearned.ForEach(s => TeachSkill(s));

			BattleSetup setup = new BattleSetup(DataModel.Party.Current, round.lineup);
			BattleFlow.PrepareBattle(setup);
			UserInterface.OnBattleStart();

			ShowTutorial(round.tutorial);
		}

		private void ReviveDeadHeroes()
		{
			foreach(FighterModel hero in DataModel.Party.Current)
			{
				TryReplenish(hero, Stat.Vitality);
				TryReplenish(hero, Stat.Sanity);
			}
		}

		private void TryReplenish(FighterModel hero, Stat stat)
		{
			if(hero.CurrentStats.Get(stat) <= 0)
			{
				int healedAmount = (int)((float)hero.DefaultStats.Get(stat) * 0.1f);
				hero.CurrentStats.Set(stat, (int)( (float)hero.DefaultStats.Get(stat) * 0.3f ));
			}
		}

		private void ShowTutorial(TutorialDisplay.Kind kind)
		{
			if(kind != TutorialDisplay.Kind.None)
			{
				UserInterface.TutorialDisplay.Show(kind);
			}
			else
			{
				StartBattle();
			}
		}

		private void StartBattle()
		{
			UserInterface.TutorialDisplay.Hide();
			UserInterface.IntroDisplay.Show(delegate () { BattleFlow.StartBattle(); });
		}



		private void TeachSkill(SkillConfig skill)
		{
			foreach(FighterModel model in DataModel.Party.Current)
			{
				if(!model.Config.Skills.Contains(skill))
				{
					continue;
				}
				model.Learn(skill);
			}
		}

		public void OnBattleCompleted(bool hasWon)
		{
			UserInterface.OnBattleEnd();

			if(!hasWon)
			{
				Loose();
				return;
			}

			CurrentRound++;
			if(CurrentRound < _rounds.Count)
			{
				WinRound();
			}
			else
			{
				WinEverything();
			}
		}

		private void Loose()
		{
			CurrentRound = 0;
			DataModel.Party.Reset();
			UserInterface.EndDisplay.Show("You lost! Start over and try again!");
		}

		private void WinRound()
		{
			UserInterface.EndDisplay.Show("You won this round! But there's more!");
		}

		private void WinEverything()
		{
			CurrentRound = 0;
			DataModel.Party.Reset();
			UserInterface.EndDisplay.Show("You beat everyone! Wanna' start over?");
		}

		private int CurrentRound { get; set; }
		#endregion



		#region Properties
		private BattleFlow BattleFlow { get; set; }

		public static Scenery Scenery { get; private set; }
		public static BattleUserInterface UserInterface { get; private set; }
		#endregion
	}
}