﻿using Taijj.ProjectGaia.Battle.Flow.Turn.Order.Clairvoyance;
using Taijj.ProjectGaia.Battle.Flow.Turn.Order.Defending;
using Taijj.ProjectGaia.Battle.Flow.Utils;
using Taijj.ProjectGaia.Battle.World.Fighters;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Order
{
    public class OrderManager
    {
        #region Init
        public OrderManager()
        {
            Barrage = new Barrage(this);
            Oracle = new Oracle(this);

            HealthObserver.WasKilled += OnFighterWasKilled;
            HealthObserver.WasResurrected += OnFighterWasResurrected;
        }
        #endregion



        #region Battle Scope
        public void OnBattleStart()
        {
            Oracle.FillQueue();
        }

        public void OnBattleEnd()
        {
            Oracle.Clear();
            Barrage.Clear();
        }



        private void OnFighterWasKilled(Fighter fighter)
        {
            Oracle.Queue.OnFighterKilled(fighter);
            Barrage.OnFighterKilled(fighter);
        }

        private void OnFighterWasResurrected(Fighter fighter)
        {
            Oracle.Queue.AddFighter(fighter);
        }
        #endregion



        #region Turn Scope
        public void OnTurnStart()
        {
            Oracle.PredictVanilla();
            Oracle.ApplyPredictions();
            Barrage.TryStaffingDown();

            SimulationIsEnforced = false;
        }

        public void SetCurrentFighterAsDefender()
        {
            Barrage.StaffUp();
            SimulationIsEnforced = true;
        }

        public bool IsDefenderTurn { get { return Barrage.HasDefenders && !SimulationIsEnforced; } }
        #endregion



        #region Properties
        public Barrage Barrage { get; set; }
        public Oracle Oracle { get; set; }

        public bool SimulationIsEnforced { get; set; }
        public Fighter CurrentFighter { get { return Oracle.TurnFighter; } }
        #endregion
    }
}