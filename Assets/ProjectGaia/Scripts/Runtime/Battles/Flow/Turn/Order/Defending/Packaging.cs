﻿using System.Collections.Generic;
using System.Linq;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.Skills.Types;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Order.Defending
{
    public class Packaging
    {
        #region Beginning
        private class Package
        {
            public Fighter fighter;
            public int buffId;
        }

        public Packaging()
        {
            Packages = new List<Package>();
        }

		public void Clear()
        {
            Packages.Clear();
        }

		private List<Package> Packages { get; set; }
        #endregion



        #region Tying Up
        public void TieyUpPackageFor(Fighter fighter)
        {
            FighterToTieUp = fighter;
            Packages.Add(CreatePackage());
        }

        private Package CreatePackage()
        {
            Package package = new Package();
            package.fighter = FighterToTieUp;
            package.buffId = AddBuffAndGetId();
            return package;
        }

        private int AddBuffAndGetId()
        {
            DefendSkillConfig skill = BattleFlow.BattleData.CurrentScheme.selectedSkill as DefendSkillConfig;
            int buffStrength = (int)( (float)FighterToTieUp.GetStat(skill.DefenseStat) * skill.DefenseMultiplier );
            int buffId = FighterToTieUp.BuffTracker.RegisterBuff(skill.DefenseStat, buffStrength);
            return buffId;
        }

		private Fighter FighterToTieUp { get; set; }
        #endregion



        #region Tossing
        public void TossPackageOf(Fighter fighter)
        {
            FighterToToss = fighter;
            TossPackage( GetCurrentPackage() );
        }

        private void TossPackage(Package package)
        {
            FighterToToss.BuffTracker.DeregisterBuff(package.buffId);
            Packages.Remove(package);
        }

        private Package GetCurrentPackage()
        {
            return Packages.FirstOrDefault(p => p.fighter == FighterToToss);
        }

		private Fighter FighterToToss { get; set; }
        #endregion
    }
}