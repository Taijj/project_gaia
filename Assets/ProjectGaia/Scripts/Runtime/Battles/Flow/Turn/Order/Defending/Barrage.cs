﻿using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.World.Fighters;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Order.Defending
{
	/// <summary>
	/// Barrage is the top level management entity for defending fighters.
	/// </summary>
	public class Barrage
	{
		#region Init
		public Barrage(OrderManager manager)
		{
			Manager = manager;
			Packaging = new Packaging();
			Validator = new Validator(this);
			Defenders = new List<Fighter>();
		}

		public void Clear()
		{
			Packaging.Clear();
			Defenders.Clear();
		}
		#endregion



		#region Staffing
        public void StaffUp()
        {
			Fighter defender = Manager.CurrentFighter;
			Validator.ValidateFighterAdding(defender);
			TransferHere(defender);
        }

		private void TransferHere(Fighter defender)
		{
            Manager.Oracle.Queue.RemoveFighter(defender);
			Packaging.TieyUpPackageFor(defender);
			Defenders.Add(defender);
		}



		public void TryStaffingDown()
        {
            if(Manager.IsDefenderTurn)
            {
				Fighter firstDefender = Defenders[0];
				Validator.ValidateFighterRemoving(firstDefender);
				TransferBack(firstDefender);
            }
        }

		private void TransferBack(Fighter defender)
		{
			Packaging.TossPackageOf(defender);
			Defenders.Remove(defender);
			Manager.Oracle.Queue.AddFighter(defender);

		}

		public void OnFighterKilled(Fighter fighter)
        {
            if(Validator.Controls(fighter))
            {
                Defenders.Remove(fighter);
            }
        }
		#endregion



		#region Properties
		private OrderManager Manager { get; set; }
		private Packaging Packaging { get; set; }
		private Validator Validator { get; set; }

		public List<Fighter> Defenders { get; private set; }
		public bool HasDefenders { get { return Defenders.Count > 0; } }
		#endregion
	}
}