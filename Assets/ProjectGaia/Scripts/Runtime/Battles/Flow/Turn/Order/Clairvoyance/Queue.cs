﻿using System.Collections.Generic;
using System;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.FighterStats;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Order.Clairvoyance
{
    public class Queue
    {
        #region Init
        public Queue()
        {
            TimesByFighter = new Dictionary<Fighter, int>();
        }

        public Dictionary<Fighter, int> TimesByFighter { get; private set; }
        #endregion



        #region Add/Remove
        public void AddFighter(Fighter fighter)
        {
            if(HasFighter(fighter))
            {
                throw new ApplicationException(fighter.gameObject.name + " is already controlled by " + this.GetType().ToString());
            }
            TimesByFighter.Add(fighter, Stat.Speed.MaxValue());
        }

        public void RemoveFighter(Fighter fighter)
        {
            if(!HasFighter(fighter))
            {
                throw new ApplicationException(fighter.gameObject.name + " is not controlled by " + this.GetType().ToString());
            }
            TimesByFighter.Remove(fighter);
        }

        public void OnFighterKilled(Fighter fighter)
        {
            if(HasFighter(fighter))
            {
                RemoveFighter(fighter);
            }
        }
        #endregion



        #region Tools
        private bool HasFighter(Fighter fighter)
        {
            return TimesByFighter.ContainsKey(fighter);
        }

        public void Clear()
        {
            TimesByFighter.Clear();
        }

        public void SyncTimes(Dictionary<Fighter, int> newTimesByFighter)
        {
            foreach(KeyValuePair<Fighter, int> timeByFighter in newTimesByFighter)
            {
                if(HasFighter(timeByFighter.Key))
                {
                    TimesByFighter[timeByFighter.Key] = timeByFighter.Value;
                }
                else
                {
                    throw new ApplicationException("Tried to sync time for " + timeByFighter.Key.name + ", which is not controlled by " + this.GetType().ToString());
                }
            }
        }
        #endregion
    }
}