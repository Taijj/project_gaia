﻿using System.Collections.Generic;
using System.Linq;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.FighterStats;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Order.Clairvoyance.Simulation
{
	public class TimeTicker
	{
		#region Entries
		public struct Entry
		{
			public Entry(Fighter fighter, int time, int speed)
			{
				Fighter = fighter;
				Time = time;
				Speed = speed;
			}

			public Fighter Fighter { get; private set; }
			public int Time { get; private set; }
			public int Speed { get; private set; }

			public Entry CopyTicked()
			{
				return new Entry(Fighter, Time - Speed, Speed);
			}

			public Entry CopyReset(int maxSpeedValue)
			{
				return new Entry(Fighter, Time + maxSpeedValue, Speed);
			}
		}
		#endregion



		#region Main
		public TimeTicker()
		{
			ResultEntries = new List<Entry>();
		}

		public void CountDown(List<Entry> entries, int turns)
		{
			OperandEntries = entries;
			ResultEntries.Clear();

			for(int i = 0; i < turns; i++)
			{
				CountDownRound();
				RecordResult();
			}
		}
		#endregion



		#region Countdown
		private void CountDownRound()
		{
			WhileCounter.OnWhileLoopStart(maxIterations: Stat.Speed.MaxValue()+1);
			while(GetLowestTime() > 0)
			{
				CountDownTimes();
				WhileCounter.OnWhileLoopIteration();
			}
		}

		private int GetLowestTime()
		{
			return OperandEntries.Min(c => c.Time);
		}

		private void CountDownTimes()
		{
			List<Entry> countingEntries = new List<Entry>();
			foreach(Entry entry in OperandEntries)
			{
				countingEntries.Add( entry.CopyTicked());
			}
			OperandEntries = countingEntries;
		}
		#endregion



		#region Recording
		private void RecordResult()
		{
			RecordAndResetLowestEntry();
			TryFirstRoundSnapshot();
		}

		private void RecordAndResetLowestEntry()
		{
			Entry lowestEntry = OperandEntries.First(c => c.Time == GetLowestTime());
			int index = OperandEntries.IndexOf(lowestEntry);

			OperandEntries[index] = lowestEntry.CopyReset(Stat.Speed.MaxValue());
			ResultEntries.Add(lowestEntry);
		}

		private void TryFirstRoundSnapshot()
		{
			if(ResultEntries.Count == 1)
			{
				FirstRoundSnapShot = OperandEntries.Copy();
			}
		}
		#endregion



		#region Properties
		private List<Entry> OperandEntries { get; set; }
		public List<Entry> ResultEntries { get; set; }
		public List<Entry> FirstRoundSnapShot { get; set; }
		#endregion
	}
}