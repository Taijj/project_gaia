﻿using System;
using Taijj.ProjectGaia.Battle.World.Fighters;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Order.Defending
{
    public class Validator
    {
        #region Init
        public Validator(Barrage barrage)
        {
            Barrage = barrage;
        }

        private Barrage Barrage { get; set; }
        #endregion



        #region Validation
        public void ValidateFighterAdding(Fighter fighter)
        {
            if(Controls(fighter))
            {
                throw new ApplicationException(fighter.gameObject.name + " is already controlled by " + this.GetType().ToString());
            }
        }

        public void ValidateFighterRemoving(Fighter fighter)
        {
            if(!Controls(fighter))
            {
                throw new ApplicationException(fighter.gameObject.name + " is is not controlled by " + this.GetType().ToString());
            }
        }

        public bool Controls(Fighter fighter)
        {
            return Barrage.Defenders.Contains(fighter);
        }
        #endregion
    }
}