﻿using System.Collections.Generic;
using System.Linq;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.FighterStats;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Order.Clairvoyance.Simulation
{
	public class TimeSimulation
	{
		#region Beginning
		private const int TURN_SIMULATIONS = 20;

		public TimeSimulation()
		{
			Ticker = new TimeTicker();
		}
		#endregion



		#region Main
		public void Run(Dictionary<Fighter, int> timesByFighter)
        {
			TimesByFighter = new Dictionary<Fighter, int>(timesByFighter);
            List<TimeTicker.Entry> tickerEntries = new List<TimeTicker.Entry>();
            foreach(KeyValuePair<Fighter, int> timeByFighter in TimesByFighter)
            {
				int speed = UnityEngine.Mathf.Max(timeByFighter.Key.GetStat(Stat.Speed), 1);

                TimeTicker.Entry entry = new TimeTicker.Entry(
                    timeByFighter.Key,
                    timeByFighter.Value,
                    speed);
                tickerEntries.Add(entry);
            }
			Ticker.CountDown(tickerEntries, TURN_SIMULATIONS);
		}

		public Dictionary<Fighter, int> GetCurrentTurnTimes()
		{
			Dictionary<Fighter, int> result = new Dictionary<Fighter, int>();
			foreach(TimeTicker.Entry entry in Ticker.FirstRoundSnapShot)
			{
				if(entry.Fighter.IsAlive)
				{
					result.Add(entry.Fighter, entry.Time);
				}
			}
			return result;
		}
		#endregion



		#region Properties
		private Dictionary<Fighter, int> TimesByFighter;
		private TimeTicker Ticker { get; set; }
		public List<Fighter> SimulatedFighterOrder { get { return Ticker.ResultEntries.Select(e => e.Fighter).ToList(); } }
		#endregion
	}
}