﻿using System;
using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.Flow.Turn.Order.Clairvoyance.Simulation;
using Taijj.ProjectGaia.Battle.World.Fighters;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Order.Clairvoyance
{
	public class Oracle
	{
		#region Beginning
		public static event Action<Fighter> OnCurrentFighterUpdated;
        public static event Action< List<Fighter> > OnOrderSimulated;

		public Oracle(OrderManager manager)
		{
			Manager = manager;
			Simulation = new TimeSimulation();
			Queue = new Queue();
		}

		public void FillQueue()
		{
			BattleFlow.BattleData.Roster.AllFighters.ForEach(f => Queue.AddFighter(f));
		}

		public void Clear()
		{
			Queue.Clear();
		}
		#endregion



		#region Predictions
		public void PredictVanilla()
        {
			if(!Manager.IsDefenderTurn)
            {
                Simulation.Run(Queue.TimesByFighter);
            }
            BuildOrder(Manager.Barrage.Defenders, Manager.IsDefenderTurn);
            OnOrderSimulated?.Invoke(FightersInOrder);
        }

        public void PredictDefending()
        {
            Dictionary<Fighter, int> whatIfTimes = new Dictionary<Fighter, int>(Queue.TimesByFighter);
            whatIfTimes.Remove(Manager.CurrentFighter);
            Simulation.Run(whatIfTimes);

            List<Fighter> whatIfDefenders = Manager.Barrage.Defenders.Copy();
            whatIfDefenders.Add(Manager.CurrentFighter);

            BuildOrder(whatIfDefenders, false);
            OnOrderSimulated?.Invoke(FightersInOrder);
        }

        private void BuildOrder(List<Fighter> defenders, bool isDefenderTurn)
        {
			int defendersInsertionIndex = isDefenderTurn ? 0 : 1;
			FightersInOrder = Simulation.SimulatedFighterOrder.Copy();
			FightersInOrder.InsertRange(defendersInsertionIndex, defenders);
        }



		public void ApplyPredictions()
        {
            TurnFighter = FightersInOrder[0];
            Queue.SyncTimes( Simulation.GetCurrentTurnTimes() );
            OnCurrentFighterUpdated?.Invoke(TurnFighter);
        }
		#endregion



		#region Properties
		private OrderManager Manager { get; set; }
		private TimeSimulation Simulation { get; set; }

		public Queue Queue { get; private set; }
		public Fighter TurnFighter { get; private set; }
		public List<Fighter> FightersInOrder { get; private set; }
		#endregion
	}
}