﻿namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Start
{
    public class StartTurnPhase : TurnPhase
    {
        protected override void SetKind() { Kind = TurnPhaseKind.Start; }

        public override void OnEnter()
        {
            BattleFlow.TurnExecutor.OrderManager.OnTurnStart();
            BattleFlow.BattleData.CurrentFighter = BattleFlow.TurnExecutor.OrderManager.CurrentFighter;
            CallCompleteHook();
        }

        public override void OnExit()
        {}
    }
}