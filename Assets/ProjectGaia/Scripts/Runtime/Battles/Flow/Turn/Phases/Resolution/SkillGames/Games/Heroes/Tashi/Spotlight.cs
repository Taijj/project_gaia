﻿using UnityEngine;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames.Games.Heroes.Sam.Scold
{
    public class Spotlight : MonoBehaviour
    {
        #region Main
        [SerializeField] private float _movementSeconds;

        public void StartMoving(float movementRadius, float proficiency)
        {
            MovementRadius = movementRadius;
            MovementSeconds = _movementSeconds * (1f+proficiency);
            Move();
        }

        public void StopMoving()
        {
            LeanTween.cancel(gameObject);
        }
        #endregion



        #region Movement
        private void Move()
        {
            Vector3 nextPosition = GetNextPosition();
            float seconds = GetNormalizedMovementSeconds(nextPosition);

            LeanTween.moveLocal(gameObject, nextPosition, seconds)
            .setIgnoreTimeScale(true)
            .setOnComplete(Move);
        }

        private Vector2 GetNextPosition()
        {
            Vector2 normalizedPosition = Random.insideUnitCircle;
            float radius = Mathf.Max(0.5f, Random.value * MovementRadius);
            return normalizedPosition * radius;
        }

        private float GetNormalizedMovementSeconds(Vector3 nextPosition)
        {
            return MovementSeconds * Vector3.Distance(gameObject.transform.localPosition, nextPosition)/MovementRadius;
        }

        private float MovementSeconds { get; set; }
        private float MovementRadius { get; set; }
        #endregion
    }
}