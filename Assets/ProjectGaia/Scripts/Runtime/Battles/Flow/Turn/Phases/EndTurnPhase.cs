﻿using System;
using Taijj.ProjectGaia.Battle.Flow.Utils;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.Battle.World.SkillEffects;
using Taijj.ProjectGaia.StaticData;
using Taijj.ProjectGaia.StaticData.FighterStats;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.End
{
    public class EndTurnPhase : TurnPhase
    {
        #region Main
        protected override void SetKind() { Kind = TurnPhaseKind.End; }

        public override void OnEnter()
        {
            ApplySkillCosts();
            TryStatRegeneration();
            BattleFlow.BattleData.CurrentTurn++;
        }

        private void ApplySkillCosts()
        {
            DebtCollector.CollectSkillCosts(
                BattleFlow.BattleData.CurrentFighter,
                BattleFlow.BattleData.CurrentScheme.selectedSkill,
                SkillCost.DueMoment.AtTurnEnd);
        }
        #endregion



        #region Stat Regeneration
        private void TryStatRegeneration()
        {
            BattleStage.Scenery.SkillVisualizer.NumbersCompleted += CompleteRegeneration;
            if(CurrentFighter.IsHero)
            {
                RegenerateStats();
            }
            else
            {
                CompleteRegeneration();
            }
        }

        private void RegenerateStats()
        {
            bool somethingRegenerated = false;
            foreach(Regeneration regeneration in Database.StatSection.Regenerations)
            {
                int regeneratedAmount = Regenerate(regeneration);
                if(regeneratedAmount != 0)
                {
                    VisualizeRegeneration(regeneration, regeneratedAmount);
                    somethingRegenerated = true;
                }
            }

            if(!somethingRegenerated)
            {
                CompleteRegeneration();
            }
        }

        private int Regenerate(Regeneration regeneration)
        {
            float multiplier = BattleFlow.TurnExecutor.OrderManager.Barrage.Defenders.Contains(CurrentFighter) ? Database.StatSection.DefendingRegenerationMultiplier : 1f;

            int oldValue = CurrentFighter.GetStat(regeneration.Stat);
            int newValue = Math.Min(oldValue + (int)(regeneration.Amount*multiplier), CurrentFighter.DefaultStats.Get(regeneration.Stat));
            CurrentFighter.SetStat(regeneration.Stat, newValue);

            return newValue - oldValue;
        }

        private void VisualizeRegeneration(Regeneration regeneration, int regeneratedAmount)
        {
            SkillVisualizationInput input = new SkillVisualizationInput();
            input.numbersStat = regeneration.Stat;
            input.position = CurrentFighter.transform.position;
            input.value = regeneratedAmount;
            input.effectKind = EffectKind.Beneficial;
            BattleStage.Scenery.SkillVisualizer.NumbersAnimator.ShowNumberAnimation(input);
        }

        private void CompleteRegeneration()
        {
            BattleStage.Scenery.SkillVisualizer.NumbersCompleted -= CompleteRegeneration;
            CallCompleteHook();
        }

        public override void OnExit()
        {}

        private Fighter CurrentFighter => BattleFlow.BattleData.CurrentFighter;
        #endregion
    }
}