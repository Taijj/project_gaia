﻿using System;
using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.Battle.World.SkillEffects;
using Taijj.ProjectGaia.StaticData.Skills;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution
{
    public class SkillFlowPlayer
    {
        #region Main
        public event Action Completed;
		public event Action<ImpactOrder> Impact;

        public void Play(SkillConfig currentSkill)
		{
            CurrentSkill = currentSkill;
            ImpactTargets();
            Completed?.Invoke();
        }

        private SkillConfig CurrentSkill { get; set; }
        #endregion



        #region Impacting
        private void ImpactTargets()
        {
            for(int j = 0; j < Targets.Count; j++)
            {
                TargetIndex = j;
                OrderImpact();
            }
        }

        private void OrderImpact()
        {
            ImpactOrder order = new ImpactOrder();
            order.targetIndex = TargetIndex;
            order.gameResult = 0f;
            order.overrideEffectKind = EffectKind.Normal;
            Impact?.Invoke(order);
        }

        private int TargetIndex { get; set; }
        private List<Fighter> Targets => BattleFlow.BattleData.CurrentScheme.targets;
        #endregion
    }
}