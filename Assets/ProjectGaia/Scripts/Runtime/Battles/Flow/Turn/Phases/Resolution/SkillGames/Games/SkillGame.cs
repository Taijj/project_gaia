﻿using System;
using Taijj.ProjectGaia.StaticData;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames.Games
{
	public class SkillGame : MonoBehaviour
	{
		#region Main
		public event Action Complete;

		public virtual void Play()
        {
			PlayHasCompleted = false;
            BattleStage.Scenery.GameResultFeedback.Completed += CompleteFeedback;
        }

		protected void CompletePlay()
		{
			PlayHasCompleted = true;
			TryComplete();
		}

        protected void Feedback(float result, Vector3 position)
        {
            FeedbackHasCompleted = false;
            BattleStage.Scenery.GameResultFeedback.ShowFeedback(position, result);
        }

        protected virtual void CompleteFeedback()
        {
            FeedbackHasCompleted = true;
            TryComplete();
        }

		private void TryComplete()
		{
			if(!FeedbackHasCompleted || !PlayHasCompleted)
			{
				return;
			}

			BattleStage.Scenery.GameResultFeedback.Completed -= CompleteFeedback;
			Complete?.Invoke();
		}
		#endregion



		#region Impacting
		public event Action<ImpactOrder> Impact;

		protected void OrderDefaultImpact()
		{
			ImpactOrder order = new ImpactOrder();
			order.targetIndex = 0;
			order.gameResult = Result;
            OrderImpact(order);
		}

		protected void OrderImpact(ImpactOrder order)
		{
			Impact?.Invoke(order);
		}
		#endregion



		#region Properties
		protected float Result { get; set; }
		private bool FeedbackHasCompleted { get; set; }
		private bool PlayHasCompleted { get; set; }

		protected float PerfectResult => Database.SkillsSection.GamesSection.PerfectTreshold;
		protected float GoodResult => Database.SkillsSection.GamesSection.GoodTreshold;
		protected float BadResult => Database.SkillsSection.GamesSection.BadTreshold;
		protected float FailedResult => 0f;
		#endregion
	}
}