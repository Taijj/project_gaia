﻿using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillExecution.SubExecution;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillExecution
{
    public class DamageSkillExecutor : SkillExecutor
    {
        public override void Execute(Input input)
        {
            base.Execute(input);
            if(SubExecutor == null)
            {
                SubExecutor = new DamageExecutor();
            }
            SubExecutor.Execute(In.skill as IDamaging, In);
        }

        private DamageExecutor SubExecutor { get; set; }
    }
}