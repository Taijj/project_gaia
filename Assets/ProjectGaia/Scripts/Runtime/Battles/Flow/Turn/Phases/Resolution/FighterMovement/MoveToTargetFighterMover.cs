﻿using UnityEngine;
using Taijj.ProjectGaia.StaticData;
using Taijj.ProjectGaia.Battle.World.Fighters;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.FighterMovement
{
	public class MoveToTargetFighterMover : FighterMover
	{
		public MoveToTargetFighterMover() : base(Database.SkillsSection.MovementSection.GetConfig(SkillMovementKind.MoveToTarget))
		{}



		protected override void SetInPositions()
		{
			From = Mover.transform.position;
			To = Target.transform.position + Vector3.right*GetOffset();
		}

		protected override void SetOutPositions()
		{
			To = From;
			From = Mover.transform.position;
		}



		private float GetOffset()
		{
			float offset = Mathf.Abs(Config.VerticalOffset);
			return Mover.IsHero ? -offset : offset;
		}

		private Fighter Target => BattleData.CurrentScheme.GetSoleTarget();
	}
}