﻿using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillCalculation.SubCalculation;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillCalculation
{
    public class HealSkillCalculator : SkillCalculator
    {
        public override int Calculate()
        {
            if(SubCalculator == null)
            {
                SubCalculator = new HealingCalculator();
            }
            return SubCalculator.Calculate(In.skill as IHealing, In.target, In.multiplier);
        }

        private HealingCalculator SubCalculator { get; set; }
    }
}