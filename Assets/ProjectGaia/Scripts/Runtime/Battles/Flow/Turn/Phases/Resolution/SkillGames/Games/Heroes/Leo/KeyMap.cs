﻿using Taijj.ProjectGaia.Utils.UI;
using UnityEngine;
using UnityEngine.InputSystem;
using System;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames.Games.Heroes.Leo.Analyze
{
    [Serializable]
    public class KeyMap
    {
        [SerializeField] private Key _key;
        [SerializeField] private Sprite _sprite;
        public Key Key => _key;
        public Sprite sprite => _sprite;
    }
}
