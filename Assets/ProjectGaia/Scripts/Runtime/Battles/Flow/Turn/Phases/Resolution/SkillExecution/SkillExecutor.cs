﻿
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.Skills;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillExecution
{
    public abstract class SkillExecutor
    {
        public class Input
        {
            public SkillConfig skill;
            public Fighter target;
            public int strength;
        }

        public virtual void Execute(Input input)
        {
            In = input;
        }

        protected Input In { get; private set; }
    }
}