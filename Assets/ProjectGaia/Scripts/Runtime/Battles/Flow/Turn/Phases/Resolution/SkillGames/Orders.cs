﻿using Taijj.ProjectGaia.Battle.World.SkillEffects;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames
{
	public class ImpactOrder
	{
		public int targetIndex;
		public float gameResult;
		public EffectKind overrideEffectKind;
	}
}