﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames.Games
{
	/// <summary>
	/// Marker to be potentially be used in Unity's Timeline feature.
	/// Kept around as a documentation how to make markers like that.
	/// </summary>
	public class EventMarker : Marker, INotification
	{
		public PropertyName id { get { return new PropertyName(); } }
		public bool retroactive = true;

		public int impactIndex = -1;
		public bool showImpactNumbers = false;

		//TODO
		//fighterAnimationTrigger
		//FullscreenVfXToShow
	}
}