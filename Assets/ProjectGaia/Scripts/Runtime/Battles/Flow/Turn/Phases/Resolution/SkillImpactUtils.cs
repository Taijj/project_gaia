using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.Battle.World.SkillEffects;
using Taijj.ProjectGaia.StaticData;
using Taijj.ProjectGaia.StaticData.Skills;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;
using Taijj.ProjectGaia.StaticData.Skills.Types;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution
{
    public class SkillImpactUtils
    {
        #region Init
        public void Set(Fighter target, EffectKind overrideEffectKind)
        {
            CurrentTarget = target;
            OverrideEffectKind = overrideEffectKind;
        }

        public float TryInvert(float originalGameMultiplier)
		{
            GameMultiplier = EnemyIsAimingAtHero ? 1f - originalGameMultiplier : originalGameMultiplier;
			return GameMultiplier;
		}
        #endregion


        #region Visualization input builder
        public void TryAddNumberInfo(SkillVisualizationInput input)
        {
            if(CurrentSkill is INumbersDisplaying)
            {
                input.showNumbers = true;
                input.numbersStat = (CurrentSkill as INumbersDisplaying).TargetStat;
            }
        }

        public void AddHitPrefab(SkillVisualizationInput input)
        {
            if(CurrentSkill is AnimationSkillConfig)
            {
               input.hitPrefab = GetAnimationHitPrefab();
            }
            input.hitPrefab = CurrentSkill.HitPrefab;
        }

        private GameObject GetAnimationHitPrefab()
        {
            AnimationSkillConfig animation = CurrentSkill as AnimationSkillConfig;
            return SkillIsAimedAtAlly ? animation.AllyHitPrefab : animation.HitPrefab;
        }

        public void AddEffectKind(SkillVisualizationInput input)
        {
            bool isHeal = CurrentSkill is HealSkillConfig;
            bool isAnimationHeal = CurrentSkill is AnimationSkillConfig && SkillIsAimedAtAlly;
            bool isInvertedDamage = CurrentSkill is DamageSkillConfig && input.value < 0;
            bool isInvertedAnimationDamage = CurrentSkill is AnimationSkillConfig && !SkillIsAimedAtAlly && input.value < 0;
			if(isHeal || isAnimationHeal || isInvertedDamage || isInvertedAnimationDamage)
			{
				input.effectKind = EffectKind.Beneficial;
                return;
			}

            if(OverrideEffectKind != EffectKind.NoEffect)
            {
                input.effectKind = OverrideEffectKind;
                return;
            }

            input.effectKind = GetEffectKindByMultiplier();
        }

        public EffectKind GetEffectKindByMultiplier()
		{
			SkillGamesSection section = Database.SkillsSection.GamesSection;
			if(GameMultiplier >= section.PerfectTreshold)
			{
				return EffectKind.Critical;
			}
			if(GameMultiplier >= section.GoodTreshold)
			{
				return EffectKind.Normal;
			}
			return EffectKind.Weak;
		}
        #endregion



        #region Properties
        private EffectKind OverrideEffectKind { get; set; }
        private float GameMultiplier { get; set; }

        private Fighter CurrentTarget { get; set; }
        private bool EnemyIsAimingAtHero => !CurrentFighter.IsHero && CurrentTarget.IsHero;
		private bool SkillIsAimedAtAlly => (CurrentFighter.IsHero && CurrentTarget.IsHero) || (!CurrentFighter.IsHero && !CurrentTarget.IsHero);
		private Fighter CurrentFighter => BattleFlow.BattleData.CurrentFighter;
        private SkillConfig CurrentSkill => BattleFlow.BattleData.CurrentScheme.selectedSkill;
        #endregion
    }
}