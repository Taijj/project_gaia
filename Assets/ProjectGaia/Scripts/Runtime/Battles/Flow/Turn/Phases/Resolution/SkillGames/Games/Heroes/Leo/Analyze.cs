﻿using UnityEngine;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames.Games.Heroes.Leo.Analyze
{
	public class Analyze : SkillGame
	{
		[SerializeField] private InputCode _code;
		[SerializeField] private Transform _resultFeedbackAnchor;

        public override void Play()
        {
			base.Play();
            _code.transform.position = BattleFlow.BattleData.CurrentFighter.transform.position;
			_code.OnCompleted = OnInputCodeCompleted;
            _code.StartInput(SkillGameHelper.GetHeroProficiency());
		}

		private void OnInputCodeCompleted(bool solved)
		{
            _code.OnCompleted -= OnInputCodeCompleted;
			Result = solved ? 1f : 0f;
			Feedback(Result, _resultFeedbackAnchor.position);
		}

		protected override void CompleteFeedback()
		{
			OrderDefaultImpact();
			CompletePlay();
			base.CompleteFeedback();
		}
	}
}