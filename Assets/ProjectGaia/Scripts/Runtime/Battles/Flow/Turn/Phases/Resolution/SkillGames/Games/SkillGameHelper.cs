
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.FighterStats;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames.Games
{
	public static class SkillGameHelper
	{
		public static Fighter GetDefaultTargetFromScheme(int targetIndex)
		{
			return BattleFlow.BattleData.CurrentScheme.targets[targetIndex];
		}

		public static float GetHeroProficiency()
		{
            return (float)BattleFlow.BattleData.CurrentFighter.GetStat(Stat.Proficiency) / (float)Stat.Proficiency.MaxValue();
		}

		public static float GetTargetProficiency(int targetIndex)
		{
            Fighter target = BattleFlow.BattleData.CurrentScheme.targets[targetIndex];
            return 1f + (float)target.GetStat(Stat.Proficiency) / (float)Stat.Proficiency.MaxValue();
		}
    }
}