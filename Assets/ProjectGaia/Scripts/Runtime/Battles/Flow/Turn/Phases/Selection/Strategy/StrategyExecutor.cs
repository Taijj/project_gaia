﻿using System;
using Taijj.ProjectGaia.Battle.Flow.Data;
using Taijj.ProjectGaia.StaticData.Strategies;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Selection.Strategies
{
    public abstract class StrategyExecutor
    {
        public virtual void Wake(Action onCompleted)
        {
            OnCompleted = onCompleted;
        }

        public virtual void Initialize()
        {}

        public abstract void Execute(StrategyConfig strategy);

        protected Action OnCompleted;
        protected BattleData BattleData { get { return BattleFlow.BattleData; } }
    }
}