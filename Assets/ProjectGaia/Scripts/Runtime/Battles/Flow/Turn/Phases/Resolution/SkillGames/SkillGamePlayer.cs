﻿using System;
using Taijj.ProjectGaia.StaticData.Skills;
using Taijj.ProjectGaia.Utils.Factories;
using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames.Games;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames
{
	public class SkillGamePlayer
	{
		#region Play
		public void Play(SkillConfig currentSkill)
		{
			CurrentGame = GameObjectFactory.CreateAs<SkillGame>(
				currentSkill.GamePrefab,
				BattleStage.Scenery.SkillGamesContainer,
				currentSkill.name + "Game");

			CurrentGame.Impact += OnGameImpact;
			CurrentGame.Complete += Complete;
			CurrentGame.Play();
		}

		private SkillGame CurrentGame { get; set; }
		#endregion



		#region Events
		public event Action Completed;
		public event Action<ImpactOrder> Impact;

		private void OnGameImpact(ImpactOrder order) { Impact?.Invoke(order); }

		private void Complete()
		{
			CurrentGame.Impact -= OnGameImpact;
			CurrentGame.Complete -= Complete;
			CurrentGame.DestructAndNullify();
			Completed?.Invoke();
		}
		#endregion
	}
}