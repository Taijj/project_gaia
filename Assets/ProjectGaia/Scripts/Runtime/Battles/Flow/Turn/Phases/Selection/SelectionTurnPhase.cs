﻿using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Selection.Strategies.Ai;
using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Selection.Strategies.Player;
using Taijj.ProjectGaia.Battle.Flow.Utils;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;
using Taijj.ProjectGaia.StaticData.Strategies;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Selection
{
    public class SelectionTurnPhase : TurnPhase
    {
        public override void Wake()
        {
            base.Wake();

            PlayerStrategyExecutor = new PlayerStrategyExecutor();
            AiStrategyExecutor = new AiStrategyExecutor();
            PlayerStrategyExecutor.Wake(CallCompleteHook);
            AiStrategyExecutor.Wake(CallCompleteHook);
        }

        public override void Initialize()
        {
            PlayerStrategyExecutor.Initialize();
            AiStrategyExecutor.Initialize();
        }

        protected override void SetKind() { Kind = TurnPhaseKind.Selection; }

        public override void OnEnter()
        {
            StrategyConfig strategy = BattleFlow.BattleData.CurrentFighter.Model.Config.Strategy;
            if(strategy.IsControlledByPlayer)
            {
                PlayerStrategyExecutor.Execute(strategy);
            }
            else
            {
                AiStrategyExecutor.Execute(strategy);
            }
        }

        public override void OnExit()
        {
            DebtCollector.CollectSkillCosts(
                BattleFlow.BattleData.CurrentFighter,
                BattleFlow.BattleData.CurrentScheme.selectedSkill,
                SkillCost.DueMoment.AfterSkillSelection);
        }



        private PlayerStrategyExecutor PlayerStrategyExecutor { get; set; }
        private AiStrategyExecutor AiStrategyExecutor { get; set; }
    }
}