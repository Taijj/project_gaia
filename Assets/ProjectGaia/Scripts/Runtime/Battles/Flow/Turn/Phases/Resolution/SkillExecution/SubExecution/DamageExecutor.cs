using Taijj.ProjectGaia.StaticData.Skills.Appendages;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillExecution.SubExecution
{
    public class DamageExecutor
    {
        public void Execute(IDamaging damageInfo, SkillExecutor.Input input)
        {
            int newStatValue = input.target.GetStat(damageInfo.TargetStat) - input.strength;
            int clampedValue = (int)Mathf.Max(newStatValue, 0);
            input.target.SetStat(damageInfo.TargetStat, clampedValue);
        }
    }
}