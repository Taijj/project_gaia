﻿using System.Collections.Generic;
using System;
using Taijj.ProjectGaia.Utils.Factories;
using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.FighterMovement;
using Taijj.ProjectGaia.StaticData.Skills;
using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution
{
    public class ResolutionTurnPhase : TurnPhase
    {
        #region Init
        public override void Wake()
        {
            base.Wake();
            Impacter = new SkillImpacter();
            CreateMovers();
            CreateSkillGamePlayer();
            CreateSkillFlowPlayer();
        }

        private void CreateMovers()
        {
            MoversByKind = new Dictionary<SkillMovementKind, FighterMover>();
            foreach(SkillMovementKind kind in Enum.GetValues(typeof(SkillMovementKind)))
            {
                FighterMover mover = EnumReliantFactory<FighterMover>.Create(kind);
                mover.OnMovedIn += OnMoverMovedIn;
                mover.OnMovedOut += OnMoverMovedOut;
                MoversByKind.Add(kind, mover);
            }
        }

        private void CreateSkillGamePlayer()
        {
            SkillGamePlayer = new SkillGamePlayer();
            SkillGamePlayer.Impact += Impacter.OnImpact;
            SkillGamePlayer.Completed += OnSkillPlayerCompleted;
        }

        private void CreateSkillFlowPlayer()
        {
            SkillFlowPlayer = new SkillFlowPlayer();
            SkillFlowPlayer.Impact += Impacter.OnImpact;
            SkillFlowPlayer.Completed += OnSkillPlayerCompleted;
        }

        protected override void SetKind()
        {
            Kind = TurnPhaseKind.Resolution;
        }
        #endregion



        #region Flow
        public override void OnEnter()
        {
            BattleStage.Scenery.SkillVisualizer.NumbersCompleted += OnSkillVisualizationCompleted;
            SkillFlowHasCompleted = false;
            SkillVisualizationHasCompleted = false;

            MoversByKind[CurrentSkill.SkillMovement].MoveIn();
        }

        private void OnMoverMovedIn()
        {
            if(CurrentSkill.HasGame)
            {
                SkillGamePlayer.Play(CurrentSkill);
            }
            else
            {
                SkillFlowPlayer.Play(CurrentSkill);
            }
        }

        private void OnSkillPlayerCompleted()
        {
            MoversByKind[CurrentSkill.SkillMovement].MoveOut();
        }

        private void OnMoverMovedOut()
        {
            SkillFlowHasCompleted = true;
            TryCompleting();
        }

        private void OnSkillVisualizationCompleted()
        {
            SkillVisualizationHasCompleted = true;
            TryCompleting();
        }

        private void TryCompleting()
        {
            if(SkillFlowHasCompleted && SkillVisualizationHasCompleted)
            {
                BattleStage.Scenery.SkillVisualizer.NumbersCompleted -= OnSkillVisualizationCompleted;
                CallCompleteHook();
            }
        }

        public override void OnExit(){}

        private bool SkillFlowHasCompleted { get; set; }
        private bool SkillVisualizationHasCompleted { get; set; }
        #endregion



        #region Properties
        private SkillConfig CurrentSkill => BattleFlow.BattleData.CurrentScheme.selectedSkill;

        private Dictionary<SkillMovementKind, FighterMover> MoversByKind;
        private SkillGamePlayer SkillGamePlayer { get; set; }
        private SkillFlowPlayer SkillFlowPlayer { get; set; }
        private SkillImpacter Impacter { get; set; }
        #endregion
    }
}