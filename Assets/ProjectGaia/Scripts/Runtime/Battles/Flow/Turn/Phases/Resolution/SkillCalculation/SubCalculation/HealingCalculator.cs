using Taijj.ProjectGaia.Battle.Flow.Utils;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillCalculation.SubCalculation
{
    public class HealingCalculator : HealthCalculator<IHealing>
    {
        protected override int CalculateVanillaStrength()
        {
            float strength = (float)BattleFlow.BattleData.CurrentFighter.GetStat(Info.StrengthStat);
            return Mathf.Max((int)strength, 1);
        }

        protected override float CalculateMight(int vanillaStrength)
        {
            return (float)(vanillaStrength+Info.Power) * Info.Might;
        }

        protected override float GetStrengthAfterResistances(float strength)
        {
            float modifier = ResistancesHelper.CalculateHealModifier(Info.Forms, Target);
            return modifier * strength;
        }
    }
}