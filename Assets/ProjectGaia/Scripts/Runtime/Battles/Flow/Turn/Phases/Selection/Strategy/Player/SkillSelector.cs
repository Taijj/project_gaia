﻿using Taijj.ProjectGaia.StaticData.Skills;
using System;
using Taijj.ProjectGaia.Battle.Flow.Data;
using Taijj.ProjectGaia.Battle.UserInterface.Selections.Skills;
using Taijj.ProjectGaia.Battle.Flow.Turn.Order;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Selection.Strategies.Player
{
	public class SkillSelector
	{
        #region Main
		public SkillSelector(Action onSelectionCompleted)
		{
            OnSelectionCompleted = onSelectionCompleted;

			SkillSelection = BattleStage.UserInterface.SkillSelection;
			SkillSelection.OnSkillSelected += OnSkillSelected;
            SkillSelection.OnSkillChosen += OnSkillChosen;
            SkillSelection.OnCanceled += OnSelectionCanceled;
		}

		public void StartSkillSelection()
        {
            BattleData.CurrentScheme = new Scheme();
            SkillSelection.ShowFor(OrderManager.CurrentFighter, GetInitialSelection());
        }

        private SkillConfig GetInitialSelection()
        {
            if(BattleData.Cache.HasSkillCached())
            {
                return BattleData.Cache.GetCachedSkill();
            }
            else
            {
                return BattleData.CurrentFighter.Model.Skills[0];
            }
        }
        #endregion



        #region Event Listeners
        private void OnSkillSelected(SkillConfig selectedSkill)
        {
            bool isDefendSkill = selectedSkill.Kind == SkillKind.Defend;
            if(isDefendSkill)
            {
                OrderManager.Oracle.PredictDefending();
            }
            else
            {
                OrderManager.Oracle.PredictVanilla();
            }
        }

        private void OnSkillChosen(SkillConfig selectedSkill)
        {
            SkillSelection.Hide();
            BattleData.CurrentScheme.selectedSkill = selectedSkill;
            BattleData.Cache.RememberSkill(selectedSkill);

            OnSelectionCompleted();
        }

        private void OnSelectionCanceled()
        {
            //TODO: Move through Button List "levels"
            SkillSelection.Hide();
            StartSkillSelection();
        }
        #endregion



        #region Properties
		private SkillSelection SkillSelection { get; set; }
        private BattleData BattleData { get { return BattleFlow.BattleData; } }
        private Action OnSelectionCompleted { get; set; }

        private OrderManager OrderManager => BattleFlow.TurnExecutor.OrderManager;
        #endregion
	}
}