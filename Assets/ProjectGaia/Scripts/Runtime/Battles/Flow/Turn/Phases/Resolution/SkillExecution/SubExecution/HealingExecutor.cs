using Taijj.ProjectGaia.StaticData.Skills.Appendages;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillExecution.SubExecution
{
    public class HealingExecutor
    {
        public void Execute(IHealing healingInfo, SkillExecutor.Input input)
        {
            int newStatValue = input.target.GetStat(healingInfo.TargetStat) + input.strength;
            int clampedValue = (int)Mathf.Min(newStatValue, input.target.DefaultStats.Get(healingInfo.TargetStat));
            input.target.SetStat(healingInfo.TargetStat, clampedValue);
        }
    }
}