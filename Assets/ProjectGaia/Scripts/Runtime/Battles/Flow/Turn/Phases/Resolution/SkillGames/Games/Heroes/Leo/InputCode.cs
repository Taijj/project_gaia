﻿using System;
using System.Collections.Generic;
using Taijj.ProjectGaia.Utils.Time;
using Taijj.ProjectGaia.Utils.UI;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames.Games.Heroes.Leo.Analyze
{
	public class InputCode : UiElement
	{
        #region Init
        [SerializeField] private List<KeyMap> _maps;
        [SerializeField] private List<SpriteRenderer> _slots;
        [SerializeField] private Transform _cursor;
        [Space]
        [SerializeField] private Progressbar _timerBar;
        [SerializeField] private float _seconds;



        public void StartInput(float proficiency)
        {
            Seconds = _seconds * (1f+proficiency);
            Initialize();
            Begin();
        }

        private void Initialize()
        {
            FillSlots();

            CurrentKeyIndex = 0;
            UpdateCursor();
        }

        private void FillSlots()
        {
            Keys = new List<Key>();
            for(int i = 0; i < _slots.Count; i++)
            {
                KeyMap map = _maps.GetRandomElement();
                Keys.Add(map.Key);
                _slots[i].sprite = map.sprite;
            }
        }
        #endregion



        #region Flow
        private void Begin()
        {
            StartTimer();
            EnableInput();
        }

        private void UpdateCursor()
        {
            _cursor.position = _slots[CurrentKeyIndex].transform.position;
        }

        private void Complete(bool solved)
        {
            StopTimer();
            DisableInput();
            OnCompleted(solved);
        }
        #endregion



        #region Timer
        private void StartTimer()
        {
            Timer = new Timer(Seconds);
            Timer.OnTick += UpdateTimeBar;
            Timer.OnCompleted += OnTimerCompleted;
            Timer.Start();
        }

        private void UpdateTimeBar()
        {
            _timerBar.Progress = 1f-Timer.ElapsedNormalized;
        }

        private void OnTimerCompleted()
        {
            Complete(false);
        }

        private void StopTimer()
        {
            Timer.Stop();
            Timer.OnTick -= UpdateTimeBar;
            Timer.OnCompleted -= OnTimerCompleted;
        }
        #endregion



        #region Input
        private void EnableInput()
        {
            Taijj.ProjectGaia.Input.PlayerInput.Instance.SkillGames.AnyAction.OnActionStarted += OnInput;
        }

        private void OnInput(Key key)
        {
            bool isWrongKey = key != Keys[CurrentKeyIndex];
            if(isWrongKey)
            {
                Complete(false);
                return;
            }

            CurrentKeyIndex++;
            if(CurrentKeyIndex < _slots.Count)
            {
                UpdateCursor();
            }
            else
            {
                Complete(true);
            }
        }

        private void DisableInput()
        {
            Taijj.ProjectGaia.Input.PlayerInput.Instance.SkillGames.AnyAction.OnActionStarted -= OnInput;
        }
        #endregion



        #region Properties
        private Timer Timer { get; set; }
        private List<Key> Keys { get; set; }
        private int CurrentKeyIndex { get; set; }

        private float Seconds { get; set; }
        public Action<bool> OnCompleted { get; set; }
        #endregion
	}
}