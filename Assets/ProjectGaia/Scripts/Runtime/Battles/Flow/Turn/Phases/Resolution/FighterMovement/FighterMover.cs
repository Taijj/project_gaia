﻿using UnityEngine;
using System;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.Battle.Flow.Data;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.FighterMovement
{
	public abstract class FighterMover
	{
		#region Common
		public event Action OnMovedIn;
		public event Action OnMovedOut;

		public FighterMover(SkillMovementConfig config)
		{
			Config = config;
		}

		private void Move(float seconds, AnimationCurve easing, Action onCompleted)
		{
			if(seconds <= 0f)
			{
				onCompleted();
				return;
			}

			Mover.transform.position = From;
			LeanTween.move(Mover.gameObject, To, seconds)
					.setIgnoreTimeScale(true)
					.setEase(easing)
					.setOnComplete(onCompleted);
		}
		#endregion



		#region In Movement
		public void MoveIn()
		{
			SetInPositions();
			Move(Config.InSeconds, Config.InCurve, OnMoveInCompleted);
		}

		protected abstract void SetInPositions();

		protected void OnMoveInCompleted()
		{
			Mover.transform.position = To;
			OnMovedIn?.Invoke();
		}
		#endregion



		#region Out Movement
		public void MoveOut()
		{
			SetOutPositions();
			Move(Config.OutSeconds, Config.OutCurve, OnMoveOutCompleted);
		}

		protected abstract void SetOutPositions();

		protected void OnMoveOutCompleted()
		{
			Mover.transform.position = To;
			OnMovedOut?.Invoke();
		}
		#endregion



		#region Properties
		protected SkillMovementConfig Config { get; private set;}
		protected Vector3 From { get; set; }
		protected Vector3 To { get; set; }

		protected Fighter Mover =>  BattleData.CurrentFighter;
		protected BattleData BattleData => BattleFlow.BattleData;
		#endregion
	}
}