﻿using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillExecution.SubExecution;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.Skills.Types;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillExecution
{
    public class AnimateSkillExecutor : SkillExecutor
    {
        #region Main
        public override void Execute(Input input)
        {
            base.Execute(input);
            if(TargetIsAlly)
            {
                ExecuteHealing();
            }
            else
            {
                ExecuteDamage();
            }
        }

        private void ExecuteHealing()
        {
            if(HealingExecutor == null)
            {
                HealingExecutor = new HealingExecutor();
            }
            HealingExecutor.Execute(Skill.HealingSettings, In);
        }

        private void ExecuteDamage()
        {
            if(DamageExecutor == null)
            {
                DamageExecutor = new DamageExecutor();
            }
            DamageExecutor.Execute(Skill.DamageSettings, In);
        }
        #endregion



        #region Properties
        private DamageExecutor DamageExecutor { get; set; }
        private HealingExecutor HealingExecutor { get; set; }

        private bool TargetIsAlly => (CurrentFighter.IsHero && In.target.IsHero) || (!CurrentFighter.IsHero && !In.target.IsHero);
		private Fighter CurrentFighter => BattleFlow.BattleData.CurrentFighter;
        private AnimationSkillConfig Skill => In.skill as AnimationSkillConfig;
        #endregion
    }
}