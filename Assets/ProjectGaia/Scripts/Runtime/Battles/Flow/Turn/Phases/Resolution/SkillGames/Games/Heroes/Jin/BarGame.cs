﻿using System;
using UnityEngine;
using Taijj.ProjectGaia.Input;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames.Games.Heroes.Jin.Attack
{
	public class BarGame : MonoBehaviour
	{
		#region Injections
		[SerializeField] private GameObject _parts;
		[SerializeField] private GameObject _cursor;
		[SerializeField] private Anchors _anchors;
		[Space]
		[SerializeField] private AnimationCurve _curve;
		[SerializeField] private float _seconds;
		#endregion



		#region init
		public void Wake()
		{
			_anchors.Wake();
			_parts.SetActive(false);
		}
		#endregion



		#region Flow
		public void Begin(float proficiency)
		{
			_parts.SetActive(true);
			TweenSeconds = _seconds * (1f+proficiency);
			OnShown();
		}

		private void OnShown()
		{
			_cursor.transform.localPosition = Vector3.right*_anchors.Min;
			PlayerInput.Instance.Menu.Decide.OnConfirmed += CompleteMoving;

			LeanTween.moveLocal(_cursor, Vector3.right*_anchors.Max, TweenSeconds)
				.setEase(_curve)
				.setIgnoreTimeScale(true)
				.setOnComplete(CompleteMoving);
		}

		private void CompleteMoving()
		{
			PlayerInput.Instance.Menu.Decide.OnConfirmed -= CompleteMoving;
			LeanTween.cancel(_cursor);

			Result = _anchors.GetResult(_cursor.transform.localPosition.x);
			OnCompleted(Result);
		}
		#endregion



		#region Properties
		public Action<float> OnCompleted { get; set; }
		private float Result { get; set; }
		private float TweenSeconds { get; set; }
		#endregion
	}
}