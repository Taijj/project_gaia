﻿using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.Skills;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillCalculation
{
    public abstract class SkillCalculator
    {
        public class Input
        {
            public SkillConfig skill;
            public Fighter target;
            public float multiplier;
        }

        public void Feed(Input input)
        {
            In = input;
        }

        public abstract int Calculate();

        protected Input In { get; private set; }
    }
}