﻿using System;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases
{
    public abstract class TurnPhase
    {
        #region Hooks
        public static event Action<TurnPhaseKind> EnterHook;
        public static event Action<TurnPhaseKind> CompleteHook;
        public static event Action<TurnPhaseKind> ExitHook;
        #endregion



        #region Init
        public virtual void Wake()
        {
            SetKind();
        }
        protected abstract void SetKind();

        public virtual void Initialize()
        {}
        #endregion


        #region Enter/Exit
        public void Enter()
        {
            CallEnterHook();
            OnEnter();
        }
        public abstract void OnEnter();

        public void Exit()
        {
            CallExitHook();
            OnExit();
        }
        public abstract void OnExit();
        #endregion



        #region Events
        private void CallHook(Action<TurnPhaseKind> hook)
        {
            if(hook != null)
            {
                hook(Kind);
            }
        }

        protected void CallEnterHook() { CallHook(EnterHook); }
        protected void CallCompleteHook() { CallHook(CompleteHook); }
        protected void CallExitHook() { CallHook(ExitHook); }
        #endregion



        #region Properties
        public TurnPhaseKind Kind { get; protected set; }
        #endregion
    }



    #region Enum
    public enum TurnPhaseKind
    {
        Start = 0,
        Selection = 1,
        Resolution = 2,
        End = 3
    }
    #endregion
}