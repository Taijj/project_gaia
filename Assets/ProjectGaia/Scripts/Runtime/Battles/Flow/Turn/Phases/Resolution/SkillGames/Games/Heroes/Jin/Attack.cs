﻿
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames.Games.Heroes.Jin.Attack
{
    public class Attack : SkillGame
    {
		[SerializeField] private BarGame _barGame;
		[SerializeField] private Transform _resultFeedbackAnchor;

        public override void Play()
        {
			base.Play();
			_barGame.Wake();
			_barGame.transform.position = BattleFlow.BattleData.CurrentFighter.transform.position;
			_barGame.OnCompleted = OnBarGameCompleted;
			_barGame.Begin(SkillGameHelper.GetHeroProficiency());
		}

		private void OnBarGameCompleted(float result)
		{
			Result = result;
			Feedback(Result, _resultFeedbackAnchor.position);
		}

		protected override void CompleteFeedback()
		{
			OrderDefaultImpact();
			CompletePlay();
			base.CompleteFeedback();
		}
    }
}