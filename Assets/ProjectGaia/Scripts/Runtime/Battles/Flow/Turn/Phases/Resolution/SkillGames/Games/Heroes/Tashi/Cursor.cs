﻿using System.Collections;
using System.Collections.Generic;
using Taijj.ProjectGaia.Input;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames.Games.Heroes.Sam.Scold
{
    public class Cursor : MonoBehaviour
    {
        #region Declarations
        private const float MOVEMENT_DAMPING = 100f;
        [SerializeField] private float _speed;
        [Space]
        [SerializeField] private Color _insideColor = Color.green;
        [SerializeField] private Color _outsideColor = Color.red;
        #endregion



        #region Main
        public void EnabledMovement()
        {
            Renderer = GetComponent<SpriteRenderer>();
            PlayerInput.Instance.SkillGames.MoveAction.Register(Move);
        }

        public void DisableMovement()
        {
            PlayerInput.Instance.SkillGames.MoveAction.Deregister(Move);
        }

        private void Move(Vector2 movement)
        {
            transform.Translate(movement/MOVEMENT_DAMPING*_speed);
        }

        public bool IsInside
        {
            set { Renderer.color = value ? _insideColor : _outsideColor; }
        }

        private SpriteRenderer Renderer { get; set; }
        #endregion
    }
}