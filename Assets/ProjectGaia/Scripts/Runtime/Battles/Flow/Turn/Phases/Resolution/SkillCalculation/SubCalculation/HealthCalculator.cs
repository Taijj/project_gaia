using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillCalculation.SubCalculation
{
    public abstract class HealthCalculator<T>
    {
        #region Main
        public int Calculate(T info, Fighter target, float multiplier)
        {
            Info = info;
            Target = target;
            Multiplier = multiplier;

            int vanillaStrength = CalculateVanillaStrength();
            float might = CalculateMight(vanillaStrength);
            float gameStrength = GetStrengthAfterSkillGame(might);
            float resistanceStrength = GetStrengthAfterResistances(gameStrength);
            float finalStrength = DivideByTargets(resistanceStrength);
            return (int)Mathf.Round(finalStrength);
        }

        protected T Info { get; set; }
        protected Fighter Target { get; set; }
        protected float Multiplier { get; set; }
        #endregion



        #region Calculations
        protected abstract int CalculateVanillaStrength();
        protected abstract float CalculateMight(int vanillaStrength);

        private float GetStrengthAfterSkillGame(float might)
        {
            float potentialBonus = Database.SkillsSection.GamesSection.MaxSkillStrengthMultiplier * might;
            float actualBonus = potentialBonus * Multiplier;
            return might + actualBonus;
        }

        protected abstract float GetStrengthAfterResistances(float gameStrength);

        private float DivideByTargets(float resistanceStrength)
        {
            float divisor = 1f;
            int targetCount = BattleFlow.BattleData.CurrentScheme.targets.Count;
            if(targetCount != 1)
            {
                float damper = 0.8f; // With multiple targets the skill should get weaker, but not exactly according to target count.
                divisor = targetCount * damper;
            }
            return resistanceStrength/divisor;
        }
        #endregion
    }
}