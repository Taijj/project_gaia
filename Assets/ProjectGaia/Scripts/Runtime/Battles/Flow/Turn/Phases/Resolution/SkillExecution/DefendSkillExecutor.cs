﻿namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillExecution
{
    public class DefendSkillExecutor : SkillExecutor
    {
        public override void Execute(Input input)
        {
            base.Execute(input);
            BattleFlow.TurnExecutor.OrderManager.SetCurrentFighterAsDefender();
        }
    }
}