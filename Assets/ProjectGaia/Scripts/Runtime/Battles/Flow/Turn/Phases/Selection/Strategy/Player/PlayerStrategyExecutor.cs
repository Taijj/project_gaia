﻿using Taijj.ProjectGaia.StaticData.Strategies;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Selection.Strategies.Player
{
    public class PlayerStrategyExecutor : StrategyExecutor
    {
        public override void Initialize()
        {
            SkillSelector = new SkillSelector(StarTargetsSelection);
            TargetsSelector = new TargetsSelector(OnCompleted, OnTargetsSelectionCanceled);
        }

        public override void Execute(StrategyConfig config)
        {
            SkillSelector.StartSkillSelection();
        }

        private void StarTargetsSelection()
        {
            TargetsSelector.StartSelectingTargets();
        }

        private void OnTargetsSelectionCanceled()
        {
            SkillSelector.StartSkillSelection();
        }

        private SkillSelector SkillSelector { get; set; }
        private TargetsSelector TargetsSelector { get; set; }
    }
}