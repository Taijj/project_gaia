﻿namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.FighterMovement
{
	public enum SkillMovementKind
	{
		NoMovement = 0,
		MoveToTarget = 1,
		StepForward = 2
	}
}