﻿using Taijj.ProjectGaia.StaticData;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.FighterMovement
{
	public class NoMovementFighterMover : FighterMover
	{
		public NoMovementFighterMover() : base(Database.SkillsSection.MovementSection.GetConfig(SkillMovementKind.NoMovement))
		{}

		protected override void SetInPositions()
		{
			From = Mover.transform.position;
			To = Mover.transform.position;
		}

		protected override void SetOutPositions()
		{
			From = Mover.transform.position;
			To = Mover.transform.position;
		}
	}
}