﻿using System.Linq;
using System;
using System.Collections.Generic;
using Taijj.ProjectGaia.Input;
using UnityEngine;

namespace Taijj.ProjectGaia
{
	public class SlotMachine : MonoBehaviour
	{
		#region Declarations
		public class Result
		{
			public int highestMatchingSlots;
			public int totalSlots;
		}

		[SerializeField] private Reel[] _reels;
		#endregion



		#region Spinning
		public void StartSpinning(float proficiency)
		{
			PlayerInput.Instance.Menu.Decide.OnConfirmed += StopNextReel;

			NextReelIndex = 0;
			LockedInSlots = new Dictionary<int, int>();
			_reels.ForEach(r => r.StartSpinning(proficiency));
		}

		private void StopNextReel()
		{
			LockInSlot();
			IncrementIndex();
		}

		private void LockInSlot()
		{
			int lockedInSlot = _reels[NextReelIndex].LockIn();
			if(LockedInSlots.ContainsKey(lockedInSlot))
			{
				LockedInSlots[lockedInSlot]++;
			}
			else
			{
				LockedInSlots.Add(lockedInSlot, 1);
			}
		}

		private void IncrementIndex()
		{
			NextReelIndex++;
			if(NextReelIndex >= _reels.Length)
			{
				Complete();
			}
		}

		private void Complete()
		{
			PlayerInput.Instance.Menu.Decide.OnConfirmed -= StopNextReel;
			OnCompleted(EvaluateResult());
		}

		private Result EvaluateResult()
		{
			Result result = new Result();
			result.highestMatchingSlots = LockedInSlots.Max(entry => entry.Value);
			result.totalSlots = _reels.Length;
			return result;
		}
		#endregion



		#region Properties
		private int NextReelIndex { get; set; }
		private Dictionary<int, int> LockedInSlots { get; set; }
		public Action<Result> OnCompleted { get; set; }
		#endregion
	}
}