﻿using System;
using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.Flow.Data;
using Taijj.ProjectGaia.Battle.Flow.Utils;
using Taijj.ProjectGaia.Battle.UserInterface.Selections.Targets;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.Skills;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Selection.Strategies.Player
{
	public class TargetsSelector
	{
		#region Main
		public TargetsSelector(Action onSelectionCompleted, Action onSelectionCanceled)
		{
			OnSelectionCompleted = onSelectionCompleted;
            OnSelectionCanceled = onSelectionCanceled;
			TargetsSelection = BattleStage.UserInterface.TargetsSelection;
			TargetsSelection.OnTargetsSelected += OnTargetsSelected;
            TargetsSelection.OnSelectionCanceled += CancelSelection;
		}

        public void StartSelectingTargets()
        {
            if(AimHelper.IsAutoAim(CurrentSkill.Aim))
            {
                SelectTargetsAutomatically();
            }
            else
            {
                LetPlayerSelectTargets();
            }
        }
		#endregion



		#region Target Selection By Entity
        private void SelectTargetsAutomatically()
        {
            // TODO: add more kinds here
            switch(CurrentSkill.Aim)
            {
                case AimKind.Self: OnTargetsSelected(BattleData.CurrentFighter.ToSingleList()); break;
            }
        }

        private void LetPlayerSelectTargets()
        {
            TargetsSelection.Show(CurrentSkill, GetFocusTarget());
        }

        private Fighter GetFocusTarget()
        {
            Fighter focusTarget = null;
            if(BattleData.Cache.HasTargetsCached(CurrentSkill))
            {
                focusTarget = BattleData.Cache.GetCachedFocusTarget(CurrentSkill);
            }
            else
            {
                focusTarget = GetDefaultFocusTarget();
            }
            return focusTarget;
        }

        private Fighter GetDefaultFocusTarget()
        {
            bool aimAtOpponent = CurrentSkill.AimAtOpponentByDefault;
            if(BattleData.CurrentFighter.IsHero)
            {
                return aimAtOpponent ? BattleData.Roster.EnemyParty[0] : BattleData.Roster.HeroParty[0];
            }
            else
            {
                return aimAtOpponent ? BattleData.Roster.HeroParty[0] : BattleData.Roster.EnemyParty[0];
            }
        }
		#endregion



		#region Selection Post Processing
        private void OnTargetsSelected(List<Fighter> targets)
        {
            TargetsSelection.Hide();
            TryCachingFocusTarget(targets[0]);
            Scheme.targets = targets;
            OnSelectionCompleted?.Invoke();
        }

        private void TryCachingFocusTarget(Fighter focusTarget)
        {
            if(BattleData.CurrentFighter.IsHero)
            {
                BattleData.Cache.RememberFocusTarget(CurrentSkill, focusTarget);
            }
        }

        private void CancelSelection()
        {
            TargetsSelection.Hide();
            OnSelectionCanceled?.Invoke();
        }
        #endregion



		#region Properties
		private BattleData BattleData { get { return BattleFlow.BattleData; } }
		private Scheme Scheme { get { return BattleData.CurrentScheme; } }
		private SkillConfig CurrentSkill { get { return Scheme.selectedSkill; } }

		private TargetsSelection TargetsSelection { get; set; }
		private Action OnSelectionCompleted { get; set; }
        private Action OnSelectionCanceled { get; set; }
		#endregion
	}
}