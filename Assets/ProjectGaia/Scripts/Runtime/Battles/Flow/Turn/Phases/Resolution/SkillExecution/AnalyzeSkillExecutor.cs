﻿using System;
using Taijj.ProjectGaia.Battle.World.Fighters;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillExecution
{
    public class AnalyzeSkillExecutor : SkillExecutor
    {
        public static event Action<Fighter> OnFirstTier;
        public static event Action<Fighter> OnSecondTier;



        public override void Execute(Input input)
        {
            base.Execute(input);

            bool isSuccess = input.strength != 0;
            if(isSuccess)
            {
                AnalyzeTarget();
            }
        }

        private void AnalyzeTarget()
        {
            Fighter target = In.target;
            int tier = BattleFlow.BattleData.Analysis.TierUp(target);
            switch(tier)
            {
                case 1: ExecuteFirstTier(target); break;
                case 2: ExecuteSecondTier(target); break;
            }
        }

        private void ExecuteFirstTier(Fighter target)
        {
            if(!target.IsHero)
            {
                OnFirstTier?.Invoke(target);
            }
        }

        private void ExecuteSecondTier(Fighter target)
        {
            if(!target.IsHero)
            {
                OnSecondTier?.Invoke(target);
            }
        }
    }
}