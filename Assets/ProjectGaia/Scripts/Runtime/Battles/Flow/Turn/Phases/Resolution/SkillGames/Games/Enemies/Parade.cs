﻿using Taijj.ProjectGaia.Battle.Flow;
using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames.Games;
using Taijj.ProjectGaia.Input;
using Taijj.ProjectGaia.StaticData;
using Taijj.ProjectGaia.Utils.SceneSingletons;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.Phases.Execution.SkillGames.Games.Enemies
{
    public class Parade : SkillGame
    {
		[SerializeField] private float _reactionSeconds;
		[SerializeField] [Range(0f, 1f)] private float _reactedResult;

        public override void Play()
        {
			base.Play();
			float seconds = _reactionSeconds * SkillGameHelper.GetTargetProficiency(0);

			BattleStage.Scenery.WarningSpawner.ShowWarning(SkillGameHelper.GetDefaultTargetFromScheme(0).transform.position, seconds);
			ReactionCoroutine = CoroutineRunner.Instance.WaitForSeconds(seconds, OnReactionMomentRanOut);
			PlayerInput.Instance.Menu.Decide.OnConfirmed += OnReacted;
		}

		private void OnReactionMomentRanOut()
		{
			StopReactionMoment();
			OnReactionMomentCompleted(false);
		}
		private void OnReacted()
		{
			StopReactionMoment();
			OnReactionMomentCompleted(true);
		}

		private void StopReactionMoment()
		{
			CoroutineRunner.Instance.StopCoroutine(ReactionCoroutine);
			ReactionCoroutine = null;
			PlayerInput.Instance.Menu.Cursor.Left -= OnReacted;
		}

		private void OnReactionMomentCompleted(bool reacted)
		{
			PlayerInput.Instance.Menu.Decide.OnConfirmed -= OnReacted;

			Result = reacted ? GoodResult : BadResult;
			Vector3 feedbackPosition = BattleFlow.BattleData.CurrentFighter.transform.position + new Vector3(-0.2f, -0.7f, 0f);
			Feedback(Result, feedbackPosition);
			OrderDefaultImpact();
			CompletePlay();
		}

		Coroutine ReactionCoroutine { get; set; }
    }
}