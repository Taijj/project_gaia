using Taijj.ProjectGaia.Battle.Flow.Utils;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillCalculation.SubCalculation
{
    public class DamageCalculator : HealthCalculator<IDamaging>
    {
        protected override int CalculateVanillaStrength()
        {
            float targetDefense = (float)Target.GetStat(Info.DefenseStat);
            float userAttack = (float)BattleFlow.BattleData.CurrentFighter.GetStat(Info.AttackStat);
            float damage = userAttack * (100f / (100f+targetDefense));
            return Mathf.Max((int)damage, 1);
        }

        protected override float CalculateMight(int vanillaStrength)
        {
            return (float)(vanillaStrength+Info.Power) * Info.Might;
        }

        protected override float GetStrengthAfterResistances(float strength)
        {
            float modifier = ResistancesHelper.CalculateDamageModifier(Info.Forms, Target);
            return modifier * strength;
        }
    }
}