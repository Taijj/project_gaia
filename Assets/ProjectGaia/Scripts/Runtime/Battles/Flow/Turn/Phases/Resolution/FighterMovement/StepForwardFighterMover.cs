﻿using Taijj.ProjectGaia.StaticData;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.FighterMovement
{
	public class StepForwardFighterMover : FighterMover
	{
		public StepForwardFighterMover() : base(Database.SkillsSection.MovementSection.GetConfig(SkillMovementKind.StepForward))
		{}



		protected override void SetInPositions()
		{
			From = Mover.transform.position;
			To = Mover.transform.position + Vector3.right*GetOffset();
		}

		protected override void SetOutPositions()
		{
			To = From;
			From = Mover.transform.position;
		}



		private float GetOffset()
		{
			float offset = Mathf.Abs(Config.VerticalOffset);
			return Mover.IsHero ? offset : -offset;
		}
	}
}