﻿using Taijj.ProjectGaia.StaticData.Skills.Types;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillCalculation
{
    public class DefendSkillCalculator : SkillCalculator
    {
        public override int Calculate()
        {
            DefendSkillConfig skill = (DefendSkillConfig)In.skill;
            float impactStrength = In.target.GetStat(skill.DefenseStat) * skill.DefenseMultiplier;
            return (int)Mathf.Round(impactStrength);
        }
    }
}