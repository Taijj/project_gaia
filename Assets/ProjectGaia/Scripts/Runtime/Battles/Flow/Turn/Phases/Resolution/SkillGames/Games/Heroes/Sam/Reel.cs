﻿using Taijj.ProjectGaia.Utils.SceneSingletons;
using UnityEngine;

namespace Taijj.ProjectGaia
{
	[ExecuteAlways]
	public class Reel : MonoBehaviour
	{
		#region Injections
		[SerializeField] private SpriteRenderer[] _slots;
		[SerializeField] private SpriteRenderer _background;
		[SerializeField] private Transform _slotsContainer;
		[Space]
		[SerializeField] private float _speed;
		[SerializeField] private float _radius;
		[SerializeField] [Range(0.1f, 0.5f)] private float _tolerance;
		#endregion



		#region Control
		public void StartSpinning(float proficiency)
		{
			CurrentAngle = Random.value * 360f;
			SpinningSpeed = _speed - 0.5f*_speed*proficiency;
			UpdateCaller.Instance.StartCallingOnUpdate(UpdateSpin);
		}

		public int LockIn()
		{
			UpdateCaller.Instance.StopCallingOnUpdate(UpdateSpin);

			float angleSteps = 360f/(float)_slots.Length;
			float lockAngle = (CurrentAngle+angleSteps*(1f-_tolerance));
			int lockedSlot = (int)(lockAngle/angleSteps);

            _slotsContainer.localRotation = Quaternion.AngleAxis( -(float)lockedSlot*angleSteps, Vector3.right );
			return lockedSlot;
		}
		#endregion



		#region Spin
		private void UpdateSpin()
		{
			UpdateAngle();
			UpdateSorting();
		}

		private void UpdateAngle()
		{
			CurrentAngle += SpinningSpeed;
			CurrentAngle %= 360f;
			_slotsContainer.transform.localRotation = Quaternion.AngleAxis(-CurrentAngle, Vector3.right );
		}

		private void UpdateSorting()
		{
			foreach(SpriteRenderer renderer in _slots)
			{
				renderer.sortingOrder = renderer.transform.position.z < _background.transform.position.z
					? _background.sortingOrder + 1
					: _background.sortingOrder - 1;
			}
		}

		private float CurrentAngle { get; set; }
		private float SpinningSpeed { get; set; }
		#endregion



		#if UNITY_EDITOR
		public void Update()
		{
			if(_radius == 0f || _background == null || UnityEditor.EditorApplication.isPlaying)
			{
				return;
			}
			LayoutImages();
			UpdateSorting();
		}

		public void LayoutImages()
		{
			float angleStep = 360f/(float)_slots.Length;
			for(int i = 0; i < _slots.Length; i++)
			{
				if(_slots[i] == null)
				{
					continue;
				}

				float angle = (float)i*angleStep;
				_slots[i].transform.localPosition = GetPositionByAngle(angle);
				_slots[i].transform.localRotation = Quaternion.Euler(Vector3.right * (angle+90f));
			}
		}

		private Vector3 GetPositionByAngle(float angle)
		{
			angle *= Mathf.Deg2Rad;
			float y = _radius * Mathf.Cos(angle);
			float z = _radius * Mathf.Sin(angle);
			return new Vector3(0f, y, z);
		}
		#endif
	}
}