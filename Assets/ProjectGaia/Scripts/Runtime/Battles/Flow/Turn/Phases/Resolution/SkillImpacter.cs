﻿using System;
using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.Flow.Data;
using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillCalculation;
using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillExecution;
using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames;
using Taijj.ProjectGaia.Battle.Flow.Utils;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.Battle.World.SkillEffects;
using Taijj.ProjectGaia.StaticData.Skills;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;
using Taijj.ProjectGaia.Utils.Factories;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution
{
	public class SkillImpacter
	{
		#region Main
		public SkillImpacter()
		{
			CreateCalculators();
			CreateExecutors();
			Utils = new SkillImpactUtils();
		}

		private void CreateCalculators()
        {
            CalculatorsByKind = new Dictionary<SkillKind, SkillCalculator>();
            foreach(SkillKind kind in Enum.GetValues(typeof(SkillKind)))
            {
                CalculatorsByKind.Add(kind, EnumReliantFactory<SkillCalculator>.Create(kind));
            }
        }

		private void CreateExecutors()
        {
            ExecutorsByKind = new Dictionary<SkillKind, SkillExecutor>();
            foreach(SkillKind kind in Enum.GetValues(typeof(SkillKind)))
            {
                ExecutorsByKind.Add(kind, EnumReliantFactory<SkillExecutor>.Create(kind));
            }
        }



		public void OnImpact(ImpactOrder order)
        {
			InitializeImpact(order);

			int impactStrength = Calculate();
			Execute(impactStrength);
			Visualize(impactStrength);
        }
		#endregion



		#region Methods
		private void InitializeImpact(ImpactOrder order)
		{
			Scheme scheme = BattleFlow.BattleData.CurrentScheme;
            CurrentSkill = scheme.selectedSkill;
            CurrentTarget = scheme.targets[order.targetIndex];

			Utils.Set(CurrentTarget, order.overrideEffectKind);
			GameMultiplier = Utils.TryInvert(order.gameResult);
		}

		private int Calculate()
		{
			SkillCalculator.Input calculatorInput = new SkillCalculator.Input();
            calculatorInput.skill = CurrentSkill;
            calculatorInput.target = CurrentTarget;
            calculatorInput.multiplier = GameMultiplier;

			SkillCalculator calculator = CalculatorsByKind[CurrentSkill.Kind];
			calculator.Feed(calculatorInput);
            return calculator.Calculate();
		}

		private void Execute(int strength)
		{
			bool targetWasAlive = CurrentTarget.IsAlive;

			SkillExecutor.Input executorInput = new SkillExecutor.Input();
			executorInput.skill = CurrentSkill;
			executorInput.target = CurrentTarget;
			executorInput.strength = strength;
			ExecutorsByKind[CurrentSkill.Kind].Execute(executorInput);

			HealthObserver.UpdateAliveStatus(CurrentTarget, targetWasAlive);
		}

		private void Visualize(int impactStrength)
		{
			SkillVisualizationInput input = new SkillVisualizationInput();
			input.position = CurrentTarget.transform.position;
			input.value = impactStrength;

			Utils.TryAddNumberInfo(input);
			Utils.AddHitPrefab(input);
			Utils.AddEffectKind(input);

			BattleStage.Scenery.SkillVisualizer.VisualizeSkill(input);
		}
		#endregion



		#region Properties
		private Dictionary<SkillKind, SkillCalculator> CalculatorsByKind;
		private Dictionary<SkillKind, SkillExecutor> ExecutorsByKind;
		private SkillConfig CurrentSkill { get; set; }
		private Fighter CurrentTarget { get; set; }
		private float GameMultiplier { get; set; }

		private SkillImpactUtils Utils { get; set; }
		#endregion
	}
}