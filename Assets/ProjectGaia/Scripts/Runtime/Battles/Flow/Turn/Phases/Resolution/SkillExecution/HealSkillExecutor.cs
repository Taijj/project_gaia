﻿using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillExecution.SubExecution;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillExecution
{
    public class HealSkillExecutor : SkillExecutor
    {
        public override void Execute(Input input)
        {
            base.Execute(input);
            if(SubExecutor == null)
            {
                SubExecutor = new HealingExecutor();
            }
            SubExecutor.Execute(In.skill as IHealing, In);
        }

        private HealingExecutor SubExecutor { get; set; }
    }
}