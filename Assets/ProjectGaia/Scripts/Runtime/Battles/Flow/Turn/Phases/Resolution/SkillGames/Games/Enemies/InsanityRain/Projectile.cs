﻿using System;
using Taijj.ProjectGaia.Battle.World.Fighters;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.Phases.Execution.SkillGames.Games.Enemies.InsanityRain
{
    public class Projectile : MonoBehaviour
    {
        #region Main
        public event Action<Projectile> Completed;

        [SerializeField] private float _startDistance;
        [SerializeField] private float _flightSeconds;

        public void ShootAt(Fighter target)
        {
            Target = target;
            StartFlying();
        }

        private void StartFlying()
        {
            transform.position = Target.transform.position + Vector3.right*_startDistance;
            LeanTween.move(gameObject, Target.transform.position, _flightSeconds)
                .setIgnoreTimeScale(true)
                .setOnComplete(OnCompleted);
        }

        private void OnCompleted()
        {
            Completed?.Invoke(this);
        }

        public Fighter Target { get; private set; }
        #endregion



        #region Hot/Defuse
        public void TrySettingHot(float hotDistance)
        {
            if(IsHot)
            {
                return;
            }

            float distanceToTarget = Mathf.Abs(Target.transform.position.x-transform.position.x);
            if(distanceToTarget < hotDistance && Target.IsAlive)
            {
                float warningSeconds = distanceToTarget/_startDistance * _flightSeconds;
                BattleStage.Scenery.WarningSpawner.ShowWarning(Target.transform.position, warningSeconds);
                IsHot = true;
            }
        }

        public bool IsHot { get; private set; }
        public bool IsDefused { get; set; }
        #endregion
    }
}