﻿using System.Collections.Generic;
using System.Linq;
using Taijj.ProjectGaia.Battle.Flow;
using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames;
using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames.Games;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.Input;
using Taijj.ProjectGaia.Utils.Factories;
using Taijj.ProjectGaia.Utils.SceneSingletons;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.Phases.Execution.SkillGames.Games.Enemies.InsanityRain
{
    public class InsanityRainGame : SkillGame
    {
		#region Init
		[SerializeField] private GameObject _projectilePrefab;
		[SerializeField] private float _tolerance;
		[SerializeField] [Range(0f, 1f)] private float _reactedResult;
		[SerializeField] private AnimationClip _clip;


        public override void Play()
        {
			base.Play();
			Projectiles = new List<Projectile>();
			PotentialTargets = BattleFlow.BattleData.CurrentScheme.targets;
			CoroutineRunner.Instance.WaitForAnimationCompletion(_clip, SetAnimationCompleted);
			Enable();
		}

		private void SetAnimationCompleted()
		{
			AnimationHasCompleted = true;
		}

		private void TryCompleteShooting()
		{
			if(Projectiles.Count != 0 || !AnimationHasCompleted)
			{
				return;
			}

			Disable();
			CompletePlay();
		}

		private List<Projectile> Projectiles { get; set; }
		private List<Fighter> PotentialTargets { get; set; }
		private bool AnimationHasCompleted { get; set; }
		#endregion




		#region Enable/Disable
		private void Enable()
		{
			UpdateCaller.Instance.StartCallingOnUpdate(ProjectileUpdate);
			PlayerInput.Instance.Menu.Decide.OnConfirmed += OnReact;
		}

		private void Disable()
		{
			UpdateCaller.Instance.StopCallingOnUpdate(ProjectileUpdate);
			PlayerInput.Instance.Menu.Decide.OnConfirmed -= OnReact;
		}
		#endregion




		#region Projectile Handling
		// Called frequently by Animation Events
		public void SpawnProjectile()
		{
			Fighter target = PotentialTargets.GetRandomElement();
			Projectile projectile = GameObjectFactory.CreateAs<Projectile>(_projectilePrefab, transform, $"Projectile{Projectiles.Count}");
			projectile.ShootAt(target);
			projectile.Completed += OnProjectileCompleted;
			Projectiles.Add(projectile);
		}

		private void DestroyProjectile(Projectile projectile)
		{
			Projectiles.Remove(projectile);
			projectile.Completed -= OnProjectileCompleted;
			projectile.DestructAndNullify();
		}



		private void OnProjectileCompleted(Projectile projectile)
		{
			float result = projectile.IsDefused ? GoodResult : BadResult;

			ShowFeedback(result, projectile.transform.position);
			PlaceImpactOrder(result, projectile.Target);
			DestroyProjectile(projectile);
			TryCompleteShooting();
		}

		private void ShowFeedback(float result, Vector3 projectilePosition)
		{
			Vector3 position = projectilePosition + new Vector3(0.75f, -0.75f, 0f);
			Feedback(result, position);
		}

		private void PlaceImpactOrder(float result, Fighter target)
		{
			ImpactOrder order = new ImpactOrder();
			order.targetIndex = BattleFlow.BattleData.CurrentScheme.targets.IndexOf(target);
			order.gameResult = result;
			OrderImpact(order);
		}
		#endregion



		#region Misc
		private void ProjectileUpdate()
		{
			Projectiles.ForEach(p => p.TrySettingHot(_tolerance));
		}

		private void OnReact()
		{
			if(Projectiles.Count == 0)
			{
				return;
			}

			Projectile projectile = Projectiles.FirstOrDefault(p => p.IsHot && !p.IsDefused);
			if(projectile != null)
			{
				projectile.IsDefused = true;
			}
		}
		#endregion
    }
}