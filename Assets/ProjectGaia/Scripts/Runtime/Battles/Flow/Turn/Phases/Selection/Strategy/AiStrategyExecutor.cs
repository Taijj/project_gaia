﻿using Taijj.ProjectGaia.StaticData.Strategies;
using Taijj.ProjectGaia.StaticData.Strategies.Ai;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Selection.Strategies.Ai
{
    public class AiStrategyExecutor : StrategyExecutor
    {
        public override void Execute(StrategyConfig config)
        {
            AiStrategy strategy = (AiStrategy)config;
            strategy.Execute();
            OnCompleted();
        }
    }
}