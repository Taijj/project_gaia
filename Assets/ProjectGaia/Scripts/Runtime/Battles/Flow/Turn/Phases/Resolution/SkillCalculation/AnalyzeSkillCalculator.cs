﻿namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillCalculation
{
    public class AnalyzeSkillCalculator : SkillCalculator
    {
        public override int Calculate()
        {
            return In.multiplier == 0 ? 0 : 1;
        }
    }
}