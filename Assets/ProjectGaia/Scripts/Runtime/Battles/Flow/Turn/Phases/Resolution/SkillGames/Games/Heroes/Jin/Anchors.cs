﻿using System;
using UnityEngine;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames.Games.Heroes.Jin.Attack
{
	[Serializable]
	public class Anchors
	{
		#region Main
		[SerializeField] private Transform _minAnchor;
		[SerializeField] private Transform _maxAnchor;
		[SerializeField] private Transform _apexAnchor;


		public void Wake()
		{
			DetermineMinAndMax();
		}

		public float GetResult(float cursorX)
		{
			CurrentCursorX = cursorX;
			return CalculateResult();
		}
		#endregion



		#region Min/Max Determination
		private void DetermineMinAndMax()
		{
			if(_minAnchor.localPosition.x < _maxAnchor.localPosition.x)
			{
				SetMinAndMaxStandard();
			}
			else
			{
				SetMinAndMaxSwapped();
			}
		}

		private void SetMinAndMaxStandard()
		{
			Min = _minAnchor.localPosition.x;
			Max = _maxAnchor.localPosition.x;
		}

		private void SetMinAndMaxSwapped()
		{
			Min = _maxAnchor.localPosition.x;
			Max = _minAnchor.localPosition.x;
		}
		#endregion



		#region Result Calculation
		private float CalculateResult()
		{
			float apex = _apexAnchor.localPosition.x;
			float cursorToApex = CurrentCursorX - apex;
			float apexToBorder = GetApexToBorderDistance(apex);
			return 1f - cursorToApex/apexToBorder;
		}

		private float GetApexToBorderDistance(float apex)
		{
			return CurrentCursorX >= apex ? Max-apex : Min-apex;
		}
		#endregion



		#region Properties
		private float CurrentCursorX { get; set; }
		public float Max { get; private set; }
		public float Min { get; private set; }
		#endregion
	}
}