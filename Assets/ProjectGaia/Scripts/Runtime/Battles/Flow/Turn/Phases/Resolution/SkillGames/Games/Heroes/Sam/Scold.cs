﻿using UnityEngine;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames.Games.Heroes.Sam.Scold
{
	public class Scold : SkillGame
	{
		[SerializeField] private SlotMachine _slotmachine;
		[SerializeField] private Transform _resultFeedbackAnchor;

        public override void Play()
        {
			base.Play();
			_slotmachine.transform.position = BattleFlow.BattleData.CurrentFighter.transform.position;
			_slotmachine.OnCompleted = OnSlotMachineCompleted;
			_slotmachine.StartSpinning(SkillGameHelper.GetHeroProficiency());
		}

		private void OnSlotMachineCompleted(SlotMachine.Result result)
		{
			Result = (float)result.highestMatchingSlots/(float)result.totalSlots;
			Feedback(Result, _resultFeedbackAnchor.position);
		}

		protected override void CompleteFeedback()
		{
			OrderDefaultImpact();
			CompletePlay();
			base.CompleteFeedback();
		}
	}
}