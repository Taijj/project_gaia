﻿using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillCalculation.SubCalculation;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.Skills.Types;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillCalculation
{
    public class AnimateSkillCalculator : SkillCalculator
    {
        #region Init
        public override int Calculate()
        {
            return TargetIsAlly ? CalculateHeal() : CalculateDamage();
        }

        private int CalculateHeal()
        {
            if(HealingCalculator == null)
            {
                HealingCalculator = new HealingCalculator();
            }
            return HealingCalculator.Calculate(Skill.HealingSettings, In.target, In.multiplier);
        }

        private int CalculateDamage()
        {
            if(DamageCalculator == null)
            {
                DamageCalculator = new DamageCalculator();
            }
            return DamageCalculator.Calculate(Skill.DamageSettings, In.target, In.multiplier);
        }


        private DamageCalculator DamageCalculator { get; set; }
        private HealingCalculator HealingCalculator { get; set; }

        private bool TargetIsAlly => (CurrentFighter.IsHero && In.target.IsHero) || (!CurrentFighter.IsHero && !In.target.IsHero);
        private Fighter CurrentFighter => BattleFlow.BattleData.CurrentFighter;
        private AnimationSkillConfig Skill => In.skill as AnimationSkillConfig;
        #endregion
    }
}