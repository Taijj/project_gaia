﻿using Taijj.ProjectGaia.Utils.SceneSingletons;
using UnityEngine;


namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillGames.Games.Heroes.Sam.Scold
{
    public class Smile : SkillGame
    {
        #region Injections
        [SerializeField] private Transform _containerTransform;
        [SerializeField] private Cursor _cursor;
        [SerializeField] private Spotlight _spotlight;
        [SerializeField] private Transform _background;
        [Space]
        [SerializeField] private float _playSeconds;
        [SerializeField] private GameObject _text;
        [SerializeField] private Transform _resultFeedbackAnchor;
        #endregion



        #region Main
        public override void Play()
        {
            base.Play();
            _containerTransform.position = BattleFlow.BattleData.CurrentFighter.transform.position;
            StartGame(SkillGameHelper.GetHeroProficiency());
            CoroutineRunner.Instance.WaitForSeconds(_playSeconds, Finish);
        }

        private void Finish()
        {
            StopGame();
            EvaluateResult();

            _text.SetActive(false);
            Feedback(Result, _resultFeedbackAnchor.position);
        }

        protected override void CompleteFeedback()
		{
			OrderDefaultImpact();
            CompletePlay();
            base.CompleteFeedback();
        }

        private void EvaluateResult()
        {
            int totalFrames = InsideFrames + OutsideFrames;
            Result = (float)InsideFrames/(float)totalFrames;
        }
        #endregion



        #region Game
        private void StartGame(float proficiency)
        {
            InsideFrames = 0;
            OutsideFrames = 0;

            _cursor.EnabledMovement();
            _spotlight.StartMoving(_background.localScale.x/2f, proficiency);
            UpdateCaller.Instance.StartCallingOnUpdate(RecordInsideState);
        }

        private void StopGame()
        {
            _cursor.DisableMovement();
            _spotlight.StopMoving();
            UpdateCaller.Instance.StopCallingOnUpdate(RecordInsideState);
        }

        private void RecordInsideState()
        {
            float spotlightRadius = _spotlight.transform.localScale.x/2f;
            float spotlightDistance = Vector3.Distance(_cursor.transform.position, _spotlight.transform.position);
            bool isInside = spotlightDistance < spotlightRadius;

            _cursor.IsInside = isInside;
            if(isInside)
            {
                InsideFrames++;
            }
            else
            {
                OutsideFrames++;
            }
        }

        private int InsideFrames { get; set; }
        private int OutsideFrames { get; set; }
        #endregion
    }
}