﻿using Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillCalculation.SubCalculation;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;

namespace Taijj.ProjectGaia.Battle.Flow.Turn.Phases.Resolution.SkillCalculation
{
    public class DamageSkillCalculator : SkillCalculator
    {
        #region Init
        public override int Calculate()
        {
            if(SubCalculator == null)
            {
                SubCalculator = new DamageCalculator();
            }
            return SubCalculator.Calculate(In.skill as IDamaging, In.target, In.multiplier);
        }

        private DamageCalculator SubCalculator { get; set; }
        #endregion
    }
}