﻿using System.Collections.Generic;
using System;
using Taijj.ProjectGaia.Battle.Flow.Data;
using Taijj.ProjectGaia.Battle.Flow.Turn.Order;
using Taijj.ProjectGaia.Battle.Flow.Turn.Phases;
using Taijj.ProjectGaia.Utils.Factories;

namespace Taijj.ProjectGaia.Battle.Flow.Turn
{
    public class TurnExecutor
    {
        #region Events
        public static event Action OnTurnStart;
        public static event Action OnTurnComplete;
        #endregion



        #region Init
        public TurnExecutor()
        {
            CreatePhases();
            TurnPhase.CompleteHook += OnPhaseCompleted;
            OrderManager = new OrderManager();
        }

        private void CreatePhases()
        {
            Phases = new List<TurnPhase>();
            foreach(TurnPhaseKind kind in Enum.GetValues(typeof(TurnPhaseKind)))
            {
                TurnPhase phase = EnumReliantFactory<TurnPhase>.Create(kind, true);
                phase.Wake();
                Phases.Add(phase);
            }
        }

        public void Initialize()
        {
            Phases.ForEach(p => p.Initialize());
        }
        #endregion



        #region Battle Flow
        public void OnBattleStart()
        {
            OrderManager.OnBattleStart();
        }

        public void StartNextTurn()
        {
            CallStartTurnHook();
            CurrentPhaseIndex = 0;
            StartNextPhase();
        }

        private void StartNextPhase()
        {
            Phases[CurrentPhaseIndex].Enter();
        }

        private void OnPhaseCompleted(TurnPhaseKind kind)
        {
            Phases[CurrentPhaseIndex].Exit();
            CurrentPhaseIndex++;

            if(CurrentPhaseIndex < Phases.Count)
            {
                StartNextPhase();
            }
            else
            {
                CallCompletedHook();
            }
        }



        public void OnBattleEnd()
        {
            OrderManager.OnBattleEnd();
        }
        #endregion



        #region Events
        private void CallStartTurnHook() { OnTurnStart?.Invoke(); }
        private void CallCompletedHook() { OnTurnComplete?.Invoke(); }
        #endregion



        #region Properties
        private BattleData BattleData => BattleFlow.BattleData;
        private List<TurnPhase> Phases { get; set; }
        private int CurrentPhaseIndex { get; set; }

        public OrderManager OrderManager { get; private set; }
        #endregion
    }
}