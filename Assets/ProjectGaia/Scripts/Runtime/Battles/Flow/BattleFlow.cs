﻿using System;
using Taijj.ProjectGaia.Battle.Flow.Data;
using Taijj.ProjectGaia.Battle.Flow.Turn;
using Taijj.ProjectGaia.Battle.Flow.Utils;

namespace Taijj.ProjectGaia.Battle.Flow
{
    public class BattleFlow
    {
        #region Init
        public static event Action<bool> OnBattleComplete;

        public BattleFlow()
        {
            BattleData = new BattleData();
            TurnExecutor = new TurnExecutor();
        }

        public void Initialize()
        {
            TurnExecutor.Initialize();
            TurnExecutor.OnTurnComplete += OnTurnCompleted;
        }
        #endregion



        #region Battle Flow
        public void PrepareBattle(BattleSetup setup)
        {
            BattleData.OnBattleStart(setup);
            TurnExecutor.OnBattleStart();
        }

        public void StartBattle()
        {
            TurnExecutor.StartNextTurn();
        }

        private void OnTurnCompleted()
        {
            bool isComplete = HealthObserver.IsOnePartyDead;
            if(!isComplete)
            {
                TurnExecutor.StartNextTurn();
            }
            else
            {
                EndBattle();
            }
        }

        private void EndBattle()
        {
            bool hasWon = BattleData.Roster.AliveHeroes.Count > 0; // this has to be done before BattleData.OnEnd() is called!
            BattleData.OnBattleEnd();
            TurnExecutor.OnBattleEnd();
            OnBattleComplete?.Invoke(hasWon);
        }
        #endregion



        #region Properties
        public static BattleData BattleData { get; private set; }
        public static TurnExecutor TurnExecutor { get; private set; }
        #endregion
    }
}