﻿using System.Collections.Generic;
using Taijj.ProjectGaia.DynamicData;
using Taijj.ProjectGaia.StaticData;

namespace Taijj.ProjectGaia.Battle.Flow
{
	public class BattleSetup
	{
		public BattleSetup(List<FighterModel> heroModels, LineupConfig lineupConfig)
		{
			HeroModels = heroModels;
			Lineup = lineupConfig;
		}

		public List<FighterModel> HeroModels { get; private set; }
		public LineupConfig Lineup { get; private set; }
	}
}