﻿using Taijj.ProjectGaia.Battle.World.Fighters;

namespace Taijj.ProjectGaia.Battle.Flow.Data
{
    public class BattleData
    {
        #region Lifecycle
        public BattleData()
        {
            Roster = new Roster();
            Cache = new Cache(this);
            Analysis = new Analysis();
        }

        public void OnBattleStart(BattleSetup setup)
        {
            Setup = setup;
            Roster.OnBattleStart();
            Analysis.OnBattleStart(Roster.AllFighters);
            CurrentTurn = 0;
        }

        public void OnBattleEnd()
        {
            Roster.OnBattleEnd();
            Cache.OnBattleEnd();
            Analysis.OnBattleEnd();

            CurrentFighter = null;
            CurrentScheme = null;
            CurrentTurn = 0;
        }
        #endregion



        #region Properties
        public BattleSetup Setup { get; set; }
        public Roster Roster { get; private set; }
        public Cache Cache { get; private set; }
        public Analysis Analysis { get; private set; }

        public Fighter CurrentFighter { get; set; }
        public Scheme CurrentScheme { get; set; }
        public int CurrentTurn { get; set; }
        #endregion
    }
}