﻿using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.World.Fighters;

public class Analysis
{
    public Analysis()
    {
        TiersByFighter = new Dictionary<Fighter, int>();
    }

    public void OnBattleStart(List<Fighter> fighters)
    {
        fighters.ForEach(f => TiersByFighter.Add(f, 0));
    }

    public void OnBattleEnd()
    {
        TiersByFighter.Clear();
    }

    public int TierUp(Fighter fighter)
    {
        TiersByFighter[fighter]++;
        return TiersByFighter[fighter];
    }

    private Dictionary<Fighter, int> TiersByFighter { get; set; }
}
