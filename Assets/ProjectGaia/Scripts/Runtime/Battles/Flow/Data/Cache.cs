﻿using System.Collections.Generic;
using System.Linq;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.Skills;

namespace Taijj.ProjectGaia.Battle.Flow.Data
{
	public class Cache
	{
		#region Beginning
		private class Entry
		{
			public Fighter fighter;
			public SkillConfig lastSelectedSkill;
			public Dictionary<SkillConfig, Fighter> lastFocusTargetBySkill;
		}

		public Cache(BattleData data)
		{
			BattleData = data;
			Entries = new List<Entry>();
		}
		#endregion



		#region Validation
		private bool HasCurrentFighterCached()
		{
			Fighter fighter = BattleData.CurrentFighter;
			return Entries.Any(e => e.fighter == fighter);
		}

		public bool HasSkillCached()
		{
			if(!HasCurrentFighterCached())
			{
				return false;
			}
			return GetEntryFor(BattleData.CurrentFighter).lastSelectedSkill != null;
		}

		public bool HasTargetsCached(SkillConfig skill)
		{
			if(!HasCurrentFighterCached())
			{
				return false;
			}
			Entry targetEntry = GetEntryFor(BattleData.CurrentFighter);
			if(targetEntry.lastFocusTargetBySkill == null)
			{
				return false;
			}
			return targetEntry.lastFocusTargetBySkill.ContainsKey(skill);
		}
		#endregion



		#region Remembering
		public void RememberSkill(SkillConfig skill)
		{
			Fighter fighter = BattleData.CurrentFighter;
			if(HasCurrentFighterCached())
			{
				GetEntryFor(fighter).lastSelectedSkill = skill;
			}
			else
			{
				Entry newEntry = new Entry();
				newEntry.fighter = fighter;
				newEntry.lastSelectedSkill = skill;
				Entries.Add(newEntry);
			}
		}

		public void RememberFocusTarget(SkillConfig impact, Fighter focusTarget)
		{
			Fighter fighter = BattleData.CurrentFighter;
			Entry entry = GetEntryFor(fighter);
			if(entry.lastFocusTargetBySkill == null)
			{
				entry.lastFocusTargetBySkill = new Dictionary<SkillConfig, Fighter>();
			}
			entry.lastFocusTargetBySkill[impact] = focusTarget;
		}
		#endregion



		#region Misc
		public SkillConfig GetCachedSkill()
		{
			return GetEntryFor(BattleData.CurrentFighter).lastSelectedSkill;
		}

		public Fighter GetCachedFocusTarget(SkillConfig skill)
		{
			return GetEntryFor(BattleData.CurrentFighter).lastFocusTargetBySkill[skill];
		}

		public void OnBattleEnd()
		{
			Entries.Clear();
		}

		private Entry GetEntryFor(Fighter fighter)
		{
			return Entries.First(p => p.fighter == fighter);
		}
		#endregion



		#region Properties
		private List<Entry> Entries { get; set; }
		private BattleData BattleData { get; set; }
		#endregion
	}
}