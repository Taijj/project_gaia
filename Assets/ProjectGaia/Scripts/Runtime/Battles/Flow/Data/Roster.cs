﻿using System.Collections.Generic;
using System.Linq;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.Battle.World.Layout;

namespace Taijj.ProjectGaia.Battle.Flow.Data
{
	public class Roster
	{
		public void OnBattleStart()
        {
            BattleLayout.CreatePartiess();
            HeroParty = BattleLayout.Heroes;
            EnemyParty = BattleLayout.Enemies;

            AllFighters = new List<Fighter>();
            AllFighters.AddRange(HeroParty);
            AllFighters.AddRange(EnemyParty);
            AllFighters.ForEach(f => f.OnBattleStart());
        }

		public void OnBattleEnd()
		{
            BattleLayout.DestroyParties();
            HeroParty = null;
            EnemyParty = null;
            AllFighters = null;
		}



		public List<Fighter> HeroParty { get; private set; }
        public List<Fighter> EnemyParty { get; private set; }
        public List<Fighter> AllFighters { get; private set; }

        public List<Fighter> AliveHeroes { get { return GetAliveFrom(HeroParty); } }
        public List<Fighter> AliveEnemies { get { return GetAliveFrom(EnemyParty); } }
        public List<Fighter> AliveFighters { get { return GetAliveFrom(AllFighters); } }

		private List<Fighter> GetAliveFrom(List<Fighter> fighters)
        {
            return fighters.Where(f => f.IsAlive).ToList();
        }

        private BattleLayout BattleLayout => BattleStage.Scenery.BattleLayout;
	}
}