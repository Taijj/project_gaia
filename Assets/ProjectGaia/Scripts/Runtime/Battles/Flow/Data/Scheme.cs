﻿using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.Skills;

namespace Taijj.ProjectGaia.Battle.Flow.Data
{
	public class Scheme
    {
        public SkillConfig selectedSkill;
        public List<Fighter> targets;

        public Fighter GetSoleTarget()
        {
            return targets[0];
        }
    }
}