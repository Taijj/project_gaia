﻿using System;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.Battle.World.SkillEffects;
using Taijj.ProjectGaia.StaticData.Skills;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;

namespace Taijj.ProjectGaia.Battle.Flow.Utils
{
	public static class DebtCollector
	{
		public static void CollectSkillCosts(Fighter skillUser, SkillConfig usedSkill, SkillCost.DueMoment currentDueMoment)
		{
			if(!usedSkill.HasCost)
			{
				return;
			}

			Cost = usedSkill.Cost;
			if(Cost.MomentOfPay != currentDueMoment)
			{
				return;
			}

			User = skillUser;
			int collectedAmount = TryCollectAndGetDifference(currentDueMoment);
			VisualizeCollection(collectedAmount);
		}

		private static int TryCollectAndGetDifference(SkillCost.DueMoment currentDueMoment)
		{
			int oldValue = User.GetStat(Cost.Stat);
			int newValue = Math.Max(oldValue - Cost.Amount, 0);
			User.SetStat(Cost.Stat, newValue);
			return oldValue-newValue;
		}

		private static void VisualizeCollection(int collectedAmount)
		{
			SkillVisualizationInput input = new SkillVisualizationInput();
			input.numbersStat = Cost.Stat;
			input.position = User.transform.position;
			input.value = collectedAmount;
			input.effectKind = EffectKind.Normal;
			BattleStage.Scenery.SkillVisualizer.NumbersAnimator.ShowNumberAnimation(input);
		}

		private static SkillCost Cost { get; set; }
		private static Fighter User { get; set; }
	}
}