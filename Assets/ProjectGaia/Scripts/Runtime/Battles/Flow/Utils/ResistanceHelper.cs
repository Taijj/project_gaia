using System.Collections.Generic;
using Taijj.ProjectGaia.Battle.World.Fighters;
using Taijj.ProjectGaia.StaticData.Fighters;
using Taijj.ProjectGaia.StaticData.Skills.Appendages;
using static Taijj.ProjectGaia.StaticData.Fighters.Resistance;

namespace Taijj.ProjectGaia.Battle.Flow.Utils
{
    public static class ResistancesHelper
    {
        #region Calculations
        public static float CalculateDamageModifier(List<FormConfig> forms, Fighter target)
        {
            CurrentForms = forms;
            CurrentTarget = target;
            return CalculateModifer(DAMAGE_MODIFIERS);
        }

        public static float CalculateHealModifier(List<FormConfig> forms, Fighter target)
        {
            CurrentForms = forms;
            CurrentTarget = target;
            return CalculateModifer(HEAL_MODIFIERS);
        }

        public static float CalculateModifer(Dictionary<Modification, float> modifiers)
		{
			float highestModifier = 0f;
            float lowestModifier = 0f;
			bool modifies = false;
            foreach(FormConfig form in CurrentForms)
            {
                Resistance currentResistance = CurrentTarget.Resistances.Find(r => r.Form == form);
                if(currentResistance == null)
                {
					continue;
                }

				modifies = true;
				float currentModifier = modifiers[currentResistance.Mod];
                highestModifier = currentModifier > highestModifier ? currentModifier : highestModifier;
                lowestModifier = currentModifier < lowestModifier ? currentModifier : lowestModifier;
            }

			return modifies ? highestModifier + lowestModifier : 1f;
        }
        #endregion



        #region Properties
        private static Fighter CurrentTarget { get; set; }
        private static List<FormConfig> CurrentForms { get; set; }

        private static readonly Dictionary<Modification, float> DAMAGE_MODIFIERS = new Dictionary<Modification, float>()
        {
            { Modification.Double, 2f },
            { Modification.Strong, 1.5f },
            { Modification.Weak, 0.5f },
            { Modification.Nullify, 0f },
            { Modification.AbsorbHalf, -0.5f },
            { Modification.AbsorbFull, -1f }
        };

        private static readonly Dictionary<Modification, float> HEAL_MODIFIERS = new Dictionary<Modification, float>()
        {
            { Modification.Double, 0.5f },
            { Modification.Strong, 0.75f },
            { Modification.Weak, 1.5f },
            { Modification.Nullify, 1f },
            { Modification.AbsorbHalf, 2f },
            { Modification.AbsorbFull, 2.5f }
        };
        #endregion
    }
}