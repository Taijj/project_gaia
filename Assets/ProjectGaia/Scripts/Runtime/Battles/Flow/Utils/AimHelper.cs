using Taijj.ProjectGaia.StaticData.Skills.Appendages;

namespace Taijj.ProjectGaia.Battle.Flow.Utils
{
    public static class AimHelper
    {
        public static bool IsAutoAim(AimKind aim)
        {
            switch(aim)
            {
                case AimKind.Self:      return true;
                case AimKind.Single:
                case AimKind.Animation:
                default:                return false;
            }
        }

        public static bool SingleOrPartySwitchIsAllowed(AimKind aim)
        {
            return aim == AimKind.Animation;
        }
    }
}