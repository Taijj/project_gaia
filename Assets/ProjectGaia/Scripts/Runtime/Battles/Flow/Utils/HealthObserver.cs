﻿using System;
using Taijj.ProjectGaia.Battle.World.Fighters;

namespace Taijj.ProjectGaia.Battle.Flow.Utils
{
    public static class HealthObserver
    {
        public static event Action<Fighter> WasKilled;
        public static event Action<Fighter> WasResurrected;



        public static void UpdateAliveStatus(Fighter target, bool wasAlive)
        {
            TryKillFighter(target, wasAlive);
            TryResurrectFighter(target, wasAlive);
        }

        private static void TryKillFighter(Fighter target, bool wasAlive)
        {
            if(wasAlive && !target.IsAlive)
            {
                target.Die();
                WasKilled?.Invoke(target);
            }
        }

        private static void TryResurrectFighter(Fighter target, bool wasAlive)
        {
            if(!wasAlive && target.IsAlive)
            {
                target.Resurrect();
                WasResurrected?.Invoke(target);
            }
        }



        public static bool IsOnePartyDead
        {
            get
            {
                bool allHeroesAreDead = BattleFlow.BattleData.Roster.AliveHeroes.Count == 0;
                bool allEnemiesAreDead = BattleFlow.BattleData.Roster.AliveEnemies.Count == 0;
                return allHeroesAreDead || allEnemiesAreDead;
            }
        }
    }
}