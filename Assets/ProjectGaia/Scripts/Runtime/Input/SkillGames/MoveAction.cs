﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Taijj.ProjectGaia.Input.SkillGames
{
	public class MoveAction
	{
		public MoveAction(InputAction keyboardAction, InputAction gamepadAction)
		{
			keyboardAction.performed += context => ActionValue = context.ReadValue<Vector2>();
			Handlers = new List<Action<Vector2>>();
		}

		public void Register(Action<Vector2> handler)
		{
			if(Handlers.Contains(handler))
			{
				return;
			}
			Handlers.Add(handler);
		}

		public void Deregister(Action<Vector2> handler)
		{
			if(Handlers.Contains(handler))
			{
				Handlers.Remove(handler);
			}
		}

		public void UpdateHandlers()
		{
			Handlers.ForEach(h => h(ActionValue));
		}



		private Vector2 ActionValue { get; set; }
		private List< Action<Vector2> > Handlers { get; set; }
	}
}