﻿using System;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;

namespace Taijj.ProjectGaia.Input.SkillGames
{
    // New Unity InputSystem can't answer the question "which key was just pressed" sufficiently yet.
    // So for now this custom implementation has to do.
    public class AnyAction
    {
        public AnyAction()
		{}

        public void UpdateKeyCheck()
        {
            if(Keyboard.current == null)
            {
                return;
            }

            foreach(KeyControl control in Keyboard.current.allKeys)
            {
                TryDispatchEvents(control);
            }
        }

        private void TryDispatchEvents(KeyControl control)
        {
            if(control.wasPressedThisFrame)
            {
                OnActionStarted?.Invoke(control.keyCode);
            }
            else if(control.isPressed)
            {
                OnActionHeld?.Invoke(control.keyCode);
            }
        }

        public event Action<Key> OnActionStarted;
        public event Action<Key> OnActionHeld;
    }
}