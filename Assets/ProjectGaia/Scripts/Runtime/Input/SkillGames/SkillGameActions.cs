﻿namespace Taijj.ProjectGaia.Input.SkillGames
{
	public class SkillGameActions
	{
		public SkillGameActions(PlayerInputConfig.SkillGamesActions actions)
		{
			MoveAction = new MoveAction(actions.Move, null);
			AnyAction = new AnyAction();
		}

		public void UpdateActions()
		{
			MoveAction.UpdateHandlers();
			AnyAction.UpdateKeyCheck();
		}

		public MoveAction MoveAction { get; private set; }
		public AnyAction AnyAction { get; private set; }
	}
}