// GENERATED AUTOMATICALLY FROM 'Assets/ProjectGaia/Scripts/Runtime/Input/PlayerInputConfig.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerInputConfig : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerInputConfig()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerInputConfig"",
    ""maps"": [
        {
            ""name"": ""Menu"",
            ""id"": ""f2ee8f76-9878-44aa-b990-a8e81ea81b5b"",
            ""actions"": [
                {
                    ""name"": ""CursorKeyboard"",
                    ""type"": ""Value"",
                    ""id"": ""231d731e-ccb4-4838-aaae-07fd4544defc"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=1)""
                },
                {
                    ""name"": ""CursorGamepad"",
                    ""type"": ""Value"",
                    ""id"": ""d8affdfc-36f0-4f0c-94b3-c8fbe791705f"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2)""
                },
                {
                    ""name"": ""Confirm"",
                    ""type"": ""Button"",
                    ""id"": ""cfb6756c-7c23-490b-b6d4-0f06c059367b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Cancel"",
                    ""type"": ""Button"",
                    ""id"": ""4dc49fa3-a582-4762-8695-e180df5b5827"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Multi"",
                    ""type"": ""Value"",
                    ""id"": ""57544cd7-7954-4dee-9d9f-cd61e06867fa"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=1)""
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""70355747-6048-4c1c-b982-d373a2f31601"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CursorKeyboard"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""b2eaa7ca-89a3-436d-9c39-be10e4d4da4d"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CursorKeyboard"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""6c8ae92c-fd83-4dd0-9b38-1a7d57cd4c92"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CursorKeyboard"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""31855963-f0a6-4bc3-becf-5a6cb9f005db"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CursorKeyboard"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""742a2f3b-8909-4346-a4d4-abe0d34e5070"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CursorKeyboard"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""72070fb3-440b-4004-88d5-81d83d4a023f"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CursorKeyboard"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""f9c112e3-8ff1-450e-85d4-5b4d59ff7eaf"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CursorKeyboard"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""34caefd3-df34-49b2-8b37-b205ce984a07"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CursorKeyboard"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""ed298ec7-b3ae-4eca-a6af-f8eb87ec9adc"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CursorKeyboard"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""99d6d869-e8bf-4f2b-ba05-8df790ed2d87"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CursorKeyboard"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""91365a32-f944-4221-86eb-663cfc49f311"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Confirm"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""19fa6833-c0e8-48d2-b957-9a9d5a528585"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Confirm"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b7d1a1ba-185e-4904-800e-f60caeca6ffc"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Cancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a35597a1-337c-4da3-8dd9-b8d41328dc39"",
                    ""path"": ""<Keyboard>/numpad0"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Cancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4e4264d6-6d68-42b8-8753-249da961975d"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Cancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d3194c24-2c8b-415d-8ddb-5bd576267017"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CursorGamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2900420a-7641-4279-b3d7-c0fb382e47f1"",
                    ""path"": ""<Keyboard>/m"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Multi"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""SkillGames"",
            ""id"": ""cea0f7a4-5ff0-456f-8953-0e8c069d3e40"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""PassThrough"",
                    ""id"": ""f2c7849f-fe26-490a-8d4b-b11c5fa2b3eb"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""f33a46ec-c1c7-4ae2-9d1a-49eaa9c9f655"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CursorKeyboard"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""71e89d0f-c400-49df-b0a1-153c567096a9"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CursorKeyboard"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""cfd61c30-784d-49b3-a610-22be2a0a12cc"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CursorKeyboard"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""797375e9-cd41-4436-9ba5-e7922e4585f9"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CursorKeyboard"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""6a3e58d5-34e0-4346-a225-61f060ef1477"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CursorKeyboard"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""0d8b82a3-7758-48d2-bbff-50ce3738577b"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""93cece16-fd88-41b0-8829-5a7cfe1aa0cb"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""bdcc7b67-4b22-4e6c-8b53-59f900c35e36"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""022daeb5-94d5-44c0-82bb-e20c0260a427"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""67f64f56-9d7d-467d-8e0c-e3400304a727"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Menu
        m_Menu = asset.FindActionMap("Menu", throwIfNotFound: true);
        m_Menu_CursorKeyboard = m_Menu.FindAction("CursorKeyboard", throwIfNotFound: true);
        m_Menu_CursorGamepad = m_Menu.FindAction("CursorGamepad", throwIfNotFound: true);
        m_Menu_Confirm = m_Menu.FindAction("Confirm", throwIfNotFound: true);
        m_Menu_Cancel = m_Menu.FindAction("Cancel", throwIfNotFound: true);
        m_Menu_Multi = m_Menu.FindAction("Multi", throwIfNotFound: true);
        // SkillGames
        m_SkillGames = asset.FindActionMap("SkillGames", throwIfNotFound: true);
        m_SkillGames_Move = m_SkillGames.FindAction("Move", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Menu
    private readonly InputActionMap m_Menu;
    private IMenuActions m_MenuActionsCallbackInterface;
    private readonly InputAction m_Menu_CursorKeyboard;
    private readonly InputAction m_Menu_CursorGamepad;
    private readonly InputAction m_Menu_Confirm;
    private readonly InputAction m_Menu_Cancel;
    private readonly InputAction m_Menu_Multi;
    public struct MenuActions
    {
        private @PlayerInputConfig m_Wrapper;
        public MenuActions(@PlayerInputConfig wrapper) { m_Wrapper = wrapper; }
        public InputAction @CursorKeyboard => m_Wrapper.m_Menu_CursorKeyboard;
        public InputAction @CursorGamepad => m_Wrapper.m_Menu_CursorGamepad;
        public InputAction @Confirm => m_Wrapper.m_Menu_Confirm;
        public InputAction @Cancel => m_Wrapper.m_Menu_Cancel;
        public InputAction @Multi => m_Wrapper.m_Menu_Multi;
        public InputActionMap Get() { return m_Wrapper.m_Menu; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MenuActions set) { return set.Get(); }
        public void SetCallbacks(IMenuActions instance)
        {
            if (m_Wrapper.m_MenuActionsCallbackInterface != null)
            {
                @CursorKeyboard.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnCursorKeyboard;
                @CursorKeyboard.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnCursorKeyboard;
                @CursorKeyboard.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnCursorKeyboard;
                @CursorGamepad.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnCursorGamepad;
                @CursorGamepad.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnCursorGamepad;
                @CursorGamepad.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnCursorGamepad;
                @Confirm.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnConfirm;
                @Confirm.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnConfirm;
                @Confirm.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnConfirm;
                @Cancel.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnCancel;
                @Cancel.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnCancel;
                @Cancel.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnCancel;
                @Multi.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnMulti;
                @Multi.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnMulti;
                @Multi.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnMulti;
            }
            m_Wrapper.m_MenuActionsCallbackInterface = instance;
            if (instance != null)
            {
                @CursorKeyboard.started += instance.OnCursorKeyboard;
                @CursorKeyboard.performed += instance.OnCursorKeyboard;
                @CursorKeyboard.canceled += instance.OnCursorKeyboard;
                @CursorGamepad.started += instance.OnCursorGamepad;
                @CursorGamepad.performed += instance.OnCursorGamepad;
                @CursorGamepad.canceled += instance.OnCursorGamepad;
                @Confirm.started += instance.OnConfirm;
                @Confirm.performed += instance.OnConfirm;
                @Confirm.canceled += instance.OnConfirm;
                @Cancel.started += instance.OnCancel;
                @Cancel.performed += instance.OnCancel;
                @Cancel.canceled += instance.OnCancel;
                @Multi.started += instance.OnMulti;
                @Multi.performed += instance.OnMulti;
                @Multi.canceled += instance.OnMulti;
            }
        }
    }
    public MenuActions @Menu => new MenuActions(this);

    // SkillGames
    private readonly InputActionMap m_SkillGames;
    private ISkillGamesActions m_SkillGamesActionsCallbackInterface;
    private readonly InputAction m_SkillGames_Move;
    public struct SkillGamesActions
    {
        private @PlayerInputConfig m_Wrapper;
        public SkillGamesActions(@PlayerInputConfig wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_SkillGames_Move;
        public InputActionMap Get() { return m_Wrapper.m_SkillGames; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(SkillGamesActions set) { return set.Get(); }
        public void SetCallbacks(ISkillGamesActions instance)
        {
            if (m_Wrapper.m_SkillGamesActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_SkillGamesActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_SkillGamesActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_SkillGamesActionsCallbackInterface.OnMove;
            }
            m_Wrapper.m_SkillGamesActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
            }
        }
    }
    public SkillGamesActions @SkillGames => new SkillGamesActions(this);
    public interface IMenuActions
    {
        void OnCursorKeyboard(InputAction.CallbackContext context);
        void OnCursorGamepad(InputAction.CallbackContext context);
        void OnConfirm(InputAction.CallbackContext context);
        void OnCancel(InputAction.CallbackContext context);
        void OnMulti(InputAction.CallbackContext context);
    }
    public interface ISkillGamesActions
    {
        void OnMove(InputAction.CallbackContext context);
    }
}
