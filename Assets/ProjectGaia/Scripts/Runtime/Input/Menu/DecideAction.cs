﻿using System;
using UnityEngine.InputSystem;

namespace Taijj.ProjectGaia.Input.Menu
{
	public class DecideAction
	{
		#region Main
		public DecideAction(InputAction confirmAction, InputAction cancelAction)
		{
			confirmAction.performed += OnConfirmPerformed;
			cancelAction.performed += OnCancelPerformed;
		}

		private void OnConfirmPerformed(InputAction.CallbackContext context)
		{
			DispatchEvent(OnConfirmed);
		}

		private void OnCancelPerformed(InputAction.CallbackContext context)
		{
			DispatchEvent(OnCanceled);
		}
		#endregion



		#region Events
		public event Action OnConfirmed;
		public event Action OnCanceled;

		private void DispatchEvent(Action action)
		{
			action?.Invoke();
		}
		#endregion
	}
}