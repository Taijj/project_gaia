﻿using System;
using UnityEngine.InputSystem;

namespace Taijj.ProjectGaia.Input.Menu
{
	public class MultiAction
	{
		#region Main
		public MultiAction(InputAction inputAction)
		{
			inputAction.performed += OnPagePerformed;
		}

		private void OnPagePerformed(InputAction.CallbackContext context)
		{
			float axisValue = context.ReadValue<float>();
			DispatchEvent(axisValue < 0f ? Up : Down);
		}
		#endregion



		#region Events
		public event Action Up;
		public event Action Down;

		private void DispatchEvent(Action action)
		{
			if(action != null)
			{
				action();
			}
		}
		#endregion
	}
}