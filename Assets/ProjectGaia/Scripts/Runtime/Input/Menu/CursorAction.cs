﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Taijj.ProjectGaia.Input.Menu
{
	public class CursorAction
	{
		#region Init
		private const int INITIAL_CURSOR_DELAY_FRAMES = 30;
		private const int FAST_CURSOR_DELAY_FRAMES = 3;

		public CursorAction(InputAction keyboardAction, InputAction gamepadAction)
		{
			keyboardAction.started += OnStarted;
			keyboardAction.performed += OnEnded;
			gamepadAction.performed += OnGamePadPerformed;

			GamepadIsIdle = true;
		}

		private void OnGamePadPerformed(InputAction.CallbackContext context)
		{
			if(GamepadIsIdle)
			{
				GamepadIsIdle = false;
				OnStarted(context);
			}
			else
			{
				GamepadIsIdle = true;
				OnEnded(context);
			}
		}

		private bool GamepadIsIdle { get; set; }
		#endregion



		#region Input Detection
		private void OnStarted(InputAction.CallbackContext context)
		{
			ActionValue = context.ReadValue<Vector2>();
			InitializeHoldButton();
			DispatchDirectionalEvent();
		}

		private void OnEnded(InputAction.CallbackContext context)
		{
			ActionValue = Vector2.zero;
		}

		public void UpdateHoldButton()
		{
			if(ActionValue != Vector2.zero)
			{
				TryPerformWithInitialDelay();
				TryPerformFast();
				FrameCounter++;
			}
		}
		#endregion



		#region Input Handling
		private void InitializeHoldButton()
		{
			FrameCounter = 0;
			FastCursorActivated = false;
		}

		private void TryPerformWithInitialDelay()
		{
			if(FrameCounter > 60 && !FastCursorActivated)
			{
				DispatchDirectionalEvent();
				FastCursorActivated = true;
			}
		}

		private void TryPerformFast()
		{
			if(FastCursorActivated && FrameCounter > 5)
			{
				FrameCounter = 0;
				DispatchDirectionalEvent();
			}
		}



		private void DispatchDirectionalEvent()
		{
			if(Mathf.Abs(ActionValue.x) >= Mathf.Abs(ActionValue.y))
			{
				DispatchEvent(ActionValue.x < 0f ? Left : Right);
			}
			else
			{
				DispatchEvent(ActionValue.y < 0f ? Down : Up);
			}
		}

		private Vector2 ActionValue { get; set; }
		private int FrameCounter { get; set; }
		private bool FastCursorActivated { get; set; }
		#endregion



		#region Events
		public event Action Up;
        public event Action Down;
		public event Action Left;
		public event Action Right;

		private void DispatchEvent(Action action)
		{
			if(action != null)
			{
				action();
			}
		}
		#endregion
	}
}