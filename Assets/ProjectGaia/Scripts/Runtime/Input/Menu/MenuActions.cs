﻿namespace Taijj.ProjectGaia.Input.Menu
{
	public class MenuActions
	{
		public MenuActions(PlayerInputConfig.MenuActions actions)
		{
			Cursor = new CursorAction(actions.CursorKeyboard, actions.CursorGamepad);
			Decide = new DecideAction(actions.Confirm, actions.Cancel);
			Multi = new MultiAction(actions.Multi);
		}

		public void UpdateActions()
		{
			Cursor.UpdateHoldButton();
		}

		public CursorAction Cursor { get; private set; }
		public DecideAction Decide { get; private set; }
		public MultiAction Multi { get; private set; }
	}
}