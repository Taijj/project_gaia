﻿using Taijj.ProjectGaia.Input.Menu;
using Taijj.ProjectGaia.Input.SkillGames;
using Taijj.ProjectGaia.Utils.SceneSingletons;

namespace Taijj.ProjectGaia.Input
{
	public class PlayerInput : SceneSingleton<PlayerInput>
	{
		#region Init
		protected override void Wake()
		{
			base.Wake();
			Config = new PlayerInputConfig();
			Menu = new MenuActions(Config.Menu);
			SkillGames = new SkillGameActions(Config.SkillGames);
		}
		#endregion



		#region Enable/Disable
		public void Enable()
		{
			if(IsEnabled)
			{
				return;
			}

			Config.Enable();
			UpdateCaller.Instance.StartCallingOnUpdate(InputUpdate);
		}

		public void Disable()
		{
			if(IsEnabled)
			{
				Config.Disable();
				UpdateCaller.Instance.StopCallingOnUpdate(InputUpdate);
			}
		}

		private void InputUpdate()
		{
			Menu.UpdateActions();
			SkillGames.UpdateActions();
		}
		#endregion



		#region Properties
		private bool IsEnabled { get; set; }
		private PlayerInputConfig Config { get; set; }

		public MenuActions Menu { get; private set; }
		public SkillGameActions SkillGames { get; private set; }
		#endregion
	}
}